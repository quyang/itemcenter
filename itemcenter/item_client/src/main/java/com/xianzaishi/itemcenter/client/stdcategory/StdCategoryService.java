package com.xianzaishi.itemcenter.client.stdcategory;

import java.util.List;

import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.common.result.Result;


/**
 * 后台类目接口，包括查询单个类目、查询多个类目、查询子类目以及插入类目和更新类目
 * 
 * @author dongpo
 * 
 */
public interface StdCategoryService {

  /**
   * 按照类目id、类目名字请求数据库
   * 
   * @param stdCategoryQuery
   * @return
   */
  Result<CategoryDTO> queryCategoryById(Integer catId);

  /**
   * 按照类目名称
   * 
   * @param stdCategoryQuery
   * @return
   */
  Result<CategoryDTO> queryCategoryByName(String catName);
  
  /**
   * 按照类目id列表查询，一个id列表上限为20
   * 
   * @param catIds
   * @return
   */
  Result<List<CategoryDTO>> queryCategoriesByIdList(List<Integer> catIds);
  
  /**
   * 根据类目编号批量查询
   * @param catCode
   * @return
   */
  Result<List<CategoryDTO>> queryCategoriesByCatCode(Long catCode);

  /**
   * 按照类目id查询子类目
   * 
   * @param catId
   * @return
   */
  Result<List<CategoryDTO>> querySubcategoryById(Integer catId);

  /**
   * 添加类目
   * 
   * @param categoryDto
   * @return
   */
  Result<Integer> insertCategory(CategoryDTO categoryDto);

  /**
   * 更新类目
   * 
   * @param categoryDto
   * @return
   */
  Result<Boolean> updateCategory(CategoryDTO categoryDto);



}
