package com.xianzaishi.itemcenter.client.itemsku.query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 查询前后台sku关联关系查询对象
 * 
 * @author zhancang
 */

public class SkuRelationScheduleQuery extends BaseQuery implements Serializable {

  /**
   * serializable UID版本号
   */
  private static final long serialVersionUID = 1188023645503002389L;

  /**
   * 采购skuid组
   */
  private List<Long> pskuIds;


  /**
   * 销售端skuid组
   */
  private List<Long> cskuIds;
  
  /**
   * 早于采购结束
   */
  private Date beforeBuyEnd;
  
  /**
   * 晚于采购开始
   */
  private Date afterBuyStart;
  
  /**
   * 早于消费结束
   */
  private Date beforeSaleEnd;
  
  /**
   * 晚于消费开始
   */
  private Date afterSaleStart;
  /**
   * 进售价关系类型
   */
  private Short relationType = RelationTypeConstants.RELATION_TYPE_NORMAL;
  
  /**
   * 下限
   */
  private Integer buyPriceMin;
  
  /**
   * 下限
   */
  private Integer salePriceMin;
  
  public List<Long> getPskuIds() {
    return pskuIds;
  }
  public void setPskuIds(List<Long> pskuIds) {
    this.pskuIds = pskuIds;
  }
  public List<Long> getCskuIds() {
    return cskuIds;
  }
  public void setCskuIds(List<Long> cskuIds) {
    this.cskuIds = cskuIds;
  }
  public Date getBeforeBuyEnd() {
    return beforeBuyEnd;
  }
  public void setBeforeBuyEnd(Date beforeBuyEnd) {
    this.beforeBuyEnd = beforeBuyEnd;
  }
  public Date getAfterBuyStart() {
    return afterBuyStart;
  }
  public void setAfterBuyStart(Date afterBuyStart) {
    this.afterBuyStart = afterBuyStart;
  }
  public Date getBeforeSaleEnd() {
    return beforeSaleEnd;
  }
  public void setBeforeSaleEnd(Date beforeSaleEnd) {
    this.beforeSaleEnd = beforeSaleEnd;
  }
  public Date getAfterSaleStart() {
    return afterSaleStart;
  }
  public void setAfterSaleStart(Date afterSaleStart) {
    this.afterSaleStart = afterSaleStart;
  }
  public Short getRelationType() {
    return relationType;
  }
  public void setRelationType(Short relationType) {
    this.relationType = relationType;
  }
  public Integer getBuyPriceMin() {
    return buyPriceMin;
  }
  public void setBuyPriceMin(Integer buyPriceMin) {
    this.buyPriceMin = buyPriceMin;
  }
  public Integer getSalePriceMin() {
    return salePriceMin;
  }
  public void setSalePriceMin(Integer salePriceMin) {
    this.salePriceMin = salePriceMin;
  }
}
