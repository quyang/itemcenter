package com.xianzaishi.itemcenter.client.sysproperty;

import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.result.Result;

public interface SystemPropertyService {

  Result<SystemConfigDTO> getSystemConfig(Integer configId);
  
  Result<Boolean> updateSystemConfig(SystemConfigDTO config);
}
