package com.xianzaishi.itemcenter.client.item.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * 商品表全部字段，一般返回商品详情页需要
 * 
 * @author dongpo
 * 
 */
public class ItemDTO extends ItemBaseDTO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = 4128033156159539535L;

  /**
   * 销量
   */
  private Integer soldCount;

  /**
   * 归属用户id
   */
  private Integer ownerId;

  /**
   * 商品状态
   */
  private Short status = ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES;

  /**
   * 商品创建时间
   */
  private Date gmtCreate;

  /**
   * 修改时间
   */
  private Date gmtModified;

  /**
   * 开始销售时间
   */
  private Date start;

  /**
   * 结束销售时间
   */
  private Date end;

  /**
   * 收藏数量
   */
  private Integer collectionsCount;

  /**
   * 要约合同条款id列表，逗号分隔
   */
  private String provision;

  /**
   * 商品属性，用每一位标识一个boolean属性。是否打折，是否下架，是否有sku等，各种可以打标类型
   */
  private Integer option;

  /**
   * 运费模板
   */
  private Integer postageId;

  /**
   * 产品id
   */
  private Long productId;

  /**
   * 商店id
   */
  private Integer shopId;

  /**
   * 商品描述
   */
  private String introduction;

  /**
   * 格式化后的商品描述
   */
  private List<IntroductDataDTO> introductionDataList;
  
  /**
   * 创建人
   */
  private Integer creator;

  /**
   * 审核人&原因，格式：cu:***,***,|cr:******。，cu(审核用户列表)，cr(审核原因)
   */
  private String checkinfo;

  /**
   * 商品后台属性，同步到前台
   */
  private String properties;
  
  /**
   * 规格属性
   */
  private String specification;
  
  /**
   * 销售类型
   */
  private Short saleStatus = SaleStatusConstants.SALE_ONLINE_AND_MARKET;
  
  /**
   * 标题标签
   */
  private String titletags;

  /**
   * 操作人
   */
  private Long processor;
  
  public Integer getSoldCount() {
    return soldCount;
  }

  public void setSoldCount(Integer soldCount) {
    this.soldCount = soldCount;
  }

  public Integer getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(Integer ownerId) {
    this.ownerId = ownerId;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Date getStart() {
    return start;
  }

  public void setStart(Date start) {
    this.start = start;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public Integer getCollectionsCount() {
    return collectionsCount;
  }

  public void setCollectionsCount(Integer collectionsCount) {
    this.collectionsCount = collectionsCount;
  }

  public String getProvision() {
    return provision;
  }

  public void setProvision(String provision) {
    this.provision = provision;
  }

  public Integer getOption() {
    return option;
  }

  public void setOption(Integer option) {
    this.option = option;
  }

  public Integer getPostageId() {
    return postageId;
  }

  public void setPostageId(Integer postageId) {
    this.postageId = postageId;
  }

  public Long getProductId() {
    return productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public Integer getShopId() {
    return shopId;
  }

  public void setShopId(Integer shopId) {
    this.shopId = shopId;
  }

  public String getIntroduction() {
    return introduction;
  }

  public void setIntroduction(String introduction) {
    this.introduction = introduction;
  }

  public List<IntroductDataDTO> getIntroductionDataList() {
    return introductionDataList;
  }

  public void setIntroductionDataList(List<IntroductDataDTO> introductionDataList) {
    this.introductionDataList = introductionDataList;
  }

  public Integer getCreator() {
    return creator;
  }

  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  public String getCheckinfo() {
    return checkinfo;
  }

  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo;
  }

  public String getProperties() {
    return properties;
  }

  public void setProperties(String properties) {
    this.properties = properties;
  }

  public Short getSaleStatus() {
    return saleStatus;
  }

  public void setSaleStatus(Short saleStatus) {
    Preconditions.checkNotNull(saleStatus, "saleStatus == null");
    Preconditions.checkArgument(SaleStatusConstants.isCorrectParameter(saleStatus),
        "saleStatus not in(1,2,3)");
    this.saleStatus = saleStatus;
  }

  public String getSpecification() {
    return specification;
  }

  public void setSpecification(String specification) {
    this.specification = specification;
  }

  public String getTitletags() {
    return titletags;
  }

  public void setTitletags(String titletags) {
    this.titletags = titletags;
  }

  public Long getProcessor() {
    return processor;
  }

  public void setProcessor(Long processor) {
    this.processor = processor;
  }
  
}
