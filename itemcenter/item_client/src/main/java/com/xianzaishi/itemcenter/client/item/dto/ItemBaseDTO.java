package com.xianzaishi.itemcenter.client.item.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import com.google.common.base.Preconditions;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;

/**
 * 商品列表部分字段，一般返回商品list的时候只需要这些字段
 * 
 * @author dongpo
 * 
 */
public class ItemBaseDTO implements Serializable {

  /**
   * 版本号
   */
  private static final long serialVersionUID = -7884913113998956331L;

  /**
   * 商品id
   */
  private Long itemId;

  /**
   * 单个商品图片地址
   */
  private String picUrl;

  /**
   * 商品标题
   */
  private String title;

  /**
   * 商品副标题
   */
  private String subtitle;

  /**
   * 原价
   */
  private Integer price;
  
  /**
   * 所有图片链接组合
   */
  private List<String> picList;

  /**
   * 商品折扣价
   */
  private Integer discountPrice;

  /**
   * sku id列表
   */
  private String sku;

  /**
   * 商品特征，包含类目标准属性，扩展结构和商品特征值，sku id列表
   */
  private String feature;

  /**
   * 商品库存
   */
  private Integer inventory;

  /**
   * 后台叶子类目id
   */
  private Integer categoryId;

  /**
   * 后台三级类目，叶子类目父类目
   */
  private Integer cmCat2;
  /**
   * 商品下的sku对象列表
   */
  private List<ItemCommoditySkuDTO> itemSkuDtos;


  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    Preconditions.checkArgument(itemId >= 0, "itemId must be greater than 0");
    this.itemId = itemId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public Integer getPrice() {
    return price;
  }

  @SuppressWarnings("unused")
  private BigDecimal priceYuan;
  
  /**
   * 推荐使用  queryPriceYuan
   * 
   * @return
   */
  public BigDecimal getPriceYuan() {
    return  new BigDecimal(getPrice()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN);
  }
  
  /**
   * 使用无效，只为支持反射接口
   * 
   * @return
   */
  @Deprecated
  public void setPriceYuan(BigDecimal priceYuan) {
  }

  /**
   * 推荐使用getPriceYuan()方法
   * 
   * @return
   */
  @Deprecated
  public BigDecimal queryPriceYuan() {
    return getPriceYuan();
  }
  
  @SuppressWarnings("unused")
  private String priceYuanString;
  /**
   * 推荐使用 queryPriceYuanString
   * 
   * @return
   */
  public String getPriceYuanString() {
    return getPriceYuan().toString();
  }

  /**
   * 使用无效，只为支持反射接口
   * 
   * @return
   */
  @Deprecated
  public void setPriceYuanString(String priceYuanString) {
  }
  /**
   * 推荐使用getPriceYuanString()方法
   * 
   * @return
   */
  @Deprecated
  public String queryPriceYuanString() {
    return getPriceYuanString();
  }
  
  public void setPrice(Integer price) {
    this.price = price;
  }

  public Integer getDiscountPrice() {
    return discountPrice;
  }

  @SuppressWarnings("unused")
  private BigDecimal discountPriceYuan;
  
  /**
   * 推荐使用 queryDiscountPriceYuan
   * 
   * @return
   */
  public BigDecimal getDiscountPriceYuan() {
    return new BigDecimal(getDiscountPrice()).divide(new BigDecimal(100), 2,
        BigDecimal.ROUND_HALF_EVEN);
  }
  
  /**
   * 使用无效，只为支持反射接口
   * 
   * @return
   */
  @Deprecated
  public void setDiscountPriceYuan(BigDecimal discountPriceYuan) {
  }

  /**
   * getDiscountPriceYuan()方法
   * 
   * @return
   */
  @Deprecated
  public BigDecimal queryDiscountPriceYuan() {
    return getDiscountPriceYuan();
  }
  
  @SuppressWarnings("unused")
  private String discountPriceYuanString;
  
  /**
   * 获取字符串格式
   * 
   * @return
   */
  public String getDiscountPriceYuanString() {
    return getDiscountPriceYuan().toString();
  }

  /**
   * 使用无效，只为支持反射接口
   * 
   * @return
   */
  @Deprecated
  public void setDiscountPriceYuanString(String discountPriceYuanString) {
  }

  /**
   * 推荐 getDiscountPriceYuanString()方法
   * 
   * @return
   */
  @Deprecated
  public String queryDiscountPriceYuanString() {
    return getDiscountPriceYuanString();
  }
  
  public void setDiscountPrice(Integer discountPrice) {
    this.discountPrice = discountPrice;
  }

  public String getFeature() {
    return feature;
  }

  public void setFeature(String feature) {
    this.feature = feature;
  }

  public String getPicUrl() {
    return picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl;
  }

  public List<String> getPicList() {
    return picList;
  }

  public void setPicList(List<String> picList) {
    this.picList = picList;
  }

  public Integer getInventory() {
    return inventory;
  }

  public void setInventory(Integer inventory) {
    this.inventory = inventory;
  }

  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku == null ? null : sku.trim();
  }

  public List<ItemCommoditySkuDTO> getItemSkuDtos() {
    return itemSkuDtos;
  }

  public void setItemSkuDtos(List<ItemCommoditySkuDTO> itemSkuDtos) {
    this.itemSkuDtos = itemSkuDtos;
  }

  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Integer categoryId) {
//    Preconditions.checkArgument(null !=categoryId && categoryId >= 0, "categoryId must be greater than 0");
    this.categoryId = categoryId;
  }
  
  public Integer getCmCat2() {
    return cmCat2;
  }

  public void setCmCat2(Integer cmCat2) {
    this.cmCat2 = cmCat2;
  }

  /**
   * 商品sku状态
   * 
   * @author zhancang
   */
  public static class ItemStatusConstants implements Serializable {

    private static final long serialVersionUID = -3464189054053122453L;

    /**
     * 预发布状态，创建后台商品时同步创建前台商品状态
     */
    public static final Short ITEM_STATUS_PRE = -5;

    /**
     * 审核不通过状态
     */
    public static final Short ITEM_STATUS_CHECKED_NOTPASS = -1;
    
    /**
     * 正在待审核状态
     */
    public static final Short ITEM_STATUS_WAITCHECK = 0;

    /**
     * 审核通过并上架
     */
    public static final Short ITEM_STATUS_PASS_AND_UP_SHELVES = 1;

    /**
     * 审核通过状态并未架
     */
    public static final Short ITEM_STATUS_PASS_AND_DOWN_SHELVES = 2;
    
//    /**
//     * 采购中状态。采购单被审核通过后，进入采购中状态
//     */
//    public static final Short ITEM_STATUS_PURCHASE = 10;
//
//    /**
//     * 采购商品入库开始后，进入入库中状态
//     */
//    public static final Short ITEM_STATUS_STOREGA_START = 11;
//    
//    /**
//     * 采购商品最后一笔入库后，进入入库完成状态
//     */
//    public static final Short ITEM_STATUS_STOREGA_END = 12;
    
    /**
     * 草稿版本
     */
    public static final Short ITEM_STATUS_DRAFT = -5;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return ITEM_STATUS_PRE.equals(parameter) || ITEM_STATUS_WAITCHECK.equals(parameter)
          || ITEM_STATUS_PASS_AND_UP_SHELVES.equals(parameter) || ITEM_STATUS_CHECKED_NOTPASS.equals(parameter)
          || ITEM_STATUS_DRAFT.equals(parameter) || ITEM_STATUS_PASS_AND_DOWN_SHELVES.equals(parameter);
    }
  }

  /**
   * sku销售方式
   * 
   * @author zhancang
   */
  public static class SaleStatusConstants implements Serializable {

    private static final long serialVersionUID = -3464189024053122453L;

    /**
     * 线上线下同步销售
     */
    public static final Short SALE_ONLINE_AND_MARKET = 1;

    /**
     * 线上销售
     */
    public static final Short SALE_ONLY_ONLINE = 2;

    /**
     * 线下销售
     */
    public static final Short SALE_ONLY_MARKET = 3;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return SALE_ONLINE_AND_MARKET.equals(parameter) || SALE_ONLY_ONLINE.equals(parameter)
          || SALE_ONLY_MARKET.equals(parameter);
    }
  }
}
