package com.xianzaishi.itemcenter.client.item.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.common.base.Preconditions;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;

/**
 * 后台商品标准对象
 * 
 * @author zhancang
 *
 */
public class ItemProductDTO implements Serializable {

  private static final long serialVersionUID = -5489707817822774998L;

  private Date now = new Date();
  /**
   * 后台商品（采购商品）id
   */
  private Long productId;

  /**
   * 商品标题
   */
  private String title;

  /**
   * 商品子标题
   */
  private String subtitle;
  
  /**
   * 对应后台叶子类目（第一套类目）
   */
  private Integer categoryId;

  /**
   * 属性配置（忽略）
   */
  private String affectProperties;

  /**
   * 商品状态
   */
  private Short status = ItemProductStatusConstants.PRODUCT_STATUS_PASS;

  /**
   * 创建时间
   */
  private Date gmtCreate = now;

  /**
   * 
   */
  private Date gmtModified = now;

  /**
   * 图片列表
   */
  private List<String> picList;

  /**
   * 后台属性
   */
  private String feature;

  /**
   * 商品介绍
   */
  private String introduction;

  /**
   * 创建人
   */
  private Integer creator;

  /**
   * 商品属性，过滤掉前台输入的  & ; \r\n 符号，作为保留字使用
   */
  private String properties;

  /**
   * 后台sku列表
   */
  private List<ItemProductSkuDTO> itemProductSkuList;

  /**
   * 审核失败原因
   */
  private String checkedFailedReason;

  /**
   * 审核人列表
   */
  private List<Integer> checkerList;
  
  /**
   * 商品标题标签
   */
  private String titletags;

  public Long getProductId() {
    return productId == null ? 0 : productId;
  }

  public void setProductId(Long productId) {
    this.productId = productId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle;
  }

  public Integer getCategoryId() {
    return categoryId == null ? 0 : categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    Preconditions.checkNotNull(categoryId, "categoryId == null");
    Preconditions.checkArgument(categoryId > 0, "categoryId <= 0");
    this.categoryId = categoryId;
  }

  public String getAffectProperties() {
    return affectProperties;
  }

  public void setAffectProperties(String affectProperties) {
    this.affectProperties = affectProperties == null ? null : affectProperties.trim();
  }

  public Short getStatus() {
    return status == null ? ItemProductStatusConstants.PRODUCT_STATUS_PASS : status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    this.status = status;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    Preconditions.checkNotNull(gmtCreate, "gmtCreate == null");
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    Preconditions.checkNotNull(gmtModified, "gmtModified == null");
    this.gmtModified = gmtModified;
  }

  public String getFeature() {
    return feature;
  }

  public void setFeature(String feature) {
    this.feature = feature == null ? null : feature.trim();
  }

  public String getIntroduction() {
    return introduction;
  }

  public void setIntroduction(String introduction) {
    this.introduction = introduction == null ? null : introduction.trim();
  }

  public Integer getCreator() {
    return creator == null ? 0 : creator;
  }

  public void setCreator(Integer creator) {
    Preconditions.checkNotNull(creator, "creator == null");
    Preconditions.checkArgument(creator >= 0, "creator <= 0");
    this.creator = creator;
  }

//  public String getProperties() {
//    return properties;
//  }
//
//  public void setProperties(String properties) {
//    this.properties = properties == null ? null : properties.trim();
//  }

  public List<String> getPicList() {
    return picList == null ? Collections.<String>emptyList() : picList;
  }

  public void setPicList(List<String> picList) {
    this.picList = picList;
  }

  public List<ItemProductSkuDTO> getItemProductSkuList() {
    return itemProductSkuList == null ? Collections.<ItemProductSkuDTO>emptyList()
        : itemProductSkuList;
  }

  public void setItemProductSkuList(List<ItemProductSkuDTO> itemProductSkuList) {
    this.itemProductSkuList = itemProductSkuList;
  }

  public String getCheckedFailedReason() {
    return checkedFailedReason;
  }

  public void setCheckedFailedReason(String checkedFailedReason) {
    this.checkedFailedReason = checkedFailedReason;
  }

  public List<Integer> getCheckerList() {
    return checkerList == null ? Collections.<Integer>emptyList() : checkerList;
  }

  public void setCheckerList(List<Integer> checkerList) {
    this.checkerList = checkerList;
  }

  public String getTitletags() {
    return titletags;
  }

  public void setTitletags(String titletags) {
    this.titletags = titletags;
  }


  /**
   * 后台商品状态
   * 
   * @author zhancang
   */
  public static class ItemProductStatusConstants implements Serializable {

    private static final long serialVersionUID = -3464189054053122453L;

    /**
     * 正在待审核状态
     */
    public static final Short PRODUCT_STATUS_WAITCHECK = 0;

    /**
     * 审核通过状态
     */
    public static final Short PRODUCT_STATUS_PASS = 1;

    /**
     * 审核不通过状态
     */
    public static final Short PRODUCT_STATUS_CHECKED_NOTPASS = 2;

    /**
     * 草稿版本
     */
    public static final Short PRODUCT_STATUS_DRAFT = 4;


    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      if(null == parameter){
        return false;
      }
      return PRODUCT_STATUS_WAITCHECK.equals(parameter) || PRODUCT_STATUS_PASS.equals(parameter)
          || PRODUCT_STATUS_CHECKED_NOTPASS.equals(parameter) || PRODUCT_STATUS_DRAFT.equals(parameter);
    }
  }
}
