package com.xianzaishi.itemcenter.client.item;

import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.item.query.ItemQuery;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.common.result.Result;
import java.util.List;
import java.util.Map;

/**
 * 商品接口，包括查询商品list、查询详情页商品、发布商品以及更新商品信息
 * 
 * @author dongpo
 * 
 */
public interface CommodityService {

  /**
   * 查询商品结果map中，取商品list key
   */
  String ITEM_LIST_KEY = "itemList";
  
  /**
   * 查询商品结果map中，取符合条件个数 key
   */
  String ITEM_COUNT_KEY = "itemCount";
  
  /**
   * 查询商品结果map中，取商品类目 key
   */
  String ITEM_CAT_KEY = "itemCat";
  
  /**
   * 通过商品id组以及其他条件查询，返回商品list数据,
   * 
   * @param query
   * @return map包括data和count两个键，data对应的值为商品数据，count表示商品数据的数量
   */
  @Deprecated
  Result<Map<String, Object>> queryCommodies(ItemListQuery query);



  /**
   * 返回商品详情页
   * 
   * @param itemId
   * @return
   */
  Result<ItemBaseDTO> queryCommodity(ItemQuery query);

  /**
   * 搜索title
   */
  Result<List<ItemCommoditySkuDTO>> queryCommodityLikeTitle(ItemDTO itemDto, Integer pageSize,
      Integer currentPage);

  /**
   * 发布商品
   * 
   * @param itemDto
   * @return
   */
  Result<Long> insertCommodity(ItemDTO itemDto);

  /**
   * 更新商品
   * 
   * @param itemDto
   * @return
   */
  Result<Boolean> updateCommodity(ItemDTO itemDto);
  
  /**
   * 更新商品价格
   * 
   * @param itemDto
   * @return
   */
  Result<Boolean> updateCommodityPrice(ItemDTO itemDto, Boolean isDiscount);

  /**
   * 更新商品图片
   * @param itemDto
   * @return
   */
  Result<Boolean> updateCommodityPic(ItemDTO itemDto);
}
