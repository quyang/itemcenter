package com.xianzaishi.itemcenter.client.presale;

import com.xianzaishi.itemcenter.client.presale.dto.ItemPreSaleDTO;
import com.xianzaishi.itemcenter.client.presale.query.ItemPreSaleQuery;
import com.xianzaishi.itemcenter.common.result.Result;

import java.util.List;

/**
 * 预售相关的对hession服务
 *
 * @author jianmo [jianmo@xianzaishi.com]
 * @create 2016-12-24 上午 10:29
 **/
public interface ItemPreSaleService {



    /**
     * 根据预售商品的预售id获取预售商品的预售信息
     * @param preSaleId
     * @return
     */
    public Result<ItemPreSaleDTO> queryItemPreSaleById(Long preSaleId);


    /**
     * 根据预售的skuID来获取预售信息
     * @param preSaleSkuId
     * @return
     */
    public Result<ItemPreSaleDTO> queryItemPreSaleBySkuId(Long preSaleSkuId);

    /**
     *
     * 获取预售商品的全部列表
     * @return
     */
    public Result<List<ItemPreSaleDTO>> queryItemPreSales(ItemPreSaleQuery query);


    /**
     * 获取预售商品的数量
     * @param query
     * @return
     */
    public Result<Integer> queryItemPreSalesCount(ItemPreSaleQuery query);
}
