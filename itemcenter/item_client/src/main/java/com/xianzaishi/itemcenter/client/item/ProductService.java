package com.xianzaishi.itemcenter.client.item;

import java.util.List;

import com.xianzaishi.itemcenter.client.item.dto.ItemProductDTO;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 后台商品服务
 * 
 * @author zhancang
 */
public interface ProductService {

  /**
   * 发布后台商品
   * 
   * @param itemProduct
   * @return
   */
  Result<Long> insertItemProduct(ItemProductDTO itemProduct);

  /**
   * 更新后台商品
   * 
   * @param itemProduct
   * @return
   */
  Result<Boolean> updateItemProduct(ItemProductDTO itemProduct);

  /**
   * 查询一个后台商品
   * 
   * @param productId
   * @param includeSku
   * @return
   */
  Result<ItemProductDTO> queryItemProduct(Long productId, Boolean includeSku);
  
  /**
   * 批量查询采购商品
   * 
   * @param productId
   * @param includeSku
   * @return
   */
  Result<List<ItemProductDTO>> queryItemProductByIdList(List<Long> productId);
  
  /**
   * 查询一个后台商品通过sku查询
   * 
   * @param productId
   * @param includeSku
   * @return
   */
  Result<ItemProductDTO> queryItemProductBySku(Long skuId, Boolean includeSku);
  
  /**
   * 查询一个后台商品通过sku查询
   * 
   * @param productId
   * @param includeSku
   * @return
   */
  Result<ItemProductDTO> queryItemProductByItemCode(Long itemCode, Boolean includeSku);
}
