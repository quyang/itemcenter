package com.xianzaishi.itemcenter.client.sysproperty;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;

public class CacheSystemPropertyService {

  private Map<Integer, SystemConfigDTO> cacheResult = Maps.newConcurrentMap();

  private static Boolean startCache = false;
  
  private SystemPropertyService remoteSystemPropertyService;
  
  public Result<SystemConfigDTO> getSystemConfig(Integer configId,
      SystemPropertyService remoteService) {
    SystemConfigDTO result = cacheResult.get(configId);
    if (null == result) {
      if (null == remoteService) {
        return Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
            "Config remote service parameter is null");
      }
      remoteSystemPropertyService = remoteService;
      Result<SystemConfigDTO> queryResult = remoteService.getSystemConfig(configId);
      if(null != queryResult && queryResult.getSuccess() && null != queryResult.getModule()){
        cacheResult.put(configId, queryResult.getModule());
      }
      startProcess();
      return queryResult;
    } else {
      return Result.getSuccDataResult(result);
    }
  }

  /**
   * 开启同步
   */
  private void startProcess(){
    synchronized(startCache){
      if(!startCache){
        process();
      }
      startCache = true;
    }
  }
  
  /**
   * 同步过程，刷
   */
  private void process() {
    Runnable runnable = new Runnable() {
      public void run() {
        for(Integer id:cacheResult.keySet()){
          Result<SystemConfigDTO> queryResult = remoteSystemPropertyService.getSystemConfig(id);
          if(null != queryResult && queryResult.getSuccess() && null != queryResult.getModule()){
            cacheResult.put(id, queryResult.getModule());
          }
        }
      }
    };
    ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
    // 第二个参数为首次执行的延时时间，第三个参数为定时执行的间隔时间
    service.scheduleAtFixedRate(runnable, 5, 5, TimeUnit.MINUTES);
  }
}
