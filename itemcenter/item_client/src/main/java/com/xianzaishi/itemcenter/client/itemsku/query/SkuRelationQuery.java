package com.xianzaishi.itemcenter.client.itemsku.query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.google.common.base.Preconditions;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 查询前后台sku关联关系查询对象
 * 
 * @author zhancang
 */

public class SkuRelationQuery extends BaseQuery implements Serializable {

  /**
   * serializable UID版本号
   */
  private static final long serialVersionUID = 1188023645503002389L;

  /**
   * 采购skuid组
   */
  private List<Long> pskuIds;


  /**
   * 销售端skuid组
   */
  private List<Long> cskuIds;
  
  /**
   * 通过商品编码区间查询，小值
   */
  private Long pskuIdStart;
  
  /**
   * 通过商品编码区间查询，大值
   */
  private Long pskuIdEnd;

  /**
   * 供应商id
   */
  private Integer supplier;
  
  /**
   * 采购结束
   */
  private Date buyEnd;
  
  /**
   * 消费结束
   */
  private Date saleEnd;
  /**
   * 进售价关系类型
   */
  private Short relationType = RelationTypeConstants.RELATION_TYPE_NORMAL;

  private SkuRelationQuery() {

  }

  public List<Long> getPskuIds() {
    return pskuIds;
  }

  private void setPskuIds(List<Long> pskuIds) {
    Preconditions.checkNotNull(pskuIds, "pskuIds == null");
    Preconditions.checkArgument(pskuIds.size() > 0, "pskuIds.size() <= 0");
    this.pskuIds = pskuIds;
  }

  public List<Long> getCskuIds() {
    return cskuIds;
  }

  private void setCskuIds(List<Long> cskuIds) {
    Preconditions.checkNotNull(cskuIds, "cskuIds == null");
    Preconditions.checkArgument(cskuIds.size() > 0, "cskuIds.size() <= 0");
    this.cskuIds = cskuIds;
  }

  public Integer getSupplier() {
    return supplier;
  }

  private void setSupplier(Integer supplier) {
    Preconditions.checkNotNull(supplier, "supplier == null");
    Preconditions.checkArgument(supplier > 0, "supplier <= 0");
    this.supplier = supplier;
  }
  
  public Short getRelationType() {
    return relationType;
  }

  private void setRelationType(Short relationType) {
    this.relationType = relationType;
  }

  /**
   * 通过销售sku查询方法
   * 
   * @param cskuIds
   * @return
   */
  public static SkuRelationQuery getQueryByCskuIds(List<Long> cskuIds, Short relationType) {
    SkuRelationQuery sk = new SkuRelationQuery();
    sk.setCskuIds(cskuIds);
    sk.setRelationType(relationType);
    sk.setPageNum(0);
    sk.setPageSize(cskuIds.size());
    return sk;
  }
  
  /**
   * 通过销售sku查询方法
   * 
   * @param cskuIds
   * @return
   */
  public static SkuRelationQuery getQuery() {
    SkuRelationQuery sk = new SkuRelationQuery();
    return sk;
  }

  /**
   * 通过销售sku查询方法
   * 
   * @param cskuIds
   * @return
   */
  public static SkuRelationQuery getQuery(short relationType) {
    SkuRelationQuery sk = new SkuRelationQuery();
    sk.setRelationType(relationType);
    return sk;
  }
  
  /**
   * 通过采购sku查询方法
   * 
   * @param cskuIds
   * @return
   */
  public static SkuRelationQuery getQueryByPskuIds(List<Long> pskuIds, Short relationType) {
    SkuRelationQuery sk = new SkuRelationQuery();
    sk.setPskuIds(pskuIds);
    sk.setRelationType(relationType);
    sk.setPageNum(0);
    sk.setPageSize(pskuIds.size());
    return sk;
  }

  /**
   * 通过供应商查询方法
   * 
   * @param cskuIds
   * @return
   */
  public static SkuRelationQuery getQueryBySupplier(Integer supplier, Short relationType, Integer pageNum,
      Integer pageSize) {
    SkuRelationQuery sk = new SkuRelationQuery();
    sk.setSupplier(supplier);
    sk.setRelationType(relationType);
    sk.setPageSize(pageSize);
    sk.setPageNum(pageNum);
    return sk;
  }

  /**
   * 按照区间查询sku列表，包含两端sku，不包含商品信息
   * 
   * @param startSkuId
   * @param endSkuId
   * @return
   */
  public static SkuRelationQuery querySkuRelationRange(Long startId, Long endId, Short relationType,
      Integer pageSize, Integer pageNum) {
    SkuRelationQuery query = new SkuRelationQuery();

    if (startId >= endId) {
      query.setPskuIdStart(endId);
      query.setPskuIdEnd(startId);
    } else {
      query.setPskuIdStart(startId);
      query.setPskuIdEnd(endId);
    }
    query.setRelationType(relationType);
    query.setPageSize(pageSize);
    query.setPageNum(pageNum);
    return query;
  }

  public Long getPskuIdStart() {
    return pskuIdStart;
  }

  public void setPskuIdStart(Long pskuIdStart) {
    this.pskuIdStart = pskuIdStart;
  }

  public Long getPskuIdEnd() {
    return pskuIdEnd;
  }

  public void setPskuIdEnd(Long pskuIdEnd) {
    this.pskuIdEnd = pskuIdEnd;
  }
}
