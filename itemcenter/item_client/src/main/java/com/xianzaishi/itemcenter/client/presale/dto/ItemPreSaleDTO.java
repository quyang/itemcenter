package com.xianzaishi.itemcenter.client.presale.dto;

import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;

import java.util.Date;

/**
 * 预售商品的DTO
 *
 * @author jianmo
 * @create 2016-12-22 下午 8:37
 **/
public class ItemPreSaleDTO  extends ItemCommoditySkuDTO {

    /**
     * 主键
     * 表字段 : item_pre_sale.id
     */
    private Long id;

    /**
     * 预售价格
     * 表字段 : item_pre_sale.pre_sale_price
     */
    private double preSalePrice;

    /**
     * 预售开始时间
     * 表字段 : item_pre_sale.start_time
     */
    private Date startTime;

    /**
     * 预售结束时间
     * 表字段 : item_pre_sale.end_time
     */
    private Date endTime;

    /**
     * 预订人数，可以设置初始值
     * 表字段 : item_pre_sale.pre_person_nums
     */
    private Integer prePersonNums;

    /**
     * json格式的数据，表示预售商品的说明等信息
     * 表字段 : item_pre_sale.content
     */
    private String content;

    /**
     * 扩展字段1
     * 表字段 : item_pre_sale.ext_param1
     */
    private String extParam1;

    /**
     * 获取 主键 字段:item_pre_sale.id
     *
     * @return item_pre_sale.id, 主键
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置 主键 字段:item_pre_sale.id
     *
     * @param id the value for item_pre_sale.id, 主键
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取 预售价格 字段:item_pre_sale.pre_sale_price
     *
     * @return item_pre_sale.pre_sale_price, 预售价格
     */
    public double getPreSalePrice() {
        return preSalePrice;
    }

    /**
     * 设置 预售价格 字段:item_pre_sale.pre_sale_price
     *
     * @param preSalePrice the value for item_pre_sale.pre_sale_price, 预售价格
     */
    public void setPreSalePrice(double preSalePrice) {
        this.preSalePrice = preSalePrice;
    }

    /**
     * 获取 预售开始时间 字段:item_pre_sale.start_time
     *
     * @return item_pre_sale.start_time, 预售开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置 预售开始时间 字段:item_pre_sale.start_time
     *
     * @param startTime the value for item_pre_sale.start_time, 预售开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取 预售结束时间 字段:item_pre_sale.end_time
     *
     * @return item_pre_sale.end_time, 预售结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置 预售结束时间 字段:item_pre_sale.end_time
     *
     * @param endTime the value for item_pre_sale.end_time, 预售结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取 预订人数，可以设置初始值 字段:item_pre_sale.pre_person_nums
     *
     * @return item_pre_sale.pre_person_nums, 预订人数，可以设置初始值
     */
    public Integer getPrePersonNums() {
        return prePersonNums;
    }

    /**
     * 设置 预订人数，可以设置初始值 字段:item_pre_sale.pre_person_nums
     *
     * @param prePersonNums the value for item_pre_sale.pre_person_nums, 预订人数，可以设置初始值
     */
    public void setPrePersonNums(Integer prePersonNums) {
        this.prePersonNums = prePersonNums;
    }

    /**
     * 获取 json格式的数据，表示预售商品的说明等信息 字段:item_pre_sale.content
     *
     * @return item_pre_sale.content, json格式的数据，表示预售商品的说明等信息
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置 json格式的数据，表示预售商品的说明等信息 字段:item_pre_sale.content
     *
     * @param content the value for item_pre_sale.content, json格式的数据，表示预售商品的说明等信息
     */
    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    /**
     * 获取 扩展字段1 字段:item_pre_sale.ext_param1
     *
     * @return item_pre_sale.ext_param1, 扩展字段1
     */
    public String getExtParam1() {
        return extParam1;
    }

    /**
     * 设置 扩展字段1 字段:item_pre_sale.ext_param1
     *
     * @param extParam1 the value for item_pre_sale.ext_param1, 扩展字段1
     */
    public void setExtParam1(String extParam1) {
        this.extParam1 = extParam1 == null ? null : extParam1.trim();
    }


}
