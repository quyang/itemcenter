package com.xianzaishi.itemcenter.client.value.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 属性值表字段，包括基本属性值字段以及属性值字段
 * 
 * @author dongpo
 * 
 */
public class ValueDTO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = 1645161675108758744L;

  /**
   * [组] 属性值id，和标准属性值表一一对应
   */
  public final static int GROUP_VALUE_ID = 1;
  
  /**
   * [箱] 属性值id，和标准属性值表一一对应
   */
  public final static int BOX_VALUE_ID = 1;
  
  /**
   * [仅线上销售] 属性值id，和标准属性值表一一对应
   */
  public final static int SALE_TYPE_ONLINE_VALUE_ID = 84;
  
  /**
   * [仅线下销售] 属性值id，和标准属性值表一一对应
   */
  public final static int SALE_TYPE_MARKET_VALUE_ID = 85;
  
  /**
   * [同步销售] 属性值id，和标准属性值表一一对应
   */
  public final static int SALE_TYPE_ALL_VALUE_ID = 83;
  
  /**
   * 类目id
   */
  private Integer catId;

  /**
   * 属性id
   */
  private Integer propertyId;

  /**
   * 属性值id
   */
  private Integer valueId;

  /**
   * 属性值名称
   */
  private String valueData;

  /**
   * 排序值
   */
  private Short sortValue;

  /**
   * 属性值别名
   */
  private String alias;

  /**
   * 属性值状态码
   */
  private Short status;

  /**
   * 创建人
   */
  private Integer creatorId;

  /**
   * 属性值描述
   */
  private String memo;

  /**
   * 属性值创建时间
   */
  private Date gmtCreate;

  /**
   * 属性值修改时间
   */
  private Date gmtModified;

  /**
   * 属性值特征
   */
  private String features;

  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    this.catId = catId;
  }

  public Integer getPropertyId() {
    return propertyId;
  }

  public void setPropertyId(Integer propertyId) {
    this.propertyId = propertyId;
  }

  public Integer getValueId() {
    return valueId;
  }

  public void setValueId(Integer valueId) {
    this.valueId = valueId;
  }

  public String getValueData() {
    return valueData;
  }

  public void setValueData(String valueData) {
    this.valueData = valueData;
  }

  public Short getSortValue() {
    return sortValue;
  }

  public void setSortValue(Short sortValue) {
    this.sortValue = sortValue;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias == null ? null : alias.trim();
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Integer getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(Integer creatorId) {
    Preconditions.checkNotNull(creatorId, "creatorId is null");
    Preconditions.checkArgument(creatorId > 0, "creatorId must be greater than 0");
    this.creatorId = creatorId;
  }

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    this.memo = memo == null ? null : memo.trim();
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getFeatures() {
    return features;
  }

  public void setFeatures(String features) {
    this.features = features == null ? null : features.trim();
  }
}
