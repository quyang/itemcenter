package com.xianzaishi.itemcenter.client.stdcategory.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 前台标准类目对象
 * @author zhancang
 */
public class CmCategoryDTO implements Serializable{

  private static final long serialVersionUID = -1491556671073983654L;

  private Integer catId;
  
  private String name;
  
  private List<Long> backCatIdList;

  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    this.catId = catId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Long> getBackCatIdList() {
    return backCatIdList;
  }

  public void setBackCatIdList(List<Long> backCatIdList) {
    this.backCatIdList = backCatIdList;
  }

}
