package com.xianzaishi.itemcenter.client.itemsku.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 前后台sku关联关系
 * 
 * @author zhancang
 */
public class ItemSkuRelationDTO implements Serializable {

  private static final long serialVersionUID = -3365968515640298068L;

  protected Date now = new Date();
  
  /**
   * 主键id
   */
  private Long Id;
  
  /**
   * 采购skuId
   */
  private Long pskuId;

  /**
   * 销售端skuId
   */
  private Long cskuId;
  
  /**
   * 商品货号
   */
  private Long itemCode;

  /**
   * 进售属性对应比例
   */
  private Integer pcProportion;

  /**
   * 整箱成本
   */
  private Integer boxCost;
  
  /**
   * 平均移动成本，分为单位
   */
  private Integer avgCost;

  /**
   * 优惠价格平均移动成本
   */
  private Integer discountAvgCost;

  /**
   * sku采购生效开始时间
   */
  private Date buyStart = now;

  /**
   * sku采购生效结束时间，为空代表永久有效
   */
  private Date buyEnd;

  /**
   * sku销售生效开始时间
   */
  private Date saleStart = now;

  /**
   * sku销售结束时间，为空代表永久有效
   */
  private Date saleEnd;
  
  /**
   * 销售价格,以分为单位
   */
  private Integer price;

  /**
   * 采购时，针对该规格的价格
   */
  private Integer bPrice;
  /**
   * 预留
   */
  private String feature;

  /**
   * 商品销售端sku属性对象
   */
  private ItemCommoditySkuDTO itemCommoditySku;

  /**
   * 商品采购端sku属性对象
   */
  private ItemProductSkuDTO itemProductSku;

  /**
   * 进售价关系类型
   */
  private Short relationType;
  
  /**
   * 操作人
   */
  private Long processor;
  
  public Long getPskuId() {
    return pskuId == null ? 0 : pskuId;
  }

  public void setPskuId(Long pskuId) {
    this.pskuId = pskuId;
  }

  public Long getCskuId() {
    return cskuId == null ? 0 : cskuId;
  }

  public void setCskuId(Long cskuId) {
    this.cskuId = cskuId;
  }
  
  public Integer getBoxCost() {
    return boxCost;
  }

  public void setBoxCost(Integer boxCost) {
    this.boxCost = boxCost;
  }

  public Integer getAvgCost() {
    return avgCost == null ? 0 : avgCost;
  }

  public void setAvgCost(Integer avgCost) {
    this.avgCost = avgCost;
  }

  public Integer getDiscountAvgCost() {
    return discountAvgCost == null ? 0 : discountAvgCost;
  }

  public void setDiscountAvgCost(Integer discountAvgCost) {
    this.discountAvgCost = discountAvgCost;
  }

  public String getFeature() {
    return feature;
  }

  public void setFeature(String feature) {
    this.feature = feature == null ? null : feature.trim();
  }

  public Integer getPcProportion() {
    return pcProportion == null ? 1 : pcProportion;
  }

  public void setPcProportion(Integer pcProportion) {
    this.pcProportion = pcProportion;
  }

  public ItemCommoditySkuDTO getItemCommoditySku() {
    return itemCommoditySku;
  }

  public void setItemCommoditySku(ItemCommoditySkuDTO itemCommoditySku) {
    this.itemCommoditySku = itemCommoditySku;
  }

  public ItemProductSkuDTO getItemProductSku() {
    return itemProductSku;
  }

  public void setItemProductSku(ItemProductSkuDTO itemProductSku) {
    this.itemProductSku = itemProductSku;
  }

  public Long getItemCode() {
    return itemCode;
  }

  public void setItemCode(Long itemCode) {
    this.itemCode = itemCode;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Integer getbPrice() {
    return bPrice;
  }

  public void setbPrice(Integer bPrice) {
    this.bPrice = bPrice;
  }

  public Date getNow() {
    return now;
  }

  public void setNow(Date now) {
    this.now = now;
  }

  public Long getId() {
    return Id;
  }

  public void setId(Long id) {
    Id = id;
  }

  public Date getBuyStart() {
    return buyStart;
  }

  public void setBuyStart(Date buyStart) {
    this.buyStart = buyStart;
  }

  public Date getBuyEnd() {
    return buyEnd;
  }

  public void setBuyEnd(Date buyEnd) {
    this.buyEnd = buyEnd;
  }

  public Date getSaleStart() {
    return saleStart;
  }

  public void setSaleStart(Date saleStart) {
    this.saleStart = saleStart;
  }

  public Date getSaleEnd() {
    return saleEnd;
  }

  public void setSaleEnd(Date saleEnd) {
    this.saleEnd = saleEnd;
  }

  public Short getRelationType() {
    return relationType;
  }

  public void setRelationType(Short relationType) {
    this.relationType = relationType;
  }
  
  public Long getProcessor() {
    return processor;
  }

  public void setProcessor(Long processor) {
    this.processor = processor;
  }



  /**
   * sku进售价关系类型
   * 
   * @author zhancang
   */
  public static class RelationTypeConstants implements Serializable {

    private static final long serialVersionUID = -3464182024053122453L;

    /**
     * 普通进售价
     */
    public static final Short RELATION_TYPE_NORMAL = 0;

    /**
     * 优惠进售价
     */
    public static final Short RELATION_TYPE_DISCOUNT = 1;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return RELATION_TYPE_NORMAL.equals(parameter) || RELATION_TYPE_DISCOUNT.equals(parameter) ;
    }
  }
}
