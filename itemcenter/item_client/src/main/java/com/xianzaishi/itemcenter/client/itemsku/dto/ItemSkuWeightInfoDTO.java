package com.xianzaishi.itemcenter.client.itemsku.dto;

import java.io.Serializable;

public class ItemSkuWeightInfoDTO implements Serializable{
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 979627649881262175L;
  
  private Long skuId;

  /**
   * 价格，分为单位<br/>
   * 针对采购商品，代表采购价格<br/>
   * 针对标品销售商品，代表标品一个销售单位价格<br/>
   * 针对称重商品，代表一份商品的价格（标品化后的商品价格，比如一个柚子的价格，一个火龙果的价格。估算值，需要称重后多退少补），称重单价为unitPrice<br/>
   * 价格精确到分为单位<br/>
   */
  private Integer price;

  /**
   * 优惠后成本价格,以分为单位，折扣价，对应price
   */
  private Integer discountPrice;
  
  /**
   * 称重总价格
   */
  private Integer priceTotal;
  
  /**
   * 称重总优惠价格
   */
  private Integer discountPriceTotal;
  
  /**
   * 称重商品相当于标品的份数
   */
  private String count;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Integer getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(Integer discountPrice) {
    this.discountPrice = discountPrice;
  }

  public Integer getPriceTotal() {
    return priceTotal;
  }

  public void setPriceTotal(Integer priceTotal) {
    this.priceTotal = priceTotal;
  }

  public Integer getDiscountPriceTotal() {
    return discountPriceTotal;
  }

  public void setDiscountPriceTotal(Integer discountPriceTotal) {
    this.discountPriceTotal = discountPriceTotal;
  }

  public String getCount() {
    return count;
  }

  public void setCount(String count) {
    this.count = count;
  }
  
}
