package com.xianzaishi.itemcenter.client.itemsku.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 商品标具体含义
 * @author zhancang
 */
public class SkuTagDetail implements Serializable {

  private static final long serialVersionUID = -53676542844578922L;

  /**
   * 代表线上场景
   */
  public static final short CHANNEL_ONLINE = 1;
  
  /**
   * 代表线下场景
   */
  public static final short CHANNEL_OFFLINE = 2;
  
  /**
   * 代表全场场景
   */
  public static final short CHANNEL_ALL = 0;
  /**
   * 标id，参考SkuTagDefinitionConstants定义
   */
  private String tagId;
  
  /**
   * 该规则适用渠道
   */
  private Short channel;

  /**
   * 标名称，多个标名称最后合并到tagInfo
   */
  private String tagName;

  /**
   * 标生效开始日期
   */
  private Date begin;

  /**
   * 标生效结束日期
   */
  private Date end;

  /**
   * 标规则
   */
  private String tagRule;

  public String getTagId() {
    return tagId;
  }

  public void setTagId(String tagId) {
    this.tagId = tagId;
  }

  public Short getChannel() {
    return channel;
  }

  public void setChannel(Short channel) {
    this.channel = channel;
  }

  public String getTagName() {
    return tagName;
  }

  public void setTagName(String tagName) {
    this.tagName = tagName;
  }

  public Date getBegin() {
    return begin;
  }

  public void setBegin(Date begin) {
    this.begin = begin;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public String getTagRule() {
    return tagRule;
  }

  public void setTagRule(String tagRule) {
    this.tagRule = tagRule;
  }
}
