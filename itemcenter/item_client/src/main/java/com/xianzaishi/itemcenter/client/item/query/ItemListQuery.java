package com.xianzaishi.itemcenter.client.item.query;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 商品查询条件，包商品id组、标题、价格、店id、页码以及是否返回全部字段等
 * 
 * @author dongpo
 */

public class ItemListQuery extends BaseQuery implements Serializable {

  /**
   * serializable UID版本号
   */
  private static final long serialVersionUID = 1188043665503002389L;

  private static final String ORDER_BY_CREATE = "gmt_create";

  private static final String ORDER_BY_MODIFIED = "gmt_modified";

  private static final String ORDER_BY_PRICE = "discount_price";

  private static final String ORDER_BY_ID = "item_id";
  
  private static final String ORDER_ASC = "ASC";

  private static final String ORDER_DESC = "DESC";

  /**
   * 商品id组
   */
  private List<Long> itemIds;
  
  /**
   * 设置查询商品状态
   */
  private List<Short> status;
  

  /**
   * 商品销售状态
   */
  private List<Short> saleStatus;
  
  /**
   * 商品标题
   */
  private String itemTitle;

  /**
   * 商品价格区间:左值
   */
  private Integer startPrice;

  /**
   * 商品价格区间:右值
   */
  private Integer endPrice;
  
  /**
   * 通过后台叶子类目查询
   */
  private List<Integer> categoryIds;

  /**
   * 通过多个后台二级类目id来查询商品
   */
  private List<Integer> cmCat2Ids;
  
  /**
   * 通过前台一级类目查询
   */
  private Integer frontCatId;

  /**
   * 商店id
   */
  // private Integer shopId;

  /**
   * 是否包含sku属性。<br/>
   * hasItemSku=true,会包含每一个商品的所有sku，尽量注意查询量大小和返回数据大小
   */
  private Boolean hasItemSku = false;

  /**
   * 是否返回商品全部属性。<br/>
   * returnAll=false 返回部分属实时返回的对象是ItemBaseDTO对象实例。<br/>
   * returnAll=true 返回全部对象时，返回的对象是ItemDTO对象实例
   */
  private Boolean returnAll = false;

  /**
   * 排序方式
   */
  private String orderBy = ORDER_BY_ID;

  /**
   * 是否按升序来排序
   */
  private String order = ORDER_DESC;
  
  /**
   * 结果是否按照cmCat2Id顺序输出
   */
  private Boolean inOrder = false;
  
  /**
   * 是否搜索干预
   */
  private Boolean searchIntervention = false;

  public List<Long> getItemIds() {
    return itemIds;
  }

  public void setItemIds(List<Long> itemIds) {
    for (Long itemId : itemIds) {
      if (itemId <= 0) {
        throw new IllegalArgumentException("itemId must be greater than 0");
      }
    }
    this.itemIds = itemIds;
    this.setPageNum(0);
    this.setPageSize(itemIds.size());
  }

  public Boolean getHasItemSku() {
    return hasItemSku;
  }

  public void setHasItemSku(Boolean hasItemSku) {
    this.hasItemSku = hasItemSku;
  }

  public Boolean getReturnAll() {
    return returnAll;

  }

  public void setReturnAll(Boolean returnAll) {
    this.returnAll = returnAll;
  }

  public String getOrder() {
    return order;
  }

  private void setOrder(String order) {
    this.order = order;
  }

  public String getOrderBy() {
    return orderBy;
  }

  private void setOrderBy(String orderBy) {
    this.orderBy = orderBy;
  }

  public String getItemTitle() {
    return itemTitle;
  }

  public void setItemTitle(String itemTitle) {
    this.itemTitle = itemTitle;
  }


  public Integer getStartPrice() {
    return startPrice;
  }

  public void setStartPrice(Integer startPrice) {
    if (startPrice < 0) {
      throw new IllegalArgumentException("startPrice can not be smaller than 0");
    }
    this.startPrice = startPrice;
  }

  public Integer getEndPrice() {
    return endPrice;
  }

  public void setEndPrice(Integer endPrice) {
    if (endPrice < startPrice && endPrice != 0) {
      throw new IllegalArgumentException("endPrice can not be smaller than startPrice");
    }
    this.endPrice = endPrice;
  }

  public Integer getFrontCatId() {
    return frontCatId;
  }

  public void setFrontCatId(Integer frontCatId) {
    this.frontCatId = frontCatId;
  }
  
  public List<Integer> getCategoryIds() {
    return categoryIds;
  }

  public void setCategoryIds(List<Integer> categoryIds) {
    this.categoryIds = categoryIds;
  }

  public List<Integer> getCmCat2Ids() {
    return cmCat2Ids;
  }

  public void setCmCat2Ids(List<Integer> cmCat2Ids) {
    this.cmCat2Ids = cmCat2Ids;
  }

  public List<Short> getStatus() {
    return status;
  }
  

  /**
   * 设置单值
   * @param status
   */
  public void setStatus(Short singleStatus) {
    if(null != singleStatus){
      this.status = Lists.newArrayList(singleStatus);
    }
  }
  
  public void setStatusList(List<Short> status) {
    this.status = status;
  }

  public List<Short> getSaleStatus() {
    return saleStatus;
  }

  /**
   * @see 设置单值
   * @param status
   */
  public void setSaleStatus(Short singleSaleStatus) {
    if(null != singleSaleStatus){
      this.saleStatus = Lists.newArrayList(singleSaleStatus);
    }
  }

  public void setSaleStatusList(List<Short> saleStatus) {
    this.saleStatus = saleStatus;
  }

  
  public void setOrderByCreateAsc() {
    this.setOrderBy(ORDER_BY_CREATE);
    this.setOrder(ORDER_ASC);
  }

  public void setOrderByModifiedAsc() {
    this.setOrderBy(ORDER_BY_MODIFIED);
    this.setOrder(ORDER_ASC);
  }

  public void setOrderByPriceAsc() {
    this.setOrderBy(ORDER_BY_PRICE);
    this.setOrder(ORDER_ASC);
  }
  
  public void setOrderByIdAsc() {
    this.setOrderBy(ORDER_BY_ID);
    this.setOrder(ORDER_ASC);
  }

  public void setOrderByCreateDesc() {
    this.setOrderBy(ORDER_BY_CREATE);
    this.setOrder(ORDER_DESC);
  }

  public void setOrderByModifiedDesc() {
    this.setOrderBy(ORDER_BY_MODIFIED);
    this.setOrder(ORDER_DESC);
  }

  public void setOrderByPriceDesc() {
    this.setOrderBy(ORDER_BY_PRICE);
    this.setOrder(ORDER_DESC);
  }
  
  public void setOrderByIdDesc() {
    this.setOrderBy(ORDER_BY_ID);
    this.setOrder(ORDER_DESC);
  }

  public Boolean getInOrder() {
    return inOrder;
  }

  public void setInOrder(Boolean inOrder) {
    this.inOrder = inOrder;
  }

  public Boolean getSearchIntervention() {
    return searchIntervention;
  }

  public void setSearchIntervention(Boolean searchIntervention) {
    this.searchIntervention = searchIntervention;
  }
  
}
