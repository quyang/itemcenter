package com.xianzaishi.itemcenter.client.stdcategory;

import java.util.List;

import com.xianzaishi.itemcenter.client.stdcategory.dto.CmCategoryDTO;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 前台标准类目接口
 * @author zhancang
 */
public interface CmCategoryService {

  public Result<List<CmCategoryDTO>> getdefaultCategoryList();
  
}
