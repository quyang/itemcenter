package com.xianzaishi.itemcenter.client.itemsku.query;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 查询后台商品sku参数对象
 * 
 * @author dongpo
 */

public class SkuQuery extends BaseQuery implements Serializable {

  /**
   * serializable UID版本号
   */
  private static final long serialVersionUID = 1188043645503002389L;

  /**
   * skuid组
   */
  private List<Long> skuIds;

  /**
   * 商品id
   */
  private Long itemId;

  /**
   * 商品名称
   */
  private String title;

  /**
   * 商品编码
   */
  private List<Long> sku69Codes;

  /**
   * 供应商id
   */
  private Integer supplier;

  /**
   * 商品标题
   */
  private String skuTitle;

  /**
   * skuid开始id（必须小于endSkuId）
   */
  private Long startSkuId;

  /**
   * skuid结束id（必须大于startSkuId）
   */
  private Long endSkuId;

  /**
   * 将前台需要属性翻译成可见属性
   */
  private Boolean translateFrontPropertyToHumanLanguage = false;
  
  /**
   * 数据修改时间
   */
  private Date gmtModified;
  
  /**
   * 结果是否按照skuId顺序输出
   */
  private Boolean inOrderBySkuId = false;
  
  /**
   * 根据商品状态查询
   */
  private List<Short> statuses;

  public List<Long> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<Long> skuIds) {
    this.skuIds = skuIds;
  }

  public Long getItemId() {
    return itemId;
  }

  private void setItemId(Long itemId) {
    Preconditions.checkNotNull(itemId, "itemId == null");
    Preconditions.checkArgument(itemId > 0, "itemId <= 0");
    this.itemId = itemId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<Long> getSku69Codes() {
    return sku69Codes;
  }

  public void setSku69Codes(List<Long> sku69Codes) {
    this.sku69Codes = sku69Codes;
  }

  public Integer getSupplier() {
    return supplier;
  }

  public void setSupplier(Integer supplier) {
    this.supplier = supplier;
  }

  public String getSkuTitle() {
    return skuTitle;
  }

  public void setSkuTitle(String skuTitle) {
    this.skuTitle = skuTitle;
  }

  public Long getStartSkuId() {
    return startSkuId;
  }

  public void setStartSkuId(Long startSkuId) {
    this.startSkuId = startSkuId;
  }

  public Long getEndSkuId() {
    return endSkuId;
  }

  public void setEndSkuId(Long endSkuId) {
    this.endSkuId = endSkuId;
  }

  public Boolean getTranslateFrontPropertyToHumanLanguage() {
    return translateFrontPropertyToHumanLanguage;
  }

  private void setTranslateFrontPropertyToHumanLanguage(
      Boolean translateFrontPropertyToHumanLanguage) {
    this.translateFrontPropertyToHumanLanguage = translateFrontPropertyToHumanLanguage;
  }
  
  public Date getGmtModified() {
    return gmtModified;
  }
  
  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }
  
  public Boolean getInOrderBySkuId() {
    return inOrderBySkuId;
  }

  public void setInOrderBySkuId(Boolean inOrderBySkuId) {
    this.inOrderBySkuId = inOrderBySkuId;
  }

  public List<Short> getStatuses() {
    return statuses;
  }

  public void setStatuses(List<Short> statuses) {
    this.statuses = statuses;
  }

  public SkuQuery() {

  }
  

  /**
   * 默认分页查询
   * 
   * @param title
   * @param pageSize
   * @param pageNum
   * @return
   */
  public static SkuQuery querySku(Integer pageSize, Integer pageNum) {
    SkuQuery query = new SkuQuery();
    query.setPageSize(pageSize);
    query.setPageNum(pageNum);
    return query;
  }

  /**
   * 按照标题查询sku
   * 
   * @param title
   * @param pageSize
   * @param pageNum
   * @return
   */
  public static SkuQuery querySkuByTitle(String title, Integer pageSize, Integer pageNum) {
    SkuQuery query = new SkuQuery();
    query.setSkuTitle(title);
    query.setPageSize(pageSize);
    query.setPageNum(pageNum);
    return query;
  }

  /**
   * 按照skuid列表查询sku
   * 
   * @param skuIds
   * @return
   */
  public static SkuQuery querySkuBySkuIds(List<Long> skuIds) {
    SkuQuery query = new SkuQuery();
    query.setSkuIds(skuIds);
    query.setPageSize(skuIds.size());
    query.setPageNum(0);
    return query;
  }

  /**
   * 按照sku供应商查询sku
   * 
   * @param supplier
   * @param pageSize
   * @param pageNum
   * @return
   */
  public static SkuQuery querySkuBySupplier(Integer supplier) {
    SkuQuery query = new SkuQuery();
    query.setSupplier(supplier);
    return query;
  }

  /**
   * 按照sku供应商以及sku id查询sku
   * 
   * @param supplier
   * @param pageSize
   * @param pageNum
   * @return
   */
  public static SkuQuery querySkuBySkuIdAndSupplier(Long skuId, Integer supplier) {
    SkuQuery query = new SkuQuery();
    query.setSkuIds(Arrays.asList(skuId));
    query.setSupplier(supplier);
    return query;
  }

  
  /**
   * 按照sku供应商查询sku
   * 
   * @param supplier
   * @param pageSize
   * @param pageNum
   * @return
   */
  public static SkuQuery querySkuBySupplierAndTitle(Integer supplier, String title, int pageSize,
      int pageNum) {
    SkuQuery query = new SkuQuery();
    query.setSupplier(supplier);
    if(StringUtils.isNotEmpty(title)){
      query.setSkuTitle(title);
    }
    query.setPageNum(pageNum);
    query.setPageSize(pageSize);
    return query;
  }

  /**
   * 按照商品id查询sku
   * 
   * @param itemId
   * @return
   */
  public static SkuQuery querySkuByItem(Long itemId) {
    SkuQuery query = new SkuQuery();
    query.setItemId(itemId);
    return query;
  }

  /**
   * 推荐使用 querySkuById
   * 
   * @param skuCode
   * @return
   */
  @Deprecated
  public static SkuQuery queryBaseSkuBySku(Long skuId) {
    SkuQuery query = new SkuQuery();
    query.setSkuIds(Arrays.asList(skuId));
    return query;
  }

  /**
   * 根据skuId查询sku信息
   * 
   * @param skuCode
   * @return
   */
  public static SkuQuery querySkuById(Long skuId) {
    SkuQuery query = new SkuQuery();
    query.setSkuIds(Arrays.asList(skuId));
    return query;
  }

  /**
   * 按照sku69码查询sku基础信息，主要结算和店内查询标类商品信息
   * 
   * @param skuCode
   * @return
   */
  public static SkuQuery querySkuBySku69Code(Long sku69Code) {
    SkuQuery query = new SkuQuery();
    query.setSku69Codes(Arrays.asList(sku69Code));
    return query;
  }
  
  /**
   * 按照sku69码查询sku基础信息，主要结算和店内查询标类商品信息
   * 
   * @param skuCode
   * @return
   */
  public static SkuQuery querySkuBySku69CodeList(List<Long> sku69Code) {
    SkuQuery query = new SkuQuery();
    query.setSku69Codes(sku69Code);
    return query;
  }

//  /**
//   * 按照skuPlu码查询sku基础信息，主要结算和店内查询非标类商品信息
//   * 
//   * @param skuCode
//   * @return
//   */
//  public static SkuQuery querySkuBySkuPluCode(Long skuPluCode) {
//    SkuQuery query = new SkuQuery();
//    query.setSkuPluCode(skuPluCode);
//    return query;
//  }
  
//  /**
//   * 按照商品3个码任意设置一个id，系统区分不同商品
//   * 
//   * @param pluCodeOr69CodeOrCode
//   * @return
//   */
//  public static SkuQuery querySkuByPluCodeOr69CodeOrCode(long pluId,long sku69Code,long productId) {
//    if(pluId <= 0 && sku69Code <= 0 && productId <= 0){
//      Preconditions.checkArgument(false, "pluCodeOr69CodeOrCode is error number");
//    }
//    SkuQuery query = new SkuQuery();
//    
//    if(pluId > 0){//69码范围
//      query.setSkuPluCode(pluId);
//    }else if(sku69Code > 0){//69码范围
//      query.setSku69Code(sku69Code);
//    }else if(productId >= 0){//商品编码范围
//      query.setItemId(productId);
//    }else{
//      Preconditions.checkArgument(false, "pluCodeOr69CodeOrCode is error number");
//    }
//    return query;
//  }

  /**
   * 按照区间查询sku列表，包含两端sku，不包含商品信息
   * 
   * @param startSkuId
   * @param endSkuId
   * @return
   */
  public static SkuQuery querySkuRange(Long startSkuId, Long endSkuId, Integer pageSize,
      Integer pageNum) {
    SkuQuery query = new SkuQuery();
    if (startSkuId == endSkuId) {
      return querySkuBySkuIds(Arrays.asList(startSkuId));
    }

    if (startSkuId > endSkuId) {
      query.setStartSkuId(endSkuId);
      query.setEndSkuId(startSkuId);
    } else {
      query.setStartSkuId(startSkuId);
      query.setEndSkuId(endSkuId);
    }
    query.setPageSize(pageSize);
    query.setPageNum(pageNum);
    return query;
  }
  
  /**
   * 根据修改时间来查询数据
   * @param modifiedTime
   * @return
   */
  public static SkuQuery queryByModifiedTime(Date modifiedTime){
    SkuQuery query = new SkuQuery();
    query.setGmtModified(modifiedTime);
    return query;
  }

  /**
   * 根据title查找
   * @param title
   * @return
   */
  public static SkuQuery queryCountByTitle(String title){
    SkuQuery query = new SkuQuery();
    query.setTitle(title);
    return query;
  }
}
