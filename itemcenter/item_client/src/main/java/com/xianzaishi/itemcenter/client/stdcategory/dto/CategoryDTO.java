package com.xianzaishi.itemcenter.client.stdcategory.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;

/**
 * 后台类目表字段
 * 
 * @author dongpo
 * 
 */
public class CategoryDTO implements Serializable {
  
  private static final String SPLIT_FEATURES_GROUT_KEY = "\n";

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -4264615449208277931L;

  /**
   * 类目id
   */
  private Integer catId;

  /**
   * 类目代码号，和主键区分，由于是按照运营指定类目层级规律，防止类目迁移等影响主键
   */
  private Long catCode;
  /**
   * 类目名字
   */
  private String catName;

  /**
   * 父类目id
   */
  private Integer parentId;

  /**
   * 是否是子叶类目，默认0表示不是，1表示是子叶类目
   */
  private Short leaf = 0;

  /**
   * 类目状态码，默认1表示正常，-1表示删除：2表示屏蔽
   */
  private Short status = 1;

  /**
   * 类目创建时间
   */
  private Date gmtCreate;

  /**
   * 类目修改时间
   */
  private Date gmtModified;

  /**
   * 类目属性
   */
  private String features;

  /**
   * 类目类型
   */
  private Short catType;

  /**
   * 类目描述
   */
  private String memo;

  /**
   * 类目创建人
   */
  private Integer creatorId;

  /**
   * 类目排序值
   */
  private Short sortValue;

  /**
   * 类目属性
   */
  private List<PropertyDTO> categoryProperties;

  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    this.catId = catId;
  }

  public Long getCatCode() {
    return catCode;
  }

  public void setCatCode(Long catCode) {
    this.catCode = catCode;
  }

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public Integer getParentId() {
    return parentId;
  }

  public void setParentId(Integer parentId) {
    this.parentId = parentId;
  }

  public Short getLeaf() {
    return leaf;
  }

  public void setLeaf(Short isLeaf) {
    this.leaf = isLeaf;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getFeatures() {
    return features;
  }

  public void setFeatures(String features) {
    this.features = features;
  }

  public Short getCatType() {
    return catType;
  }

  public void setCatType(Short catType) {
    this.catType = catType;
  }

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  public Integer getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(Integer creatorId) {
    Preconditions.checkNotNull(creatorId, "creatorId is null");
    Preconditions.checkArgument(creatorId > 0, "creatorId must be greater than 0");
    this.creatorId = creatorId;
  }

  public Short getSortValue() {
    return sortValue;
  }

  public void setSortValue(Short sortValue) {
    this.sortValue = sortValue;
  }

  public List<PropertyDTO> getCategoryProperties() {
    return categoryProperties;
  }

  public void setCategoryProperties(List<PropertyDTO> categoryProperties) {
    this.categoryProperties = categoryProperties;
  }

  /**
   * 取属性字段kv对
   * 
   * @return
   */
  public Map<String, String> queryPropertyMap() {
    Map<String, String> proM = Maps.newHashMap();
    if (StringUtils.isNotEmpty(this.getFeatures())) {
      String featuresArrays[] = this.getFeatures().split(SPLIT_FEATURES_GROUT_KEY);
      for (String tmp : featuresArrays) {
        if (StringUtils.isNotEmpty(tmp)) {
          String keyValue[] = tmp.split("=");
          if (keyValue.length != 2) {
            continue;
          } else {
            proM.put(keyValue[0], keyValue[1]);
          }
        }
      }
    }
    return proM;
  }

}
