package com.xianzaishi.itemcenter.client.itemsku.dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemSpecificationDTO;
import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;

/**
 * 商品销售端sku属性
 * 
 * @author zhancang
 * 
 */
public class ItemCommoditySkuDTO extends ItemSkuDTO {

  private static final long serialVersionUID = 5771999756328920529L;

  /**
   * 销售类型
   */
  private Short saleStatus = SaleStatusConstants.SALE_ONLINE_AND_MARKET;

  /**
   * 销量
   */
  private Integer quantity;

  /**
   * 加工成本（加工类使用）
   */
  private Integer cost;

  /**
   * sku类型
   */
  private Short skuType = SkuTypeConstants.ORIGINAL_GOODS_SKU;

  /**
   * 促销信息
   */
  private String promotionInfo;

  public Short getSaleStatus() {
    return saleStatus == null ? SaleStatusConstants.SALE_ONLINE_AND_MARKET : saleStatus;
  }

  public void setSaleStatus(Short saleStatus) {
    Preconditions.checkNotNull(saleStatus, "saleStatus == null");
    Preconditions.checkArgument(SaleStatusConstants.isCorrectParameter(saleStatus),
        "saleStatus not in(1,2,3)");
    this.saleStatus = saleStatus;
  }

  public Integer getQuantity() {
    return quantity == null ? 0 : quantity;
  }

  public void setQuantity(Integer quantity) {
    Preconditions.checkNotNull(quantity, "quantity == null");
    Preconditions.checkArgument(quantity >= 0, "quantity <= 0");
    this.quantity = quantity;
  }

  public Integer getCost() {
    return cost == null ? 0 : cost;
  }

  public void setCost(Integer cost) {
    Preconditions.checkNotNull(cost, "cost == null");
    Preconditions.checkArgument(cost >= 0, "cost <= 0");
    this.cost = cost;
  }

  public Short getSkuType() {
    return skuType == null ? SkuTypeConstants.ORIGINAL_GOODS_SKU : skuType;
  }

  public void setSkuType(Short skuType) {
    Preconditions.checkNotNull(skuType, "skuType == null");
    Preconditions
        .checkArgument(SkuTypeConstants.isCorrectParameter(skuType), "skuType not in(1,2)");
    this.skuType = skuType;
  }

  public String getPromotionInfo() {
    return promotionInfo;
  }

  public void setPromotionInfo(String promotionInfo) {
    this.promotionInfo = promotionInfo;
  }

  public Map<String, String> getFrontProperty() {
    return frontProperty;
  }

  public void setFrontProperty(Map<String, String> frontProperty) {
    this.frontProperty = frontProperty;
  }

  /**
   * 设置规格属性
   * 
   * @param specification
   */
  public void addSpecification(String specification) {
    if (StringUtils.isEmpty(specification)) {
      return;
    }
    this.addFeature(SPECIFICATION_FEATURE_KEY, specification);
  }

  /**
   * 设置规格属性
   * 
   * @param specification
   */
  public void addUnSpecification(String specification) {
    if (StringUtils.isEmpty(specification)) {
      return;
    }
    this.addFeature(UNSTANDERDSPECIFICATION_FEATURE_KEY, specification);
  }
  
  /**
   * 设置对应采购的销售规格，
   * 
   * @param saleUnitType
   */
  public void addSaleUnitType(Short saleUnitType) {
    if (null == saleUnitType) {
      return;
    }
    this.addFeature(SALEUNITTYPE_FEATURE_KEY, String.valueOf(saleUnitType));
  }

  /**
   * 设置销售单位的中文
   * 
   * @param saleUnitStr
   */
  public void addSaleUnitStr(String saleUnitStr) {
    if (null == saleUnitStr) {
      return;
    }
    this.addFeature(SALEUNITSTR_FEATURE_KEY, saleUnitStr);
  }

  /**
   * 设置对应销售的最小起订量
   * 
   * @param saleUnitType
   */
  public void addSaleRangeNum(Integer saleRangeNum) {
    if (null == saleRangeNum) {
      return;
    }
    this.addFeature(SALERANGE_FEATURE_KEY, String.valueOf(saleRangeNum));
  }
  
  /**
   * 查询对应boom单id
   * 
   * @return
   */
  public Long queryBoomId() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(BOOMID_FEATURE_KEY);
      if (StringUtils.isNotEmpty(value) && StringUtils.isNumeric(value)) {
        return Long.valueOf(value);
      }
    }
    return 0L;
  }

  /**
   * 设置boom单id
   * 
   * @param boomId
   */
  public void addBoomId(Long boomId) {
    this.addFeature(BOOMID_FEATURE_KEY, String.valueOf(boomId));
  }

  /**
   * 查询商品成分列表
   * 
   * @return
   */
  public List<String> queryElement() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(ELEMENT_KEY);
      if (StringUtils.isNotEmpty(value)) {
        String[] result = value.split(SPLIT_SEPARATOR_KEY);
        return Arrays.asList(result);
      }
    }
    return null;
  }

  /**
   * 查询商品成分字符串
   * 
   * @return
   */
  public String queryElementString() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(ELEMENT_KEY);
      if (StringUtils.isNotEmpty(value)) {
        return value;
      }
    }
    return null;
  }

  /**
   * 设置商品成分
   * 
   * @param element
   */
  public void addElement(String element) {
    this.addFeature(ELEMENT_KEY, element);
  }

  /**
   * 查询关联商品skuId
   * 
   * @return
   */
  public List<ItemSpecificationDTO> queryRelatedItems() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(RELATED_ITEMS_KEY);
      if (StringUtils.isNotEmpty(value)) {
        String[] midValue = value.split("\\[");
        List<String> relatedItemsString = Lists.newArrayList();
        for (String v : midValue) {
          if (StringUtils.isNotEmpty(v)) {
            relatedItemsString.add(v.split("\\]")[0]);
          }
        }
        List<ItemSpecificationDTO> itemSpecificationDTOs = Lists.newArrayList();
        for (String relatedItemString : relatedItemsString) {
          ItemSpecificationDTO itemSpecificationDTO = new ItemSpecificationDTO();
          String[] result = relatedItemString.split(SPLIT_POUND_KEY);// 以#分隔符分割的数组
          String name = result[0];// #前面为展示规格的名字
          if(result.length > 1){
            List<String> relatedItems = Arrays.asList(result[1].split(SPLIT_SEPARATOR_KEY));// #后面的值以\分隔符分割出来的数组
            itemSpecificationDTO.setName(name);
            itemSpecificationDTO.setValues(relatedItems);
            itemSpecificationDTOs.add(itemSpecificationDTO);
          }
        }
        return itemSpecificationDTOs;
      }
    }
    return null;
  }

  /**
   * 设置关联商品skuId
   * 
   * @param relatedItems
   */
  public void addRelatedItems(String relatedItems) {
    this.addFeature(RELATED_ITEMS_KEY, relatedItems);
  }

  /**
   * 查询展示规格列表
   * 
   * @return
   */
  public List<ItemSpecificationDTO> queryDisplaySpecification() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(DISPLAY_SPECIFICATIONS_KEY);
      if (StringUtils.isNotEmpty(value)) {
        String[] midValue = value.split("\\[");
        List<String> displaySpecifications = Lists.newArrayList();
        for (String v : midValue) {
          if (StringUtils.isNotEmpty(v)) {
            displaySpecifications.add(v.split("\\]")[0]);
          }
        }
        List<ItemSpecificationDTO> itemSpecificationDTOs = Lists.newArrayList();
        for (String displaySpecification : displaySpecifications) {
          ItemSpecificationDTO itemSpecificationDTO = new ItemSpecificationDTO();
          String[] result = displaySpecification.split(SPLIT_POUND_KEY);// 以#分隔符分割的数组
          String name = result[0];// #前面为展示规格的名字
          if(result.length > 1){
            List<String> specifications = Arrays.asList(result[1].split(SPLIT_SEPARATOR_KEY));// #后面的值以\分隔符分割出来的数组
            itemSpecificationDTO.setName(name);
            itemSpecificationDTO.setValues(specifications);
            itemSpecificationDTOs.add(itemSpecificationDTO);
          }
        }
        return itemSpecificationDTOs;
      }
    }
    return null;
  }

  /**
   * 查询展示规格字符串
   * 
   * @return
   */
  public String queryDisplaySpecificationString() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(DISPLAY_SPECIFICATIONS_KEY);
      if (StringUtils.isNotEmpty(value)) {
        return value;
      }
    }
    return null;
  }

  public void addDisplaySpecification(String displaySpecifications) {
    this.addFeature(DISPLAY_SPECIFICATIONS_KEY, displaySpecifications);
  }

  /**
   * 查询产地属性，从非标准属性直接翻译
   * 
   * @return
   */
  public String queryOriginpace() {
    Map<String, String> properM = super.queryUnStandardCatPropertyValueMap();
    String place = properM.get(String.valueOf(PropertyDTO.ORIGINPACE_PROPERTY_ID));
    if(getSkuId() >= 40000 && getSkuId() < 50000){
      return "自产";
    }
    place = place == null ? "" : place;
    return place;
  }
  
  /**
   * 获取商品标内容json
   * @return
   */
  public String queryTagContent(){
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(TAG_CONTENT_KEY);
      if (StringUtils.isNotEmpty(value)) {
        return value;
      }
    }
    return null; 
  }
  
  /**
   * 添加标内容
   * @param tagContent
   */
  public void addTagContent(String tagContent){
    this.addFeature(TAG_CONTENT_KEY, tagContent);
  }

  private void addFeature(String key, String value) {
    if(StringUtils.isEmpty(key)){
      return;
    }
    Map<String, String> featureM = queryFeatureMap();
    if(StringUtils.isEmpty(value)){
      featureM.remove(key);
    }else{
      featureM.put(key, value);
    }
    String feature = "";
    for (String tmpKey : featureM.keySet()) {
      String tmpValue = featureM.get(tmpKey);
      feature = feature + tmpKey + SPLIT_KVPROPERTY_KEY + tmpValue + SPLIT_PROPERTY_GROUT_KEY;
    }
    this.setFeature(feature);
  }

  /**
   * sku销售方式
   * 
   * @author zhancang
   */
  public static class SkuTypeConstants implements Serializable {

    private static final long serialVersionUID = -3464189024053122453L;

    /**
     * 非加工类
     */
    public static final Short ORIGINAL_GOODS_SKU = 1;

    /**
     * 加工类
     */
    public static final Short SECONDARY_PROCESSING_SKU = 2;

    /**
     * /** 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return ORIGINAL_GOODS_SKU.equals(parameter) || SECONDARY_PROCESSING_SKU.equals(parameter);
    }
  }

  /**
   * sku销售方式
   * 
   * @author zhancang
   */
  public static class SkuSaleUnitConstants implements Serializable {

    private static final long serialVersionUID = -3464189024056122453L;

    /**
     * 按采购规格销售
     */
    public static final Short BOX = 1;

    /**
     * 按采购规格内，分组销售
     */
    public static final Short GROUP = 2;

    /**
     * 按采购规格内，最小件单位销售，称重就是按照称重单位销售
     */
    public static final Short UNIT = 3;

    /**
     * 称重商品，按照自然属性销售，销售单位取 [称重类自然属性]
     */
    public static final Short NATURAL = 4;
  }
}
