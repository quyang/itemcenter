package com.xianzaishi.itemcenter.client.itemsku.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Maps;

public class ItemSkuTagsDTO implements Serializable {

  /**
   * serial version uid
   */
  private static final long serialVersionUID = 2761482556901324510L;

  protected static final String SPLIT_PROPERTY_GROUT_KEY = "\n";
  protected static final String SPLIT_PROPERTY_KEY = ";";
  protected static final String SPLIT_KVPROPERTY_KEY = "&";

  /**
   * sku id
   */
  private Long skuId;

  /**
   * 商品特殊标详情
   */
  private String feature;

  /**
   * 商品标
   */
  private Integer tags;

  /**
   * 库存接口
   */
  private Integer inventory;

  /**
   * 是否可加购
   */
  private Boolean mayPlus = true;

  /**
   * 是否可下单
   */
  private Boolean mayOrder = true;

  /**
   * 是否限购
   */
  private Boolean mayLimitBuySku = false;

  /**
   * 是否有优惠
   */
  private Boolean mayPromotionSku = false;

  /**
   * 是否有足够库存
   */
  private Boolean mayInventoryEnough = true;

  /**
   * 各种标合并后的商品提示信息
   */
  private String tagInfo;

  /**
   * 商品特征详细信息
   */
  private List<SkuTagDetail> skuTagDetailList;


  public Integer getInventory() {
    return inventory;
  }

  public void setInventory(Integer inventory) {
    this.inventory = inventory;
  }

  public String getTagInfo() {
    return tagInfo;
  }

  public void setTagInfo(String tagInfo) {
    this.tagInfo = tagInfo;
  }

  public List<SkuTagDetail> getSkuTagDetailList() {
    return skuTagDetailList;
  }

  public void setSkuTagDetailList(List<SkuTagDetail> skuTagDetailList) {
    this.skuTagDetailList = skuTagDetailList;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public String getFeature() {
    return feature;
  }

  public void setFeature(String feature) {
    this.feature = feature;
  }

  public Integer getTags() {
    return tags;
  }

  public void setTags(Integer tags) {
    this.tags = tags;
  }

  public Boolean getMayPlus() {
    return mayPlus;
  }

  public void setMayPlus(Boolean mayPlus) {
    this.mayPlus = mayPlus;
  }

  public Boolean getMayOrder() {
    return mayOrder;
  }

  public void setMayOrder(Boolean mayOrder) {
    this.mayOrder = mayOrder;
  }

  public Boolean getMayLimitBuySku() {
    return mayLimitBuySku;
  }

  public void setMayLimitBuySku(Boolean mayLimitBuySku) {
    this.mayLimitBuySku = mayLimitBuySku;
  }

  public Boolean getMayPromotionSku() {
    return mayPromotionSku;
  }

  public void setMayPromotionSku(Boolean mayPromotionSku) {
    this.mayPromotionSku = mayPromotionSku;
  }

  public Boolean getMayInventoryEnough() {
    return mayInventoryEnough;
  }

  public void setMayInventoryEnough(Boolean mayInventoryEnough) {
    this.mayInventoryEnough = mayInventoryEnough;
  }

  /**
   * 取属性字段kv对
   * 
   * @return
   */
  public Map<String, String> queryFeatureMap() {
    Map<String, String> proM = Maps.newHashMap();
    if (StringUtils.isNotEmpty(this.getFeature())) {
      String featureArrays[] = this.getFeature().split(SPLIT_PROPERTY_GROUT_KEY);
      for (String tmp : featureArrays) {
        if (StringUtils.isNotEmpty(tmp)) {
          String keyValue[] = tmp.split(SPLIT_KVPROPERTY_KEY);
          if (keyValue.length != 2) {
            continue;
          } else {
            proM.put(keyValue[0], keyValue[1]);
          }
        }
      }
    }
    return proM;
  }

  /**
   * 商品标签详细定义
   * 
   * @author zhancang
   */
  public static class SkuTagDefinitionConstants implements Serializable {

    private static final long serialVersionUID = 8261588100967729151L;

    public static final String PROMOTION_TAG = "1";// 商品优惠标

    public static final String SEND_ONE_BUY_MORE_THAN_TWO = "3";// 商品超过2件赠送一件（包含2件）

    public static final String BUY_SKU_SEND_COUPON = "4";// 买商品赠送，具体赠送优惠券id参考优惠券id

    public static final String BUY_SKU_DISCOUNT_RATE = "5";// 商品满N件折扣M，具体折扣规则参考优惠中心

    public static final String BUY_LIMIT = "6";// 商品限购标

    public static final String DES_TAG = "7";// 商品打描述标

    public static final String NOT_ALLOW_PROMOTION = "8";// 商品打无优惠标

    public static final String NOT_ALLOW_SHOPPINGCART = "9";// 商品不允许加购物车

    public static final String NOT_ALLOW_CHARGE = "10";// 商品不允许交易

    public static final String NOT_ENOUGH_INVENTORY = "11";// 商品库存不足

    public static final String LIMITED_TIME_PREFERENCE = "12";// 限时特惠标

    public static final String DISTRIBUTION_ALONE = "15";// 商品独立配送标
    
    public static final String DISTRIBUTION_FREE = "16";// 包邮标

    // 下面的tagId供解析时用，不用于存储
    public static final String BUY_DISCOUNT_99_40 = "满99减40";

    public static final String BUY_DISCOUNT_40_20 = "满40减20";// 商品满40-20

    public static final String ONLINE_DISCOUNT = "商品线上优惠标";// 线上优惠标

    public static final String OFFLINE_DISCOUNT = "商品线下优惠标";// 线下优惠标

    public static final String BUY_DISCOUNT_COOKED_FOOD_60_20 = "年货节熟食满60减20";// 年货节熟食满60减20

    public static final String BUY_DISCOUNT_VEGETABLE_FRUIT_198_30 = "年货节蔬果满198减30";

    public static final String BUY_DISCOUNT_VEGETABLE_FRUIT_299_50 = "年货节蔬果满299减50";

    public static final String BUY_DISCOUNT_SEA_FOOD_99_10 = "年货节海鲜满99减10";

    public static final String BUY_DISCOUNT_SEA_FOOD_198_30 = "年货节海鲜满198减30";

    public static final String BUY_DISCOUNT_SEA_FOOD_299_50 = "年货节海鲜满299减50";

    public static final String BUY_DISCOUNT_SEA_FOOD_499_100 = "年货节海鲜满499减100";
  }

}
