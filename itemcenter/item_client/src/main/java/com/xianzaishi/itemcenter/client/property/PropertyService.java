package com.xianzaishi.itemcenter.client.property;

import java.util.List;

import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 类目属性接口，包括单个查询和批量查询、插入属性以及更新属性信息
 * 
 * @author dongpo
 * 
 */
public interface PropertyService {


  /**
   * 按照类目id查询类目属性
   * 
   * @param query
   * @return
   */
  public Result<List<PropertyDTO>> queryPropertyByCatId(Integer catId, Boolean includeValue);

  /**
   * 按照类目id和属性id查询类目属性
   * 
   * @param catId
   * @param propertyId
   * @return
   */
  public Result<PropertyDTO> queryPropertyByPropertyId(Integer catId, Integer propertyId,
      Boolean hasValue);

  /**
   * 按照类目id和属性名称查询类目属性
   * 
   * @param catId
   * @param name
   * @return
   */
  public Result<PropertyDTO> queryPropertyByPropertyName(Integer catId, String name,
      Boolean hasValue);

  /**
   * 按照类目和tags来查询类目属性
   * 
   * @param catId
   * @param tags
   * @return
   */
  public Result<List<PropertyDTO>> queryPropertyByTags(Integer catId, Integer tags);

  /**
   * 根据属性id查询属性名称
   * 
   * @param propertyId
   * @return
   */
  public Result<String> queryPropertyNameById(Integer propertyId);

  /**
   * 增加类目属性
   * 
   * @param propertyDto
   * @return
   */
  public Result<Integer> insertProperty(PropertyDTO propertyDto);

  /**
   * 更新类目属性
   * 
   * @param propertyDto
   * @return
   */
  public Result<Boolean> updateProperty(PropertyDTO propertyDto);


}
