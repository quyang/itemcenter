package com.xianzaishi.itemcenter.client.property.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.xianzaishi.itemcenter.client.value.dto.ValueDTO;

/**
 * 属性表字段，包括基本属性字段以及属性字段
 * 
 * @author dongpo
 * 
 */
public class PropertyDTO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -1389560557543377455L;

  /**
   * [件装数]属性id，和标准属性表一一对应
   */
  public final static int COUNT_PROPERTY_ID = 6;

  /**
   * [件装单位属性]属性id，和标准属性表一一对应
   */
  public final static int COUNTUNIT_PROPERTY_ID = 7;

  /**
   * [称重比例]属性id，和标准属性表一一对应
   */
  public final static int NATURALRATE_PROPERTY_ID = 38;

  /**
   * [称重类自然属性]属性id，和标准属性表一一对应
   */
  public final static int NATURALUNIT_PROPERTY_ID = 37;

  /**
   * [件装规格]属性id，和标准属性表一一对应
   */
  public final static int GOURP_PROPERTY_ID = 8;

  /**
   * [件装属性]属性id，和标准属性表一一对应
   */
  public final static int GOURPUNIT_PROPERTY_ID = 9;

  /**
   * [采购规格]属性id，和标准属性表一一对应
   */
  public final static int BUYUNIT_PROPERTY_ID = 10;

  /**
   * [销售规格]属性id，和标准属性表一一对应
   */
  public final static int SALEUNIT_PROPERTY_ID = 11;

  /**
   * [69码，外部条码]属性id，和标准属性表一一对应
   */
  public final static int SKU69CODE_PROPERTY_ID = 5;

  /**
   * 销售外包装长度
   */
  public final static int SALE_PROPERTY_LEN = 12;

  /**
   * 销售外包装宽带
   */
  public final static int SALE_PROPERTY_WIDTH = 13;

  /**
   * 销售外包装高度
   */
  public final static int SALE_PROPERTY_HEIGHT = 14;

  /**
   * [销售类型，线上线下销售等]属性id，和标准属性表一一对应
   */
  public final static int SALETYPE_PROPERTY_ID = 31;

  /**
   * [产地]属性id，和标准属性表一一对应
   */
  public final static int ORIGINPACE_PROPERTY_ID = 4;

  /**
   * 保质期对应的属性id
   */
  public final static int QUALITY_PROPERTY_ID = 19;
  
  /**
   * 标签类型的属性id
   */
  public final static int PRICETAG_PROPERTY_ID = 33;
  
  /**
   * 等级类型的属性id
   */
  public final static int LEVEL_PROPERTY_ID = 18;
  
  /**
   * 存储类型的属性id
   */
  public final static int STORAGETYPE_PROPERTY_ID = 28;
  
  /**
   * 食用类型属性id
   */
  public final static int EATTYPE_PROPERTY_ID = 42;
  
  /**
   * 退货类型的属性id
   */
  public final static int RETURN_PROPERTY_ID = 29;
  
  /**
   * 定义前台可见属性值列表
   */
  public static final Set<Integer> FRONT_PROPERTY_NAME = new HashSet<Integer>() {
    private static final long serialVersionUID = 1L;
    {
      add(1);// 产地
      add(2);// 品牌
      add(3);// 国家
    }
  };
  /**
   * 类目id
   */
  private Integer catId;

  /**
   * 类目属性id
   */
  private Integer propertyId;

  /**
   * 属性别名
   */
  private String propertyNameAlias;

  /**
   * 类目属性创建人
   */
  private Integer creatorId;

  /**
   * 排序值
   */
  private Short sortValue;

  /**
   * 属性创建时间
   */
  private Date gmtCreate;

  /**
   * 属性修改时间
   */
  private Date gmtModified;

  /**
   * 类目属性状态
   */
  private Short status;

  /**
   * 类目特征值
   */
  private String features;


  /**
   * 从右向左，第一位代表是否必须属性，第二位代表是否下拉属性，第三位代表是否可输入属性，第4位代表app端是否显示该属性。。。。。。通过类目id+tags位运算查询
   */
  private Integer tags;

  /**
   * 后续扩展使用。。。。。。
   */
  private Integer formatTags;

  /**
   * 类目描述信息
   */
  private String memo;

  /**
   * 类目属性值
   */
  private List<ValueDTO> categoryPropertyValues;

  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    Preconditions.checkArgument(catId > 0, "catId must be greater than 0");
    this.catId = catId;
  }

  public Integer getPropertyId() {
    return propertyId;
  }

  public void setPropertyId(Integer propertyId) {
    Preconditions.checkArgument(propertyId > 0, "propertyId must be greater than 0");
    this.propertyId = propertyId;
  }

  public String getPropertyNameAlias() {
    return propertyNameAlias;
  }

  public void setPropertyNameAlias(String propertyNameAlias) {
    this.propertyNameAlias = propertyNameAlias == null ? null : propertyNameAlias.trim();
  }

  public Integer getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(Integer creatorId) {
    Preconditions.checkArgument(creatorId > 0, "creatorId must be greater than 0");
    this.creatorId = creatorId;
  }

  public Short getSortValue() {
    return sortValue;
  }

  public void setSortValue(Short sortValue) {
    this.sortValue = sortValue;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getFeatures() {
    return features;
  }

  public void setFeatures(String features) {
    this.features = features == null ? null : features.trim();
  }

  public Integer getTags() {
    return tags;
  }

  public void setTags(Integer tags) {
    this.tags = tags;
  }

  public Integer getFormatTags() {
    return formatTags;
  }

  public void setFormatTags(Integer formatTags) {
    this.formatTags = formatTags;
  }

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  public List<ValueDTO> getCategoryPropertyValues() {
    return categoryPropertyValues;
  }

  public void setCategoryPropertyValues(List<ValueDTO> categoryPropertyValues) {
    this.categoryPropertyValues = categoryPropertyValues;
  }

  /**
   * 是否必选属性
   * 
   * @return
   */
  public boolean isRequireProperty() {
    return (null != this.tags && (tags.intValue() & 1) == 1);
  }

  /**
   * 是否下拉属性
   * 
   * @return
   */
  public boolean isSelectProperty() {
    return (null != this.tags && (tags.intValue() & 2) == 2);
  }

  /**
   * 是否可输入属性
   * 
   * @return
   */
  public boolean isInputProperty() {
    return (null != this.tags && (tags.intValue() & 4) == 4);
  }

}
