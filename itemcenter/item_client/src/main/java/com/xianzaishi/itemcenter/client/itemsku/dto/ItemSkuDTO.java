package com.xianzaishi.itemcenter.client.itemsku.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.ItemStatusConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO.SkuSaleUnitConstants;
import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;

/**
 * 商品sku基础属性
 * 
 * @author zhancang
 */
public class ItemSkuDTO implements Serializable {

  private static final long serialVersionUID = 8631154073266150758L;

  protected static final String SALEUNITTYPE_FEATURE_KEY = "su";// 进售关系类型
  protected static final String SPECIFICATION_FEATURE_KEY = "sp";// 规格信息
  protected static final String UNSTANDERDSPECIFICATION_FEATURE_KEY = "usp";// 包含非标类规格信息
  protected static final String SALEUNITSTR_FEATURE_KEY = "st";// 销售单位中文
  protected static final String SALERANGE_FEATURE_KEY = "sr";// 最小起订量
  protected static final String BOOMID_FEATURE_KEY = "boomid";
  protected static final String ELEMENT_KEY = "element";
  protected static final String DISPLAY_SPECIFICATIONS_KEY = "displaySpecifications";// 展示规格，代表加工类商品口味、糖分、甜度等可选属性
  protected static final String RELATED_ITEMS_KEY = "relatedItems";// 关联sku,代表加奶、加咖啡等复合sku关联
  protected static final String TAG_CONTENT_KEY = "tg";// 商品标内容

  protected static final String STANDARD_PROPERTY_KEY = "p";
  protected static final String UNSTANDARD_PROPERTY_KEY = "s";
  protected static final String SPLIT_PROPERTY_GROUT_KEY = "\n";
  protected static final String SPLIT_PROPERTY_KEY = ";";
  protected static final String SPLIT_KVPROPERTY_KEY = "&";
  protected static final String SPLIT_SEPARATOR_KEY = "\\\\";
  protected static final String SPLIT_POUND_KEY = "#";

  protected Date now = new Date();

  /**
   * 后台skuID，系统使用，唯一标识一个sku
   */
  private Long skuId;

  /**
   * 对应后台采购\销售商品id
   */
  private Long itemId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 店内skuCode，供店内人员辨识，按照类目层级设置
   */
  private Long skuCode;

  /**
   * 69码
   */
  private Long sku69code;

  /**
   * plu码
   */
  private Long skuPluCode;

  /**
   * 对应库存数量
   */
  private Integer inventory;

  /**
   * 价格，分为单位<br/>
   * 针对采购商品，代表采购价格<br/>
   * 针对标品销售商品，代表标品一个销售单位价格<br/>
   * 针对称重商品，代表一份商品的价格（标品化后的商品价格，比如一个柚子的价格，一个火龙果的价格。估算值，需要称重后多退少补），称重单价为unitPrice<br/>
   * 价格精确到分为单位<br/>
   */
  private Integer price;

  /**
   * 优惠后成本价格,以分为单位，折扣价，对应price
   */
  private Integer discountPrice;

  /**
   * 对于标品，和price相同<b/> 
   * 对于非标品，自然类属性单个价格（1个榴莲500元，500对应price.1kg榴莲20元，20对应unitPrice）<b/> 
   * 称重类型单位公斤<b/>
   * 价格精确到分为单位<br/>
   */
  private Integer unitPrice;
  
  /**
   * 对于标品，和discountPrice相同<b/> 
   * 对于非标品，自然类属性单个优惠价格（1个榴莲500元，500对应price.1kg榴莲优惠价20元，20对应unitDiscountPrice）<b/> 
   * 称重类型单位公斤<b/>
   * 价格精确到分为单位<br/>
   */
  private Integer discountUnitPrice;

  /**
   * sku属性，继承来自发布时的各种sku属性
   */
  private String property;

  /**
   * sku状态,针对采购商品sku，包含有采购状态
   */
  private Short status = ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES;

  /**
   * 创建时间
   */
  private Date gmtCreate = now;

  /**
   * 
   */
  private Date gmtModified = now;

  /**
   * 线下sku单位，取标准属性库，比如kg、合、包、斤等<br/>
   * 针对采购sku，属于采购进货单位<br/>
   * 针对销售sku，属于销售sku单位<br/>
   * 针对称重类型的，统一为斤
   */
  private Integer skuUnit;

  /**
   * 线上销售规格，对应skuUnit
   */
  @SuppressWarnings("unused")
  private Integer onlineSkuUnit;
  
  /**
   * sku数量，针对采购商品无意义。针对销售商品代表箱规
   */
  private float skuCount;

  /**
   * 自然称重类商品，最小起订量，也就是一次增加多少重量，以克为单位数量
   */
  protected int saleRangeNum;

  /**
   * sku标题
   */
  private String title;

  /**
   * 属性
   */
  private String feature;

  /**
   * 商品打的各种标
   */
  private Integer tags;

  /**
   * 审核失败原因
   */
  private String checkedFailedReason;

  /**
   * 保质期多少天
   */
  @SuppressWarnings("unused")
  private Integer quality;

  /**
   * 价签类型
   */
  @SuppressWarnings("unused")
  private Integer priceTagType;

  /**
   * 是否称重类型的
   */
  private Boolean isSteelyardSku;

  /**
   * 产品等级
   */
  @SuppressWarnings("unused")
  private Integer level;

  /**
   * 储存方式
   */
  @SuppressWarnings("unused")
  private Integer storageType;

  /**
   * 食用方式
   */
  private Integer eatType;

  /**
   * 是否允许退货
   */
  @SuppressWarnings("unused")
  private Boolean allowReturn;

  /**
   * 销售单位的中文描述
   */
  @SuppressWarnings("unused")
  private String saleUnitStr;

  /**
   * 标品化后的商品销售规格描述，和price对应
   */
  @SuppressWarnings("unused")
  private String specification;

  /**
   * 包含非标品的销售规格，和unitPrice对应
   */
  @SuppressWarnings("unused")
  private String unstandardSpecification;
  
  /**
   * sku规格数量，对于非称重类型的sku规格数量无意义。<br/>
   * 针对称重类型代表一份具体的数量
   */
  @SuppressWarnings("unused")
  private int skuSpecificationNum;

  /**
   * sku规格单位，对于非称重类型的sku规格单位无意义。<br/>
   * 针对称重类型代表一份具体的数量对应的单位，前期全部为g
   */
  @SuppressWarnings("unused")
  private int skuSpecificationUnit;
  
  /**
   * 审核人列表
   */
  private List<Integer> checkerList;

  /**
   * 所有属性翻译后的属性值
   */
  private Map<String, String> propertyInfo = Maps.newHashMap();

  /**
   * 前台可见属性合集
   */
  protected Map<String, String> frontProperty = Maps.newHashMap();

  public Long getSkuId() {
    return skuId == null ? 0 : skuId;
  }

  public void setSkuId(Long skuId) {
    Preconditions.checkNotNull(skuId, "skuId == null");
    Preconditions.checkArgument(skuId >= 0, "skuId <= 0");
    this.skuId = skuId;
  }

  public Long getItemId() {
    return itemId == null ? 0 : itemId;
  }

  public void setItemId(Long itemId) {
    Preconditions.checkNotNull(itemId, "itemId == null");
    Preconditions.checkArgument(itemId >= 0, "itemId <= 0");
    this.itemId = itemId;
  }

  public Long getSkuCode() {
    if (null == skuCode) {
      return 0L;
    }
    return skuCode;
  }

  public void setSkuCode(Long skuCode) {
    if (null != skuCode && skuCode > 0) {
      this.skuCode = skuCode;
    }
  }

  public Long getSku69code() {
    return sku69code;
  }

  public void setSku69code(Long sku69code) {
    this.sku69code = sku69code;
  }

  public Long getSkuPluCode() {
    return skuPluCode;
  }

  public void setSkuPluCode(Long skuPluCode) {
    this.skuPluCode = skuPluCode;
  }

  public Integer getSupplierId() {
    return supplierId == null ? 0 : supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    Preconditions.checkNotNull(supplierId, "supplierId == null");
    Preconditions.checkArgument(supplierId >= 0, "supplierId <= 0");
    this.supplierId = supplierId;
  }

  public Integer getInventory() {
    return inventory == null ? 0 : inventory;
  }

  public void setInventory(Integer inventory) {
    Preconditions.checkNotNull(inventory, "inventory == null");
    Preconditions.checkArgument(inventory >= 0, "inventory < 0");
    this.inventory = inventory;
  }

  public Integer getPrice() {
    return price == null ? 0 : price;
  }

  @SuppressWarnings("unused")
  private BigDecimal priceYuan;

  /**
   * 只为支持反射调用
   * 
   * @param priceYuan
   */
  @Deprecated
  public void setPriceYuan(BigDecimal priceYuan) {}

  /**
   * 获取元为单位的价格
   * 
   * @return
   */
  public BigDecimal getPriceYuan() {
    return new BigDecimal(getPrice()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN);
  }

  /**
   * 推荐使用getPriceYuan()
   * 
   * @return
   */
  @Deprecated
  public BigDecimal queryPriceYuan() {
    return getPriceYuan();
  }

  @SuppressWarnings("unused")
  private String priceYuanString;

  /**
   * 只为支持反射调用
   * 
   * @param priceYuanString
   */
  @Deprecated
  public void setPriceYuanString(String priceYuanString) {}

  /**
   * 获取元为单位的价格，字符串表示
   * 
   * @return
   */
  public String getPriceYuanString() {
    return getPriceYuan().toString();
  }

  /**
   * 推荐使用getPriceYuanString
   * 
   * @return
   */
  @Deprecated
  public String queryPriceYuanString() {
    return getPriceYuanString();
  }

  /**
   * @param price
   */
  public void setPrice(Integer price) {
    this.price = price;
  }

  public Integer getDiscountPrice() {
    if (null != discountPrice && discountPrice > 0) {
      return discountPrice;
    }
    return getPrice();
  }

  @SuppressWarnings("unused")
  private BigDecimal discountPriceYuan;

  /**
   * 只为支持反射调用
   * @param discountPriceYuan
   */
  @Deprecated
  public void setDiscountPriceYuan(BigDecimal discountPriceYuan) {}

  /**
   * 获取元为单位的优惠价格
   * 
   * @return
   */
  public BigDecimal getDiscountPriceYuan() {
    return new BigDecimal(getDiscountPrice()).divide(new BigDecimal(100), 2,
        BigDecimal.ROUND_HALF_EVEN);
  }

  /**
   * 推荐使用 getDiscountPriceYuan
   * 
   * @return
   */
  @Deprecated
  public BigDecimal queryDiscountPriceYuan() {
    return getDiscountPriceYuan();
  }

  @SuppressWarnings("unused")
  private String discountPriceYuanString;

  /**
   * 只为支持反射调用
   */
  @Deprecated
  public void setDiscountPriceYuanString(String discountPriceYuanString) {}

  /**
   * 获取元为单位的优惠价格，字符串表示
   * 
   * @return
   */
  public String getDiscountPriceYuanString() {
    return getDiscountPriceYuan().toString();
  }

  /**
   * 推荐使用  getDiscountPriceYuanString
   * @return
   */
  @Deprecated
  public String queryDiscountPriceYuanString() {
    return getDiscountPriceYuanString();
  }

  public void setDiscountPrice(Integer discountPrice) {
    this.discountPrice = discountPrice;
  }

  public Integer getUnitPrice() {
    if (null != unitPrice && unitPrice > 0) {
      return unitPrice;
    }
    return getPrice();
  }

  public void setUnitPrice(Integer unitPrice) {
    this.unitPrice = unitPrice;
  }

  public Integer getDiscountUnitPrice() {
    if(null != discountUnitPrice && discountUnitPrice > 0){
      return discountUnitPrice;
    }
    return getUnitPrice();
  }

  public void setDiscountUnitPrice(Integer discountUnitPrice) {
    this.discountUnitPrice = discountUnitPrice;
  }

  public String getProperty() {
    return property;
  }

  public void setProperty(String property) {
    this.property = property == null ? null : property.trim();
  }

  /**
   * 取属性字段kv对
   * 
   * @return
   */
  private Map<String, String> queryPropertyMap() {
    Map<String, String> proM = Maps.newHashMap();
    if (StringUtils.isNotEmpty(this.getProperty())) {
      String propertyArrays[] = this.getProperty().split(SPLIT_PROPERTY_GROUT_KEY);
      for (String tmp : propertyArrays) {
        if (StringUtils.isNotEmpty(tmp)) {
          String keyValue[] = tmp.split("=");
          if (keyValue.length != 2) {
            continue;
          } else {
            proM.put(keyValue[0], keyValue[1]);
          }
        }
      }
    }
    return proM;
  }

  private void addPropertyMap(Map<String, String> input) {
    Map<String, String> tmpM = queryPropertyMap();
    tmpM.putAll(input);

    String propery = "";
    for (String key : tmpM.keySet()) {
      String value = input.get(key);
      propery = propery + key + "=" + value + SPLIT_PROPERTY_GROUT_KEY;
    }
    this.setProperty(propery);
  }

  /**
   * 解析类目标准属性kv值
   * 
   * @return
   */
  public Map<String, String> queryStandardCatPropertyValueMap() {
    String standarPropertyStr = queryPropertyMap().get(STANDARD_PROPERTY_KEY);
    if (StringUtils.isEmpty(standarPropertyStr)) {
      return Maps.newHashMap();
    }
    String tmpPropertyArray[] = standarPropertyStr.split(SPLIT_PROPERTY_KEY);
    Map<String, String> proM = Maps.newHashMap();
    for (String kv : tmpPropertyArray) {
      String tmpkvArray[] = kv.split(SPLIT_KVPROPERTY_KEY);
      if (tmpkvArray.length != 2) {
        continue;
      }
      proM.put(tmpkvArray[0], tmpkvArray[1]);
    }
    return proM;
  }

  /**
   * 解析类目非标准输入属性kv值
   * 
   * @return
   */
  public Map<String, String> queryUnStandardCatPropertyValueMap() {
    String unstandarPropertyStr = queryPropertyMap().get(UNSTANDARD_PROPERTY_KEY);
    if (StringUtils.isEmpty(unstandarPropertyStr)) {
      return Maps.newHashMap();
    }
    String tmpPropertyArray[] = unstandarPropertyStr.split(SPLIT_PROPERTY_KEY);
    Map<String, String> proM = Maps.newHashMap();
    for (String kv : tmpPropertyArray) {
      String tmpkvArray[] = kv.split(SPLIT_KVPROPERTY_KEY);
      if (tmpkvArray.length != 2) {
        continue;
      }
      proM.put(tmpkvArray[0], tmpkvArray[1]);
    }
    return proM;
  }

  /**
   * 前台增加一个选择的属性&属性值对
   */
  public void addSelectCatProperty(int propertyId, String valueId) {
    if (propertyId <= 0 || StringUtils.isEmpty(valueId)) {
      return;
    }

    Map<String, String> kvProperty = this.queryStandardCatPropertyValueMap();
    kvProperty.put(String.valueOf(propertyId), valueId);
    String kvStr = "";
    for (String tmp : kvProperty.keySet()) {
      String tmpK = tmp;
      String tmpV = kvProperty.get(tmpK);
      if (StringUtils.isNotEmpty(tmpK) && StringUtils.isNotEmpty(tmpV)) {
        kvStr = kvStr + tmpK + SPLIT_KVPROPERTY_KEY + tmpV + SPLIT_PROPERTY_KEY;
      }
    }
    Map<String, String> propertyM = this.queryPropertyMap();
    propertyM.put(STANDARD_PROPERTY_KEY, kvStr);
    this.addPropertyMap(propertyM);
  }

  /**
   * 前台增加一个输入的属性&属性值对
   */
  public void addInputCatProperty(int propertyId, String value) {
    if (propertyId <= 0 || StringUtils.isEmpty(value)) {
      return;
    }
    Map<String, String> kvProperty = this.queryUnStandardCatPropertyValueMap();
    kvProperty.put(String.valueOf(propertyId), value);
    String kvStr = "";
    for (String tmp : kvProperty.keySet()) {
      String tmpK = tmp;
      String tmpV = kvProperty.get(tmpK);
      if (StringUtils.isNotEmpty(tmpK) && StringUtils.isNotEmpty(tmpV)) {
        kvStr = kvStr + tmpK + SPLIT_KVPROPERTY_KEY + tmpV + SPLIT_PROPERTY_KEY;
      }
    }
    Map<String, String> propertyM = this.queryPropertyMap();
    propertyM.put(UNSTANDARD_PROPERTY_KEY, kvStr);
    this.addPropertyMap(propertyM);
  }

  public Short getStatus() {
    return status == null ? ItemStatusConstants.ITEM_STATUS_PASS_AND_UP_SHELVES : status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument(ItemStatusConstants.isCorrectParameter(status),
        "status not in(0,1,2,4)");
    this.status = status;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    Preconditions.checkNotNull(gmtCreate, "gmtCreate == null");
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    Preconditions.checkNotNull(gmtModified, "gmtModified == null");
    this.gmtModified = gmtModified;
  }

  public Integer getSkuUnit() {
    return skuUnit == null ? 0 : skuUnit;
  }

  public void setSkuUnit(Integer skuUnit) {
    Preconditions.checkNotNull(skuUnit, "skuUnit == null");
    Preconditions.checkArgument(skuUnit >= 0, "skuUnit < 0");
    this.skuUnit = skuUnit;
  }

  public Integer getOnlineSkuUnit() {
    if(getIsSteelyardSku()){
      return 17;//线上散称数据，统一规格为份
    }else{
      return skuUnit;
    }
  }

  public void setOnlineSkuUnit(Integer onlineSkuUnit) {
    this.onlineSkuUnit = onlineSkuUnit;
  }

  public float getSkuCount() {
    return skuCount;
  }

  public void setSkuCount(float skuCount) {
    this.skuCount = skuCount;
  }

  public int getSaleRangeNum() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(SALERANGE_FEATURE_KEY);
      if(null == value  || !StringUtils.isNumeric(value)){
        return 1;
      }
      return Integer.valueOf(value);
    }
    return 0;
  }

  public void setSaleRangeNum(int saleRangeNum) {
    this.saleRangeNum = saleRangeNum;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public String getFeature() {
    return feature;
  }

  public void setFeature(String feature) {
    this.feature = feature == null ? null : feature.trim();
  }

  public Integer getTags() {
    return tags;
  }

  public void setTags(Integer tags) {
    this.tags = tags;
  }

  public String getCheckedFailedReason() {
    return checkedFailedReason;
  }

  public void setCheckedFailedReason(String checkedFailedReason) {
    this.checkedFailedReason = checkedFailedReason;
  }

  public List<Integer> getCheckerList() {
    return checkerList == null ? Collections.<Integer>emptyList() : checkerList;
  }

  public void setCheckerList(List<Integer> checkerList) {
    this.checkerList = checkerList;
  }

  protected Map<String, String> getPropertyInfo() {
    return propertyInfo;
  }

  public void setPropertyInfo(Map<String, String> propertyInfo) {
    this.propertyInfo = propertyInfo;
  }

  public int getSkuSpecificationNum() {
    if(getIsSteelyardSku()){
      return getSaleRangeNum();
    }else{
      return 1;
    }
  }

  public void setSkuSpecificationNum(int skuSpecificationNum) {
    this.skuSpecificationNum = skuSpecificationNum;
  }

  public int getSkuSpecificationUnit() {
    if(getIsSteelyardSku()){
      return 71;
    }else{
      return skuUnit;
    }
  }

  public void setSkuSpecificationUnit(int skuSpecificationUnit) {
    this.skuSpecificationUnit = skuSpecificationUnit;
  }

  /**
   * 取属性字段kv对
   * 
   * @return
   */
  protected Map<String, String> queryFeatureMap() {
    Map<String, String> proM = Maps.newHashMap();
    if (StringUtils.isNotEmpty(this.getFeature())) {
      String featureArrays[] = this.getFeature().split(SPLIT_PROPERTY_GROUT_KEY);
      for (String tmp : featureArrays) {
        if (StringUtils.isNotEmpty(tmp)) {
          String keyValue[] = tmp.split(SPLIT_KVPROPERTY_KEY);
          if (keyValue.length != 2) {
            continue;
          } else {
            proM.put(keyValue[0], keyValue[1]);
          }
        }
      }
    }
    return proM;
  }

  public void setSpecification(String specification) {
    this.specification = specification;
  }

  /**
   * 查询规格属性
   * 
   * @return
   */
  public String getSpecification() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(SPECIFICATION_FEATURE_KEY);
      return value;
    }
    return "";
  }

  public String querySpecification() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(SPECIFICATION_FEATURE_KEY);
      return value;
    }
    return "";
  }

  public String getUnstandardSpecification() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(UNSTANDERDSPECIFICATION_FEATURE_KEY);
      return value;
    }
    return "";
  }

  public void setUnstandardSpecification(String unstandardSpecification) {
    this.unstandardSpecification = unstandardSpecification;
  }

  /**
   * 查询对应采购的销售单位，属于对应关系，代表A进A出，或者A进B出等方式
   * 
   * @return
   */
  public Short querySaleUnitType() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(SALEUNITTYPE_FEATURE_KEY);
      if (StringUtils.isNotEmpty(value) && StringUtils.isNumeric(value)) {
        return Short.valueOf(value);
      }
    }
    return SkuSaleUnitConstants.BOX;
  }

  /**
   * 获取销售单位的中文描述
   * 
   * @return
   */
  public String getSaleUnitStr() {
    Map<String, String> featureM = this.queryFeatureMap();
    if (CollectionUtils.isNotEmpty(featureM.keySet())) {
      String value = featureM.get(SALEUNITSTR_FEATURE_KEY);
      return value;
    }
    return "";
  }

  public void setSaleUnitStr(String saleUnitStr) {
    this.saleUnitStr = saleUnitStr;
  }

  /**
   * 翻译保质期
   * 
   * @return
   */
  public Integer getQuality() {
    int quality = 0;
    Map<String, String> unstandM = this.queryUnStandardCatPropertyValueMap();
    String qualityStr = unstandM.get(String.valueOf(PropertyDTO.QUALITY_PROPERTY_ID));
    if (StringUtils.isNotEmpty(qualityStr) && StringUtils.isNumeric(qualityStr)) {
      quality = Integer.valueOf(qualityStr);
    }else{
      String qualityStr2 = unstandM.get("41");
      if(StringUtils.isNotBlank(qualityStr2)){
        qualityStr2 = qualityStr2.replace("天", "");
        if (StringUtils.isNotEmpty(qualityStr2) && StringUtils.isNumeric(qualityStr2)) {
          quality = Integer.valueOf(qualityStr2);
        }
      }
    }
    
    if(quality <= 1){
      return 0;
    }else{
      return quality;
    }
  }

  public void setQuality(Integer quality) {
    this.quality = quality;
  }

  /**
   * 翻译价签类型
   * 
   * @return
   */
  public Integer getPriceTagType() {
    Map<String, String> standM = this.queryStandardCatPropertyValueMap();
    String priceTagStr = standM.get(String.valueOf(PropertyDTO.PRICETAG_PROPERTY_ID));
    if (StringUtils.isNotEmpty(priceTagStr) && StringUtils.isNumeric(priceTagStr)) {
      return Integer.valueOf(priceTagStr);
    }
    return 89;
  }

  public void setPriceTagType(Integer priceTagType) {
    this.priceTagType = priceTagType;
  }

  /**
   * 是否称重类的sku
   * 
   * @return
   */
  public Boolean getIsSteelyardSku() {
    if (null != isSteelyardSku) {
      return isSteelyardSku;
    } else {
      if (null == skuUnit) {
        return false;
      } else {
        // 只计算销售单位属于 斤，kg，两，克 的属性为称重类型
        return (skuUnit.intValue() == 32 || skuUnit.intValue() == 3 || skuUnit.intValue() == 41 || skuUnit
            .intValue() == 71);
      }
    }
  }

  public void setIsSteelyardSku(Boolean isSteelyardSku) {
    this.isSteelyardSku = isSteelyardSku;
  }

  /**
   * 获取质量等级信息
   * 
   * @return
   */
  public Integer getLevel() {
    Map<String, String> standM = this.queryStandardCatPropertyValueMap();
    String levelStr = standM.get(String.valueOf(PropertyDTO.LEVEL_PROPERTY_ID));
    if (StringUtils.isNotEmpty(levelStr) && StringUtils.isNumeric(levelStr)) {
      return Integer.valueOf(levelStr);
    }
    return 73;// 特级
  }

  public void setLevel(Integer level) {
    this.level = level;
  }

  /**
   * 存储方式
   * 
   * @return
   */
  public Integer getStorageType() {
    Map<String, String> standM = this.queryStandardCatPropertyValueMap();
    String storageStr = standM.get(String.valueOf(PropertyDTO.STORAGETYPE_PROPERTY_ID));
    if (StringUtils.isNotEmpty(storageStr) && StringUtils.isNumeric(storageStr)) {
      return Integer.valueOf(storageStr);
    }
    return 81;// 默认冷藏
  }

  public void setEatType(Integer eatType) {
    this.eatType = eatType;
  }

  /**
   * 食用方式
   * 
   * @return
   */
  public Integer getEatType() {
    if (null != eatType) {
      return eatType;
    } else {
      Map<String, String> standM = this.queryStandardCatPropertyValueMap();
      String eatStr = standM.get(String.valueOf(PropertyDTO.EATTYPE_PROPERTY_ID));
      if (StringUtils.isNotEmpty(eatStr) && StringUtils.isNumeric(eatStr)) {
        return Integer.valueOf(eatStr);
      }
    }
    return 99;// 默认即食
  }

  public void setStorageType(Integer storageType) {
    this.storageType = storageType;
  }

  public Boolean getAllowReturn() {
    Map<String, String> standM = this.queryStandardCatPropertyValueMap();
    String returnStr = standM.get(String.valueOf(PropertyDTO.RETURN_PROPERTY_ID));
    if (StringUtils.isNotEmpty(returnStr) && "77".equals(returnStr)) {
      return false;
    }
    return true;
  }

  public void setAllowReturn(Boolean allowReturn) {
    this.allowReturn = allowReturn;
  }
  
  /**
   * sku是否包含某个tag
   * @return
   */
  public boolean skuContainTag(int tagetTag){
    if(null != this.getTags() && tagetTag > 0 && (this.getTags() & tagetTag) == tagetTag){
      return true;
    }else{
      return false;
    }
  }
}
