package com.xianzaishi.itemcenter.client.itemsku.query;

import java.io.Serializable;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

public class SkuRelationAvaliableScheduleQuery extends BaseQuery implements Serializable{

  private static final long serialVersionUID = -6632454067918277311L;
  
  private int type = 0;

  private int relationType = -1;
  
  public int getType() {
    return type;
  }

  protected void setType(int type) {
    this.type = type;
  }

  public int getRelationType() {
    return relationType;
  }

  protected void setRelationType(int relationType) {
    this.relationType = relationType;
  }

  private SkuRelationAvaliableScheduleQuery(){
    
  }
  public static SkuRelationAvaliableScheduleQuery getBuyQuery(){
    SkuRelationAvaliableScheduleQuery query = new SkuRelationAvaliableScheduleQuery();
    query.setType(-1);
    query.setRelationType(0);
    return query;
  }
  
  public static SkuRelationAvaliableScheduleQuery getBuyDiscountQuery(){
    SkuRelationAvaliableScheduleQuery query = new SkuRelationAvaliableScheduleQuery();
    query.setType(-1);
    query.setRelationType(1);
    return query;
  }
  
  public static SkuRelationAvaliableScheduleQuery getSaleQuery(){
    SkuRelationAvaliableScheduleQuery query = new SkuRelationAvaliableScheduleQuery();
    query.setType(1);
    query.setRelationType(0);
    return query;
  }
  
  public static SkuRelationAvaliableScheduleQuery getSaleDiscountQuery(){
    SkuRelationAvaliableScheduleQuery query = new SkuRelationAvaliableScheduleQuery();
    query.setType(1);
    query.setRelationType(1);
    return query;
  }
}
