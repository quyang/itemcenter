package com.xianzaishi.itemcenter.client.sysproperty.dto;

import java.io.Serializable;

public class SystemConfigDTO implements Serializable {

  private static final long serialVersionUID = -1089489021133108354L;

  private Integer configId;
  
  private String configInfo;

  public Integer getConfigId() {
    return configId;
  }

  public void setConfigId(Integer configId) {
    this.configId = configId;
  }

  public String getConfigInfo() {
    return configInfo;
  }

  public void setConfigInfo(String configInfo) {
    this.configInfo = configInfo;
  }
}
