package com.xianzaishi.itemcenter.client.stdcategory.query;

import java.io.Serializable;

/**
 * 后台类目查询条件，包括类目id、是否包括属性以及是否包括属性值
 * 
 * @author dongpo
 * 
 */
public class StdCategoryQuery implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -3272828633988349497L;

  /**
   * 类目id
   */
  private Long catId;

  /**
   * 是否包含类目属性
   */
  private Boolean hasProperty = false;

  /**
   * 是否包含类目属性值
   */
  private Boolean hasPropertyValue = false;


  public Long getCatId() {
    return catId;
  }

  public void setCatId(Long catId) {
    if (catId != null && catId <= 0) {
      throw new IllegalArgumentException("catId must be greater than 0");
    }
    this.catId = catId;
  }

  public Boolean getHasProperty() {
    return hasProperty;
  }

  public void setHasProperty(Boolean hasProperty) {
    this.hasProperty = hasProperty;
  }

  public Boolean getHasPropertyValue() {
    return hasPropertyValue;
  }

  public void setHasPropertyValue(Boolean hasPropertyValue) {
    this.hasPropertyValue = hasPropertyValue;
  }



}
