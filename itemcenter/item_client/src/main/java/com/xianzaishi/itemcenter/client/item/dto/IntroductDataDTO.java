package com.xianzaishi.itemcenter.client.item.dto;

import java.io.Serializable;

/**
 * 商品介绍一个基本对象
 * @author zhancang
 */
public class IntroductDataDTO implements Serializable{

  private static final long serialVersionUID = 4221564651999440646L;

  public static final int PIC_FLAG = 2;
  
  private Integer id;

  private String source;

  private Integer type;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }
}
