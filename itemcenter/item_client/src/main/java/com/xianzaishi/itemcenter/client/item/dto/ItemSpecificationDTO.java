package com.xianzaishi.itemcenter.client.item.dto;

import java.io.Serializable;
import java.util.List;

public class ItemSpecificationDTO implements Serializable {
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -1234009197441679476L;

  /**
   * 规格名
   */
  private String name;
  
  /**
   * 规格列表
   */
  private List<String> values;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<String> getValues() {
    return values;
  }

  public void setValues(List<String> values) {
    this.values = values;
  }
  
  
}
