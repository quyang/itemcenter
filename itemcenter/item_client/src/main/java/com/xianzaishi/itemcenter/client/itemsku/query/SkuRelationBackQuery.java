package com.xianzaishi.itemcenter.client.itemsku.query;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 进售价后台查询接口
 * @author zhancang
 */
public class SkuRelationBackQuery extends BaseQuery implements Serializable{

  private static final long serialVersionUID = 4681205089211538303L;

  private Date saleStartRangeBegin;
  
  private Date saleStartRangeEnd;
  
  private Date saleEndRangeBegin;

  private Date saleEndRangeEnd;

  public Date getSaleStartRangeBegin() {
    return saleStartRangeBegin;
  }

  public void setSaleStartRangeBegin(Date saleStartRangeBegin) {
    this.saleStartRangeBegin = saleStartRangeBegin;
  }

  public Date getSaleStartRangeEnd() {
    return saleStartRangeEnd;
  }

  public void setSaleStartRangeEnd(Date saleStartRangeEnd) {
    this.saleStartRangeEnd = saleStartRangeEnd;
  }

  public Date getSaleEndRangeBegin() {
    return saleEndRangeBegin;
  }

  public void setSaleEndRangeBegin(Date saleEndRangeBegin) {
    this.saleEndRangeBegin = saleEndRangeBegin;
  }

  public Date getSaleEndRangeEnd() {
    return saleEndRangeEnd;
  }

  public void setSaleEndRangeEnd(Date saleEndRangeEnd) {
    this.saleEndRangeEnd = saleEndRangeEnd;
  }
}
