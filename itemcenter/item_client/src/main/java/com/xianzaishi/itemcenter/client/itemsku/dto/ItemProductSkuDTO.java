package com.xianzaishi.itemcenter.client.itemsku.dto;


/**
 * 采购端sku对象
 * 
 * @author zhancang
 */
public class ItemProductSkuDTO extends ItemSkuDTO {

  private static final long serialVersionUID = 4822670712111612800L;

  /**
   * 采购人员id
   */
  private Integer supplierAgent;

  public Integer getSupplierAgent() {
    return supplierAgent;
  }

  public void setSupplierAgent(Integer supplierAgent) {
    this.supplierAgent = supplierAgent;
  }
  
}
