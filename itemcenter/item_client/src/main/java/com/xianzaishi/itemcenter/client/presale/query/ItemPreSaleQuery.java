package com.xianzaishi.itemcenter.client.presale.query;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

import java.io.Serializable;

/**
 * 预售商品的查询字段，可以供hession使用也可以供App使用
 *
 * @author jianmo
 * @create 2016-12-23 上午 10:58
 **/
public class ItemPreSaleQuery extends BaseQuery implements Serializable{
    /**
     * serializable UID版本号
     */
    private static final long serialVersionUID = 1188023645503112389L;

    /**
     * 预售商品的预售ID
     * 表字段 : item_pre_sale.id
     */
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
