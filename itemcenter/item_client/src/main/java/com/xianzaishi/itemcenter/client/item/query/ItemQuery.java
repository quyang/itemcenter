package com.xianzaishi.itemcenter.client.item.query;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * 商品单个商品详情
 * 
 * @author dongpo
 */

public class ItemQuery implements Serializable {

  /**
   * serializable UID版本号
   */
  private static final long serialVersionUID = 1388043665503002389L;

  /**
   * 商品id组
   */
  private Long itemId;

  /**
   * 商品对应skuId
   */
  private Long skuId;

  /**
   * 是否包含sku属性
   */
  private Boolean hasItemSku = false;

  /**
   * 是否返回商品全部属性。<br/>
   * returnAll=false 返回部分属实时返回的对象是ItemBaseDTO对象实例。<br/>
   * returnAll=true 返回全部对象时，返回的对象是ItemDTO对象实例
   */
  private Boolean returnAll = false;
  
  /**
   * 将前台需要属性翻译成可见属性
   */
  private Boolean translateFrontPropertyToHumanLanguage = false;
  
  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    Preconditions.checkNotNull(itemId, "itemId == null");
    Preconditions.checkArgument(itemId > 0, "itemId <= 0");
    this.itemId = itemId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    Preconditions.checkNotNull(skuId, "skuId == null");
    Preconditions.checkArgument(skuId > 0, "skuId <= 0");
    this.skuId = skuId;
//    this.setHasItemSku(true);
  }

  public Boolean getHasItemSku() {
    return hasItemSku;
  }

  public void setHasItemSku(Boolean hasItemSku) {
    this.hasItemSku = hasItemSku;
  }

  public Boolean getReturnAll() {
    return returnAll;
  }

  public void setReturnAll(Boolean returnAll) {
    this.returnAll = returnAll;
  }

  public Boolean getTranslateFrontPropertyToHumanLanguage() {
    return translateFrontPropertyToHumanLanguage;
  }

  public void setTranslateFrontPropertyToHumanLanguage(Boolean translateFrontPropertyToHumanLanguage) {
    this.translateFrontPropertyToHumanLanguage = translateFrontPropertyToHumanLanguage;
  }
}
