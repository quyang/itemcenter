package com.xianzaishi.itemcenter.client.itemsku.constants;

/**
 * sku各种静态变量
 * @author zhancang
 */
public class SkuConstants {
  
  /**
   * SKU打标标志
   */
  public static final Integer SKU_TAG_FLAG_ADD = 1;
  
  /**
   * SKU去标标志
   */
  public static final Integer SKU_TAG_FLAG_REMOVE = -1;
  
  /**
   * SKU满90-40标,线上使用的标，暂时标做的细一点
   */
  public static final Integer SKU_TAG_90_DISCOUNT_40 = 1;
  
  /**
   * SKU满40-20标,线下使用的标，暂时标做的细一点
   */
  public static final Integer SKU_TAG_40_DISCOUNT_20_OFFLINE =  (1<<1);
  
  /**
   * SKU少量标签
   */
  public static final Integer SKU_TAG_SHAOLIANG = (1<<2);
  
  /**
   * SKU限时特惠
   */
  public static final Integer SKU_TAG_XIANSHITEHUI = (1<<3);
  
  /**
   * SKU满90-40标,线下使用的标，暂时标做的细一点
   */
  public static final Integer SKU_TAG_90_DISCOUNT_40_OFFLINE = (1<<4);
  
  /**
   * SKU满40-20标,线上使用的标，暂时标做的细一点
   */
  public static final Integer SKU_TAG_40_DISCOUNT_20 = (1<<5);
  
  /**
   * SKU不参加优惠(64)
   */
  public static final Integer SKU_IGNORE_PROMOTION = (1<<6);
  
  /**
   * SKU不参加优惠(128)
   */
  public static final Integer SKU_CANNOT_BUY = (1<<7);
}
