package com.xianzaishi.itemcenter.client.value;

import java.util.List;
import java.util.Map;

import com.xianzaishi.itemcenter.client.value.dto.ValueDTO;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 属性值接口，
 * 
 * @author dongpo
 * 
 */
public interface ValueService {


  /**
   * 查询属性值列表
   * 
   * @param query
   * @return
   */
  Result<List<ValueDTO>> queryValue(Integer catId, Integer propertyId, Integer valueId);

  /**
   * 查询属性值对象
   * 
   * @param query
   * @return
   */
  Result<Integer> queryValueIdByValueName(String valueName);

  /**
   * 查询标准属性值名称，待缓存查询
   * 
   * @param query
   * @return
   */
  Result<String> queryValueNameByValueId(Integer valueId);
  
  /**
   * 查询标准属性值名称，待缓存查询
   * 
   * @param query
   * @return
   */
  Result<Map<Integer,String>> queryValueNameListByValueId(List<Integer> valueId);
  /**
   * 插入属性值对象
   * 
   * @param valueDto
   * @return
   */
  Result<Integer> insertPropertyValue(ValueDTO ValueDto);

  /**
   * 更新属性值列表
   * 
   * @param valueDto
   * @return
   */
  Result<Boolean> updatePropertyValue(ValueDTO ValueDto);
}
