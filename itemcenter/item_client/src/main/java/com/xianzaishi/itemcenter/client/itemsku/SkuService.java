package com.xianzaishi.itemcenter.client.itemsku;

import java.util.List;

import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationScheduleDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuWeightInfoDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationAvaliableScheduleQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationScheduleQuery;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * sku对外服务，包括后台sku对外服务和前台sku对外服务，及sku关联关系
 * 
 * @author zhancang
 */
public interface SkuService {

  /**
   * 发布后台商品sku
   * 
   * @param itemProduct
   * @return
   */
  public Result<Long> insertProductSku(ItemProductSkuDTO productSku,Integer catId);

  /**
   * 更新后台商品sku
   * 
   * @param itemProduct
   * @return
   */
  public Result<Boolean> updateProductSku(ItemProductSkuDTO productSku);

  /**
   * 后台方法，快速修改skuId
   * @param cskuId
   * @param sku69Code
   * @return
   */
  public Result<Boolean> updateSku69Code(long cskuId,long sku69Code);
  
  /**
   * 查询后台商品sku列表
   * 
   * @param query
   * @return
   */
  public Result<List<ItemProductSkuDTO>> queryProductSku(SkuQuery query);
  
  /**
   * 查询后台商品sku列表个数
   * 
   * @param query
   * @return
   */
  public Result<Integer> queryProductSkuCount(SkuQuery query);

  /**
   * 发布前台商品sku
   * 
   * @param itemProduct
   * @return
   */
  public Result<Long> insertItemSku(ItemCommoditySkuDTO itemSku);

  /**
   * 更新前台商品sku
   * 
   * @param itemProduct
   * @return
   */
  public Result<Boolean> updateItemSku(ItemCommoditySkuDTO itemSku);

  /**
   * sku打标去标逻辑
   * @param skuId skuId
   * @param tag 修改目标tag
   * @param addRemoveFlag 增加删除标志
   */
  public Result<Boolean> updateItemSkuTags(Long skuId, Integer tag, Integer addRemoveFlag);
  /**
   * 查询前台商品sku列表
   * 
   * @param productId
   * @return
   */
  public Result<List<ItemCommoditySkuDTO>> queryItemSkuList(SkuQuery query);
  
  /**
   * 查询前台商品sku列表个数
   * 
   * @param query
   * @return
   */
  public Result<Integer> queryItemSkuCount(SkuQuery query);
  
  /**
   * 查询前台商品sku列表，只查询一个
   * 
   * @param productId
   * @return
   */
  public Result<ItemCommoditySkuDTO> queryItemSku(SkuQuery query);
  
//  /**
//   * 查询基础sku信息，只查询单个sku
//   * @param query
//   * @return
//   */
//  public Result<ItemSkuDTO> querySkuInfo(Long skuCode);
  
  /**
   * 发布前后台商品sku关联关系
   * 
   * @param itemProduct
   * @return
   */
  public Result<Boolean> insertItemSkuRelation(ItemSkuRelationDTO skuRelation);

  /**
   * 发布前后台商品sku关联关系
   * 
   * @param itemProduct
   * @return
   */
  public Result<Boolean> insertItemSkuScheduleRelation(ItemSkuRelationScheduleDTO skuRelation);
  
  /**
   * 发布前后台商品sku关联关系
   * 
   * @param itemProduct
   * @return
   */
  public Result<Boolean> batchInsertItemSkuScheduleRelation(List<ItemSkuRelationScheduleDTO> skuRelation);
  
  /**
   * 删除关系
   * @param id
   * @return
   */
  public Result<Boolean> deleteItemSkuScheduleRelation(long id);
  
  /**
   * 更新前后台商品sku关联关系
   * 
   * @param itemProduct
   * @return
   */
  public Result<Boolean> updateItemSkuRelation(ItemSkuRelationDTO skuRelation);
  
  /**
   * 更新前后台商品sku关联关系
   * 
   * @param itemProduct
   * @return
   */
  public Result<Boolean> updateItemSkuScheduleRelation(ItemSkuRelationScheduleDTO skuRelation);
  
  /**
   * 更新前后台商品sku关联关系价格相关，包括采购某个时间段价格，销售某个时间段价格
   * 
   * @param itemProduct
   * @return
   */
  public Result<Boolean> updateItemSkuRelationPrice(ItemSkuRelationDTO skuRelation);
  
  /**
   * 根据前台skuId查询符合条件关系列表
   * @param cskuId
   * @return
   */
  public Result<List<ItemSkuRelationDTO>> querySkuRelationList(SkuRelationQuery skuRelationQuery);
 
  /**
   * 查询定时关系列表
   * 
   * @param itemProduct
   * @return
   */
  public Result<List<ItemSkuRelationScheduleDTO>> querySkuRelationScheduleDetailList(SkuRelationScheduleQuery skuRelationQuery);
  
  /**
   * 查询有效的sku数据
   * 
   * @param itemProduct
   * @return
   */
  public Result<List<ItemSkuRelationScheduleDTO>> queryAvailableSkuRelationScheduleList(SkuRelationAvaliableScheduleQuery query);
  
  /**
   * 根据前台skuId查询符合条件关系列表个数
   * @param cskuId
   * @return
   */
  public Result<Integer> querySkuRelationCount(SkuRelationQuery skuRelationQuery);
  
  /**
   * 根据前后台skuId查询符合条件关系列表对象
   * @param cskuId
   * @return
   */
  public Result<ItemSkuRelationDTO> querySkuRelation(Long id);
  
  /**
   * 获取sku商品标详情
   * @param skuIds
   * @return
   */
  public Result<List<ItemSkuTagsDTO>> querySkuTagsDetail(List<Long> skuIds);
  
  /**
   * 修改商品以及sku销售渠道
   * @param skuId
   * @param channel
   * @return 返回是否修改成功
   */
  Result<Boolean> updateItemSkuChannel(Long skuId, Short channel);
  
  /**
   * 获取称重商品信息
   * @param skuId
   * @param weight 重量
   * @return 返回该称重商品的总价、总优惠价以及份数
   */
  Result<ItemSkuWeightInfoDTO> queryWeighItemInfo(Long skuId, Integer weight);
}
