package com.xianzaishi.itemcenter.component.item;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.naming.directory.SearchControls;

import javax.transaction.HeuristicRollbackException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.CommodityService;
import com.xianzaishi.itemcenter.client.item.dto.IntroductDataDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.client.item.query.ItemQuery;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO.SkuSaleUnitConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO.SkuTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CmCategoryDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.pic.PicUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.itemcenter.component.stdcategory.CmCategoryServiceImpl;
import com.xianzaishi.itemcenter.dal.item.dataobject.ItemBaseDO;
import com.xianzaishi.itemcenter.dal.item.dataobject.ItemDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemCommoditySkuDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemProductSkuDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuRelationDO;
import com.xianzaishi.itemcenter.dal.log.dataobject.ItemProcessLogDO;
import com.xianzaishi.itemcenter.dal.product.dataobject.ItemProductDO;
import com.xianzaishi.itemcenter.dal.stdcategory.dataobject.CategoryDO;

/**
 * 请求商品接口的实现类
 * 
 * @author zhancang
 */
@Service("commodityService")
public class CommodityServiceImpl extends ItemService implements CommodityService {

  private static final Logger logger = Logger.getLogger(CommodityServiceImpl.class);
  
  @Override
  public Result<Long> insertCommodity(ItemDTO itemDto) {
    if (null == itemDto) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input itemdto parameter is null");
    }

    if (null == itemDto.getCategoryId() || itemDto.getCategoryId() <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert itemdto catid error");
    }

    if (CollectionUtils.isEmpty(itemDto.getItemSkuDtos())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert itemdto skuList is null");
    }

    Long commodityId = super.getCommodityId();
    
    Long commoditySkuId = super.getSkuPluId(itemDto.getCategoryId());
    
    ItemDO itemDO = getCommdityDOByCommdityDTO(itemDto, commodityId, commoditySkuId, true);

    ItemCommoditySkuDO sku = new ItemCommoditySkuDO();
    ItemCommoditySkuDTO skuDTO = (ItemCommoditySkuDTO) (itemDto.getItemSkuDtos().get(0));
    skuDTO.setSkuId(commoditySkuId);
    skuDTO.setSkuPluCode(commoditySkuId);
    skuDTO.setItemId(commodityId);

    Short skuType = skuDTO.getSkuType();
    // 加工类的不需要设置，发布时就设置好了
    if (!SkuTypeConstants.ORIGINAL_GOODS_SKU.equals(skuType.shortValue())) {
      // skuDTO.setSkuCode(getItemCodeId(itemDto.getCategoryId()));
      setSpecificationInfoWithDeal(skuDTO);
    } else {
      setSpecificationInfo(skuDTO);
    }

    setPriceInfo(skuDTO, itemDO, true);

    BeanCopierUtils.copyProperties(skuDTO, sku);//

    // 非加工类的设置sku进售价关联关系
    if (skuType == SkuTypeConstants.ORIGINAL_GOODS_SKU) {
      Long pskuId = skuDTO.getSkuCode();
      if(null == pskuId || pskuId <= 0){
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
            "query productsku failed");
      }
      List<ItemProductSkuDO> productSkuList = itemProductSkuDOMapper.query(SkuQuery.querySkuById(pskuId));
      if(CollectionUtils.isEmpty(productSkuList)){
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
            "query productsku list failed");
      }
      
      ItemProductSkuDO psku = productSkuList.get(0);

      ItemSkuRelationDO relationDO =
          super.getSkuRelationByPSSku(pskuId, sku, RelationTypeConstants.RELATION_TYPE_NORMAL, 
              null,psku.getPrice(), sku.getUnitPrice());
      int insertSkuRelationCount = itemSkuRelationDOMapper.insert(relationDO);
      if (insertSkuRelationCount != 1) {
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
            "insert SkuRelation db error");
      }

      ItemSkuRelationDO disRelationDO =
          super.getSkuRelationByPSSku(pskuId, sku, RelationTypeConstants.RELATION_TYPE_DISCOUNT,
              null ,psku.getPrice(), sku.getUnitPrice());
      insertSkuRelationCount = itemSkuRelationDOMapper.insert(disRelationDO);
      if (insertSkuRelationCount != 1) {
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
            "insert DisSkuRelation db error");
      }
    } else {
      ItemSkuRelationDO relationDO =
          super.getSkuRelationByBoomSku(sku, RelationTypeConstants.RELATION_TYPE_NORMAL, null);
      int insertSkuRelationCount = itemSkuRelationDOMapper.insert(relationDO);
      if (insertSkuRelationCount != 1) {
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
            "insert SkuRelation db error");
      }

      ItemSkuRelationDO disRelationDO =
          super.getSkuRelationByBoomSku(sku, RelationTypeConstants.RELATION_TYPE_DISCOUNT, null);
      insertSkuRelationCount = itemSkuRelationDOMapper.insert(disRelationDO);
      if (insertSkuRelationCount != 1) {
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
            "insert DisSkuRelation db error");
      }
      //加工类型商品没有进价独立维护，所以在发布这里维护进入系统日志中
      if (1 != itemProcessLogDOMapper.insert(getItemProcessLogDO(sku.getSkuId(),
          itemDto.getProcessor(), String.valueOf(-1), String.valueOf(relationDO.getbPrice()),
          ItemProcessLogDO.ITEM_UPDATE_TYPE_PRODUCT_PRICE))) {
        logger.error("[Item_update_price],update price " + sku.getSkuId() + " from " + -1 + " to "
            + sku.getPrice());
        //TODO zhancangprice 采购价格
      }
    }

    int insertCommoditySkuCount = itemCommoditySkuDOMapper.insert(sku);
    if (insertCommoditySkuCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "insert CommoditySku db error");
    }else{
      logSkuUpPrice(sku,null,null,itemDto.getProcessor(),false);
    }

    Integer insertNumber = itemDOMapper.insertItem(itemDO);// 插入商品数据
    Long itemId = insertNumber == 1 ? itemDO.getItemId() : 0;
    if (itemId != 0) {
      return Result.getSuccDataResult(itemId);
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR_COMMODITY_ITEM_INSERT,
          "insert item failed");
    }
  }

  @Override
  public Result<Boolean> updateCommodityPic(ItemDTO itemDto) {
    if (null == itemDto) {
      return Result.getErrDataResult(-1, "Update item pic failed");
    }

    ItemDO itemDO = itemDOMapper.queryItem(itemDto.getItemId());
    Long productId = itemDO.getProductId();
    ItemProductDO productDO = itemProductDOMapper.query(productId);

    productDO.setPic(PicUtil.joinPicStr(PicUtil.processPicListHeader(itemDto.getPicList(), false)));
    productDO.setIntroduction(processIntroductPicHeader(itemDto.getIntroduction(), false));

    itemDO.setPic(PicUtil.joinPicStr(PicUtil.processPicListHeader(itemDto.getPicList(), false)));
    itemDO.setIntroduction(processIntroductPicHeader(itemDto.getIntroduction(), false));

    int count = itemDOMapper.updateItem(itemDO);
    if (count == 1) {
      int productCount = itemProductDOMapper.update(productDO);
      if (productCount == 1) {
        return Result.getSuccDataResult(true);
      }
      return Result.getErrDataResult(-2, "Update productDO failed");
    }
    return Result.getErrDataResult(-3, "Update itemDO failed");
  }

  @Override
  public Result<Boolean> updateCommodity(ItemDTO itemDto) {

    ItemDO itemDO =
        getCommdityDOByCommdityDTO(itemDto, itemDto.getItemId(), Long.valueOf(itemDto.getSku()),
            false);

    itemDto.setGmtModified(new Date());

    ItemCommoditySkuDO sku = new ItemCommoditySkuDO();
    ItemCommoditySkuDTO skuDTO = (ItemCommoditySkuDTO) (itemDto.getItemSkuDtos().get(0));

    Short skuType = skuDTO.getSkuType();
    // 加工类的不需要设置，发布时就设置好了
    if (SkuTypeConstants.ORIGINAL_GOODS_SKU.equals(skuType.shortValue())) {
      setSpecificationInfo(skuDTO);
    } else {
      setSpecificationInfoWithDeal(skuDTO);
    }

    setPriceInfo(skuDTO, itemDO, false);
    BeanCopierUtils.copyProperties(skuDTO, sku);//

    itemDO.setSaleStatus(sku.getSaleStatus());

    // 非加工类型的，可能因为销售规格变化，导致进售价变化
    List<ItemSkuRelationDO> relationList =
        itemSkuRelationDOMapper.queryList(SkuRelationQuery.getQueryByCskuIds(
            Arrays.asList(skuDTO.getSkuId()), null));
    
    int buyPrice = 0;
    int buyDiscountPrice = 0;
    
    //TODO bug from
//    List<ItemProductSkuDO> proQueryResult = itemProductSkuDOMapper.query(SkuQuery.querySkuById(sku.getSkuCode()));
//    if(CollectionUtils.isNotEmpty(proQueryResult)){
//      buyPrice = proQueryResult.get(0).getPrice();
//      buyDiscountPrice = proQueryResult.get(0).getDiscountPrice();
//    }
    
    for (ItemSkuRelationDO skudo : relationList) {
      if (skuType == SkuTypeConstants.ORIGINAL_GOODS_SKU) {
        if(skudo.getRelationType().equals(RelationTypeConstants.RELATION_TYPE_NORMAL)){
          if(buyPrice <= 0){
            buyPrice = skudo.getbPrice();
          }
          skudo = getSkuRelationByPSSku(skudo.getPskuId(), sku, skudo.getRelationType(), skudo,
              buyPrice,sku.getUnitPrice());
        }else{
          if(buyDiscountPrice <= 0){
            buyDiscountPrice = skudo.getbPrice();
          }
          skudo = getSkuRelationByPSSku(skudo.getPskuId(), sku, skudo.getRelationType(), skudo,
              buyDiscountPrice,sku.getDiscountUnitPrice());
        }
        int count = itemSkuRelationDOMapper.update(skudo);
        if (count != 1) {
          return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
              "update skurelation failed,id is:" + skudo.getId());
        }
      } else {
//        skudo = getSkuRelationByBoomSku(sku, skudo.getRelationType(), skudo);
      }
    }
    
    List<ItemCommoditySkuDO> queryResult = itemCommoditySkuDOMapper.query(SkuQuery.querySkuById(sku.getSkuId()),null);
    if(CollectionUtils.isEmpty(queryResult)){
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update sku,query is null,id is:" + sku.getSkuId());
    }
    ItemCommoditySkuDO dbSku = queryResult.get(0);
    int update = itemCommoditySkuDOMapper.update(sku);
    if (update != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update commoditySku failed");
    }else{
      logSkuUpPrice(sku,dbSku,null,itemDto.getProcessor(),false);
    }

    itemDO.setSku("-1");// 标识sku依赖不修改
    update = itemDOMapper.updateItem(itemDO);
    if (update == 1) {
      return Result.getSuccDataResult(true);
    }
    return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR_COMMODITY_ITEM_UPDATE,
        "insert item failed");
  }

  @Override
  public Result<Boolean> updateCommodityPrice(ItemDTO itemDto, Boolean isDiscount) {
//    ItemDO itemDO =
//        getCommdityDOByCommdityDTO(itemDto, itemDto.getItemId(), Long.valueOf(itemDto.getSku()),
//            false);
//
//    itemDto.setGmtModified(new Date());
//
//    ItemCommoditySkuDO sku = new ItemCommoditySkuDO();
//    ItemCommoditySkuDTO skuDTO = (ItemCommoditySkuDTO) (itemDto.getItemSkuDtos().get(0));
//
//    Short skuType = skuDTO.getSkuType();
//    // 加工类的不需要设置，发布时就设置好了
//    if (SkuTypeConstants.ORIGINAL_GOODS_SKU.equals(skuType.shortValue())) {
//      setSpecificationInfo(skuDTO);
//    } else {
//      setSpecificationInfoWithDeal(skuDTO);
//    }
//
//    setPriceInfo(skuDTO, itemDO, false);
//    BeanCopierUtils.copyProperties(skuDTO, sku);//
//
//    itemDO.setSaleStatus(sku.getSaleStatus());
//
//    List<ItemSkuRelationDO> relationList = null;
//    if (isDiscount) {
//      // 非加工类型的，可能因为销售规格变化，导致进售价变化
//      relationList =
//          itemSkuRelationDOMapper.queryList(SkuRelationQuery.getQueryByCskuIds(
//              Arrays.asList(skuDTO.getSkuId()), RelationTypeConstants.RELATION_TYPE_DISCOUNT));
//    } else {
//      relationList =
//          itemSkuRelationDOMapper.queryList(SkuRelationQuery.getQueryByCskuIds(
//              Arrays.asList(skuDTO.getSkuId()), RelationTypeConstants.RELATION_TYPE_NORMAL));
//    }
//    for (ItemSkuRelationDO skudo : relationList) {
//      if (skuType == SkuTypeConstants.ORIGINAL_GOODS_SKU) {
//        skudo = getSkuRelationByPSSku(skudo.getPskuId(), sku, skudo.getRelationType(), skudo,0,0);
//      } else {
//        skudo = getSkuRelationByBoomSku(sku, skudo.getRelationType(), skudo);
//      }
//      int count = itemSkuRelationDOMapper.update(skudo);
//      if (count != 1) {
//        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
//            "update skurelation failed,id is:" + skudo.getId());
//      }
//    }
//
//    int update = itemCommoditySkuDOMapper.update(sku);
//    if (update != 1) {
//      return Result
//          .getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update commoditySku failed");
//    }else{
//      logSkuUpPrice(sku,null,isDiscount,itemDto.getProcessor(),false);
//    }
//
//    itemDO.setSku("-1");// 标识sku依赖不修改
//    update = itemDOMapper.updateItem(itemDO);
//    if (update == 1) {
//      return Result.getSuccDataResult(true);
//    }
    return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR_COMMODITY_ITEM_UPDATE,
        "insert item failed");
  }

  @Override
  public Result<Map<String, Object>> queryCommodies(ItemListQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }
    Map<String, Object> result = Maps.newHashMap();

    //根据前台类目查询数据
    if (null != query.getFrontCatId() && query.getFrontCatId() > 0) {
      List<Integer> subCatIds = Lists.newArrayList();
      List<CmCategoryDTO> localcache = Lists.newArrayList();
      Map<Integer, List<CmCategoryDTO>> ListMap = CmCategoryServiceImpl.CM_STD_MAP;
      if (CollectionUtils.isEmpty(ListMap.get(query.getFrontCatId()))) {
        List<CategoryDO> catList = getSubCatByParent(query.getFrontCatId());
        for (CategoryDO cat : catList) {
          if (null != cat) {
            CmCategoryDTO tmp = new CmCategoryDTO();
            tmp.setCatId(cat.getCatId());
            tmp.setName(cat.getCatName());
            localcache.add(tmp);
          }
        }
        subCatIds.add(localcache.get(0).getCatId());
        ListMap.put(query.getFrontCatId(), localcache);
      } else {
        localcache = ListMap.get(query.getFrontCatId());
        for (CmCategoryDTO cat : localcache) {
          subCatIds.add(cat.getCatId());
        }
      }
      if (CollectionUtils.isNotEmpty(subCatIds)) {
        query.setCmCat2Ids(subCatIds);
        result.put(ITEM_CAT_KEY, localcache);
      }
    }
    
    //针对按照商品名称搜索进行干预
    if(StringUtils.isNotBlank(query.getItemTitle()) && null != query.getSearchIntervention() && query.getSearchIntervention()){
      queryCommodiesWithIntervention(query);
    }
    logger.error("search query:" + JackSonUtil.getJson(query));
    //查询结果是否按照查询顺序输出
    List<ItemBaseDO> doResult = Lists.newArrayList();
    if(null != query.getInOrder() && query.getInOrder()){
      //结果按照id的顺序输出
      logger.error("query:"+JackSonUtil.getJson(query));
      List<Integer> cmCat2Ids = query.getCmCat2Ids();
      StringBuilder sBuilder = new StringBuilder();
      for(Integer cmCat2Id : cmCat2Ids){
        if(null == cmCat2Id){
          continue;
        }
        sBuilder.append(cmCat2Id);
        if(cmCat2Ids.indexOf(cmCat2Id) != (cmCat2Ids.size() - 1)){
          sBuilder.append(",");
        }
      }
      doResult = itemDOMapper.queryItemListInOrder(query,sBuilder.toString());
    }else{
      logger.error("调用queryItemList");
      doResult = itemDOMapper.queryItemList(query);
    }
    if (CollectionUtils.isEmpty(doResult)) {
      Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "Query item result is null");
    }
    
    logger.error(JackSonUtil.getJson(doResult));
    // 按照多id查询时重新排序
    if (null != query && CollectionUtils.isNotEmpty(query.getItemIds())
        && query.getItemIds().size() > 1) {
      Map<Long, ItemBaseDO> itemM = Maps.newHashMap();
      for (ItemBaseDO item : doResult) {
        itemM.put(item.getItemId(), item);
      }
      List<ItemBaseDO> sortResult = Lists.newArrayList();
      for (Long id : query.getItemIds()) {
        if (null != itemM.get(id)) {
          sortResult.add(itemM.get(id));
        }
      }
      doResult = sortResult;
    }

    List<ItemDTO> dtoResult = Lists.newArrayList();
    BeanCopierUtils.copyListBean(doResult, dtoResult, ItemDTO.class);

    List<Long> skuIds = Lists.newArrayList();
    Map<Long, ItemCommoditySkuDTO> tmpM = Maps.newHashMap();
    logger.error("doResult:"+JackSonUtil.getJson(doResult));
    for (int i = 0; i < doResult.size(); i++) {
      ItemBaseDO item = doResult.get(i);
      String picStr = item.getPic();
      List<String> picList = PicUtil.splitPicStr(picStr);
      if (CollectionUtils.isNotEmpty(picList)) {
        // 取一张图片返回前台
        dtoResult.get(i).setPicUrl(PicUtil.processPicListHeader(picList, true).get(0));
      }
      ItemDTO itemDTO = dtoResult.get(i);
      itemDTO.setPicList(picList);
      String skuStr = item.getSku();
      if (StringUtils.isNotEmpty(skuStr)) {
        String idArray[] = skuStr.split(";");
        if (null != idArray && idArray.length >= 1) {
          skuIds.add(Long.valueOf(idArray[0]));// 取出sku批量查询sku属性
        }
      }
    }

    if (CollectionUtils.isNotEmpty(skuIds)) {
      List<ItemCommoditySkuDO> skuList =
          itemCommoditySkuDOMapper.query(SkuQuery.querySkuBySkuIds(skuIds),null);
      List<ItemCommoditySkuDTO> skuDTOResult = Lists.newArrayList();
      BeanCopierUtils.copyListBean(skuList, skuDTOResult, ItemCommoditySkuDTO.class);
      for (ItemCommoditySkuDTO sku : skuDTOResult) {
        tmpM.put(sku.getItemId(), sku);
      }
      for (ItemBaseDTO item : dtoResult) {
        ItemCommoditySkuDTO sku = tmpM.get(item.getItemId());
        if (null != sku) {
          item.setItemSkuDtos(Arrays.asList(sku));
        }
      }
    }

    Long count = itemDOMapper.count(query);

    logger.error("Query is:" + JackSonUtil.getJson(query) + ",Result count:" + count);
    result.put(ITEM_LIST_KEY, dtoResult);
    result.put(ITEM_COUNT_KEY, count);

    return Result.getSuccDataResult(result);
  }

  /**
   * 搜索干预=一个关键字+多个叶子类目
   * @param query
   */
  private void queryCommodiesWithIntervention(ItemListQuery query) {
    if(null == query || StringUtils.isBlank(query.getItemTitle()) || !query.getSearchIntervention()){
      return;
    }
    String itemTitle = query.getItemTitle();
    ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
    String interventionQuery = context.getMessage(itemTitle, null, null, null);
    if(StringUtils.isBlank(interventionQuery)){
      context.close();
      return;
    }
    String[] queryArray = interventionQuery.split(SPLIT_PROPERTY_KEY);
    if(null == queryArray || queryArray.length == 0){
      context.close();
      return;
    }
    String itemKeyWord = queryArray[0];
    String categoryIdArray = null;
    if(itemKeyWord.length() > 1){
      categoryIdArray = queryArray[1];
    }
    List<Integer> categoryIds = Lists.newArrayList();
    for(String categoryId : categoryIdArray.split(SPLIT_COMMA_KEY)){
      categoryIds.add(Integer.valueOf(categoryId));
    }
    if(StringUtils.isNotBlank(itemKeyWord)){
      query.setItemTitle(itemKeyWord.trim());
    }
    if(CollectionUtils.isNotEmpty(categoryIds)){
      query.setCategoryIds(categoryIds);
    }
    context.close();
  }

  @Override
  public Result<ItemBaseDTO> queryCommodity(ItemQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }

    logger.error("Query parameter debug. ItemId :" + query.getItemId() + ",skuId:"
        + query.getSkuId() + ",isIncludeAll:" + query.getReturnAll() + ",returnSku:"
        + query.getHasItemSku());

    long itemId = 0;
    ItemCommoditySkuDO skuDO = null;
    if (null != query.getItemId()) {
      itemId = query.getItemId();
    }

    if (null != query.getSkuId()) {
      List<ItemCommoditySkuDO> skuList =
          itemCommoditySkuDOMapper.query(SkuQuery.querySkuById(query.getSkuId()),null);
      skuDO = skuList.get(0);
      itemId = skuDO.getItemId();
    }
    if (query.getHasItemSku() && null == skuDO) {
      List<ItemCommoditySkuDO> skuList =
          itemCommoditySkuDOMapper.query(SkuQuery.querySkuByItem(itemId),null);
      if (CollectionUtils.isNotEmpty(skuList)) {
        skuDO = skuList.get(0);
      }
    }

    if (itemId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter get itemid is null");
    }
    ItemBaseDO item = null;
    ItemBaseDTO result = null;
    if (query.getReturnAll()) {
      result = new ItemDTO();
      item = itemDOMapper.queryItem(itemId);
    } else {
      result = new ItemBaseDTO();
      item = itemDOMapper.queryBaseItem(itemId);
    }

    if (item == null) {
      return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST,
          "Input parameter get item is null");
    }

    ItemCommoditySkuDTO resultSkuDTO = new ItemCommoditySkuDTO();
    BeanCopierUtils.copyProperties(item, result);

    String pic = item.getPic();
    if (StringUtils.isNotEmpty(pic)) {
      List<String> picList = PicUtil.processPicListHeader(PicUtil.splitPicStr(pic), true);
      result.setPicUrl(picList.get(0));
      if (result instanceof ItemDTO) {
        ((ItemDTO) result).setPicList(picList);
        if (item instanceof ItemDO) {
          String introInfo = ((ItemDO) item).getIntroduction();
          List<IntroductDataDTO> introDTOList = getIntroductDataDTOList(introInfo, true);
          ((ItemDTO) result).setIntroductionDataList(introDTOList);
          ((ItemDTO) result).setIntroduction(processIntroductPicHeader(introDTOList, true));
        }
      }
    }

    if (null != resultSkuDTO) {
      BeanCopierUtils.copyProperties(skuDO, resultSkuDTO);
      result.setItemSkuDtos(Arrays.asList(resultSkuDTO));
    }

    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<List<ItemCommoditySkuDTO>> queryCommodityLikeTitle(ItemDTO itemDto,
      Integer pageSize, Integer currentPage) {

    if (null == itemDto || StringUtils.isEmpty(itemDto.getTitle()) || pageSize < 0 || currentPage < 0 ) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数对象为空");
    }

    List<ItemCommoditySkuDO> skuList =
        itemCommoditySkuDOMapper
            .query(SkuQuery.querySkuByTitle(itemDto.getTitle(), pageSize, currentPage),null);

    List<ItemCommoditySkuDTO> list = new ArrayList<>();
    if (null != skuList && CollectionUtils.isNotEmpty(skuList)) {
      for (ItemCommoditySkuDO itemCommoditySkuDO : skuList) {
        if (null == itemCommoditySkuDO) {
          continue;
        }
        ItemCommoditySkuDTO itemCommoditySkuDTO = new ItemCommoditySkuDTO();
        BeanCopierUtils.copyProperties(itemCommoditySkuDO, itemCommoditySkuDTO);
        list.add(itemCommoditySkuDTO);
      }
    }

    if (CollectionUtils.isEmpty(list)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询结果为空");
    }

    return Result.getSuccDataResult(list);
  }

  /**
   * 获取规格属性.这个地方谁能看懂。。。。。。
   * 
   * @param standardCatPropertyValueMap
   * @return
   */
  public void setSpecificationInfoWithDeal(ItemCommoditySkuDTO skuDTO) {
    Map<String, String> standardCatPropertyValueMap = skuDTO.queryStandardCatPropertyValueMap();

    // 取 销售规格 对应属性值
    String saleUnitValueId =
        standardCatPropertyValueMap.get(String.valueOf(PropertyDTO.SALEUNIT_PROPERTY_ID));
    String saleUnitStr =
        valueService.queryValueNameByValueId(Integer.valueOf(saleUnitValueId)).getModule();

    if (StringUtils.isEmpty(saleUnitValueId)) {
      return;
    }
    skuDTO.setSkuUnit(Integer.valueOf(saleUnitValueId));
    skuDTO.addSaleUnitStr(saleUnitStr);
    skuDTO.addSpecification("1" + saleUnitStr);
    return;
  }

  /**
   * 获取规格属性.这个地方谁能看懂。。。。。。
   * 
   * @param standardCatPropertyValueMap
   * @return
   */
  public void setSpecificationInfo(ItemCommoditySkuDTO skuDTO) {
    Map<String, String> standardCatPropertyValueMap = skuDTO.queryStandardCatPropertyValueMap();
    Map<String, String> unStandardCatPropertyValueMap = skuDTO.queryUnStandardCatPropertyValueMap();

    // 取采购规格 对应的属性值
    String buyUnitValueId =
        standardCatPropertyValueMap.get(String.valueOf(PropertyDTO.BUYUNIT_PROPERTY_ID));
    // 取 销售规格 对应属性值
    String saleUnitValueId =
        standardCatPropertyValueMap.get(String.valueOf(PropertyDTO.SALEUNIT_PROPERTY_ID));

    // 取 件装单位属性 对应属性值
    String countUnitValueId =
        standardCatPropertyValueMap.get(String.valueOf(PropertyDTO.COUNTUNIT_PROPERTY_ID));

    // 取 件装属性 对应属性值
    String groupUnitValueId =
        standardCatPropertyValueMap.get(String.valueOf(PropertyDTO.GOURPUNIT_PROPERTY_ID));

    // // 自然属性 对应属性值
    // String naturalUnitValueId =
    // standardCatPropertyValueMap.get(String.valueOf(PropertyDTO.NATURALUNIT_PROPERTY_ID));

    // 输入属性不足，无法获取
    if (StringUtils.isEmpty(saleUnitValueId) || StringUtils.isEmpty(countUnitValueId)
        || StringUtils.isEmpty(groupUnitValueId) || StringUtils.isEmpty(buyUnitValueId)) {
      return;
    }
    logger.error(JackSonUtil.getJson(skuDTO));

    // 件 数量
    String countValue =
        unStandardCatPropertyValueMap.get(String.valueOf(PropertyDTO.COUNT_PROPERTY_ID));

    // 件 中文单位
    String countUnitStr =
        valueService.queryValueNameByValueId(Integer.valueOf(countUnitValueId)).getModule();

    String saleUnitStr =
        valueService.queryValueNameByValueId(Integer.valueOf(saleUnitValueId)).getModule();

    skuDTO.addSaleRangeNum(1);// 默认为1,称重类另算
    if ("3".equals(saleUnitValueId)) {// 属于称重类商品，销售单位=kg
      float skuCount = 0;
      if ("3".equals(countUnitValueId)) {// 采购按照公斤采购
        skuCount = new BigDecimal(countValue).floatValue();
      } else if ("32".equals(countUnitValueId) || "71".equals(countUnitValueId)) {// 采购按照斤、克采购
        throw new UnsupportedOperationException("斤  or g buy unit is not support");
      }
      skuDTO.addSaleUnitType(SkuSaleUnitConstants.NATURAL);
      skuDTO.setSkuUnit(3);// 销售单位都是kg代表称重方式为公斤称重
      String saleRange = unStandardCatPropertyValueMap.get("26");// 用户最小起订量
      if (StringUtils.isNotEmpty(saleRange) && StringUtils.isNumeric(saleRange) && skuCount > 0) {
        int saleRangeNum = Integer.valueOf(saleRange);
        if (saleRangeNum <= 0) {
          saleRangeNum = 500;
        }
        skuDTO.setSkuCount(skuCount);
        skuDTO.addSaleRangeNum(saleRangeNum);
      } else {
        skuDTO.setSkuCount(0);
        skuDTO.addSaleRangeNum(0);
      }
      skuDTO.addSaleUnitStr("kg");
      skuDTO.addSpecification("散称1份/" + skuDTO.getSaleRangeNum() + "g");
      skuDTO.addUnSpecification("kg");
      return;
    } else if ("32".equals(saleUnitValueId) || "71".equals(saleUnitValueId)) {
      throw new UnsupportedOperationException("斤  or g sale unit is not support");
    }
    skuDTO.setSkuUnit(Integer.valueOf(saleUnitValueId));
    skuDTO.addSaleUnitStr(saleUnitStr);
    if (buyUnitValueId.equals(saleUnitValueId)) {// 采购单位=销售单位 按采购单位直接销售
      skuDTO.addSaleUnitType(SkuSaleUnitConstants.BOX);
      skuDTO.setSkuCount(1);
      if (StringUtils.isNotEmpty(saleUnitStr) && !saleUnitStr.equals(countUnitStr)) {
        skuDTO.addSpecification("1" + saleUnitStr + "/" + countValue + countUnitStr);
      } else {
        skuDTO.addSpecification(countValue + countUnitStr);
      }
      skuDTO.addUnSpecification(skuDTO.getSpecification());
      return;
    }
    if (groupUnitValueId.equals(saleUnitValueId) && countUnitValueId.equals(saleUnitValueId)
        && (!buyUnitValueId.equals(saleUnitValueId))) {// 件装属性=销售单位，销售单位=件装单位属性，采购单位!=销售单位
      skuDTO.addSaleUnitType(SkuSaleUnitConstants.UNIT);
      skuDTO.setSkuCount(Float.valueOf(countValue));
      skuDTO.addSpecification("1" + countUnitStr);
      skuDTO.addUnSpecification(skuDTO.getSpecification());
      return;
    }
    if (groupUnitValueId.equals(saleUnitValueId)) {// 件装属性=销售单位
      skuDTO.addSaleUnitType(SkuSaleUnitConstants.GROUP);
      String groupCountValue =
          unStandardCatPropertyValueMap.get(String.valueOf(PropertyDTO.GOURP_PROPERTY_ID));
      skuDTO.setSkuCount(new BigDecimal(countValue).divide(new BigDecimal(groupCountValue), 3,
          BigDecimal.ROUND_HALF_EVEN).floatValue());
      skuDTO.addSpecification("1" + saleUnitStr + "/" + groupCountValue + countUnitStr);
      skuDTO.addUnSpecification(skuDTO.getSpecification());
      return;
    }
    if (groupUnitValueId.equals(buyUnitValueId) && countUnitValueId.equals(saleUnitValueId)
        && (!buyUnitValueId.equals(saleUnitValueId))) {// 件装属性=采购单位，销售单位=件装单位属性，采购单位!=销售单位
      skuDTO.addSaleUnitType(SkuSaleUnitConstants.UNIT);
      skuDTO.setSkuCount(Float.valueOf(countValue));
      skuDTO.addSpecification("1" + countUnitStr);
      skuDTO.addUnSpecification(skuDTO.getSpecification());
      return;
    }
    return;
  }

  /**
   * 更新价格属性，由于称重类型商品线上线下两种销售各式，需要解析不同格式的价格字段
   * 
   * @param skuDTO
   */
  public void setPriceInfo(ItemCommoditySkuDTO skuDTO, ItemDO itemDO, boolean isInsert) {
    int price = skuDTO.getUnitPrice();
    int discountprice = 0;
    if (isInsert) {
      discountprice = skuDTO.getUnitPrice();
    } else {
      discountprice = skuDTO.getDiscountUnitPrice();
    }

//    if (!isInsert) {
//      if (discountprice > price && price > 0) {
//        discountprice = price;
//        skuDTO.setDiscountUnitPrice(price);
//        skuDTO.setUnitPrice(price);
//        skuDTO.setDiscountPrice(price);
//        skuDTO.setPrice(price);
//      }
//    }

    if (SkuSaleUnitConstants.NATURAL.equals(skuDTO.querySaleUnitType())) {
      // 非加工类，并且属于自然不可分割称重类的
      int rangeNum = skuDTO.getSaleRangeNum();
      if (rangeNum > 0) {
        skuDTO.setPrice(new BigDecimal(rangeNum).multiply(new BigDecimal(price))
            .divide(new BigDecimal(1000)).intValue());
        skuDTO.setDiscountPrice(new BigDecimal(rangeNum).multiply(new BigDecimal(discountprice))
            .divide(new BigDecimal(1000)).intValue());
      } else {// 默认1斤
        skuDTO.setPrice(new BigDecimal(price).divide(new BigDecimal(2)).intValue());
        skuDTO.setDiscountPrice(new BigDecimal(discountprice).divide(new BigDecimal(2)).intValue());
      }
    }

    if (null != skuDTO.getPrice() && skuDTO.getPrice() == 0) {
      itemDO.setPrice(999999);
      skuDTO.setPrice(999999);
    } else {
      itemDO.setPrice(skuDTO.getPrice());
    }
    if (null == skuDTO.getDiscountPrice()
        || (null != skuDTO.getDiscountPrice() && skuDTO.getDiscountPrice() == 0)) {
      itemDO.setDiscountPrice(itemDO.getPrice());
      skuDTO.setDiscountPrice(skuDTO.getPrice());
    } else {
      itemDO.setDiscountPrice(skuDTO.getDiscountPrice());
    }
  }
}
