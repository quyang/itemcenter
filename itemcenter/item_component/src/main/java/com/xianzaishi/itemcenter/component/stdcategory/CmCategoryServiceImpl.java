package com.xianzaishi.itemcenter.component.stdcategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.stdcategory.CmCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CmCategoryDTO;
import com.xianzaishi.itemcenter.common.result.Result;

@Service("cmcategoryservice")
public class CmCategoryServiceImpl implements CmCategoryService {

  @Override
  public Result<List<CmCategoryDTO>> getdefaultCategoryList() {
    List<CmCategoryDTO> tmp = new ArrayList<CmCategoryDTO>();
    CmCategoryDTO cm1 = new CmCategoryDTO();
    cm1.setCatId(107);
    cm1.setName("新鲜果蔬");
    tmp.add(cm1);
    
    CmCategoryDTO cm2 = new CmCategoryDTO();
    cm2.setCatId(83);
    cm2.setName("肉禽蛋类");
    tmp.add(cm2);
    
    CmCategoryDTO cm3 = new CmCategoryDTO();
    cm3.setCatId(55);
    cm3.setName("海鲜水产");
    tmp.add(cm3);
    
    CmCategoryDTO cm4 = new CmCategoryDTO();
    cm4.setCatId(2);
    cm4.setName("牛奶乳品");
    tmp.add(cm4);
    
    CmCategoryDTO cm5 = new CmCategoryDTO();
    cm5.setCatId(12);
    cm5.setName("冷冻冷藏");
    tmp.add(cm5);
    
    CmCategoryDTO cm6 = new CmCategoryDTO();
    cm6.setCatId(10000);
    cm6.setName("酒水零食");
    tmp.add(cm6);
    
    CmCategoryDTO cm7 = new CmCategoryDTO();
    cm7.setCatId(166);
    cm7.setName("粮油副食");
    tmp.add(cm7);
    
    CmCategoryDTO cm9 = new CmCategoryDTO();
    cm9.setCatId(481);
    cm9.setName("熟食料理");
    tmp.add(cm9);
    
    CmCategoryDTO cm10 = new CmCategoryDTO();
    cm10.setCatId(542);
    cm10.setName("精品烘培");
    tmp.add(cm10);
    
    CmCategoryDTO cm8 = new CmCategoryDTO();
    cm8.setCatId(10001);
    cm8.setName("厨卫清洁");
    tmp.add(cm8);
    
    return Result.getSuccDataResult(tmp);
  }

  /**
   * 前台类目映射关系，只初始部分数据，后续数据系统运行过程中自动补充
   */
  public static Map<Integer,List<CmCategoryDTO>> CM_STD_MAP = new HashMap<Integer,List<CmCategoryDTO>>(){

    private static final long serialVersionUID = 2L;
    {
      List<CmCategoryDTO> chuweiCatList1 = Lists.newArrayList();
      CmCategoryDTO chuweiCat1 = new CmCategoryDTO();
      chuweiCat1.setCatId(393);
      chuweiCat1.setName("餐具");
      
      CmCategoryDTO chuweiCat2 = new CmCategoryDTO();
      chuweiCat2.setCatId(400);
      chuweiCat2.setName("烹饪用具");
      
      CmCategoryDTO chuweiCat3 = new CmCategoryDTO();
      chuweiCat3.setCatId(407);
      chuweiCat3.setName("一次性用品");
      
      CmCategoryDTO chuweiCat4 = new CmCategoryDTO();
      chuweiCat4.setCatId(421);
      chuweiCat4.setName("家庭清洁");
      
      CmCategoryDTO chuweiCat5 = new CmCategoryDTO();
      chuweiCat5.setCatId(428);
      chuweiCat5.setName("纸制品 ");
      
      chuweiCatList1.add(chuweiCat1);
      chuweiCatList1.add(chuweiCat2);
      chuweiCatList1.add(chuweiCat3);
      chuweiCatList1.add(chuweiCat4);
      chuweiCatList1.add(chuweiCat5);
      
      
      List<CmCategoryDTO> yinliaoCatList1 = Lists.newArrayList();
      CmCategoryDTO yinliaoCat1 = new CmCategoryDTO();
      yinliaoCat1.setCatId(141);
      yinliaoCat1.setName("蜂制品");
      
      CmCategoryDTO yinliaoCat2 = new CmCategoryDTO();
      yinliaoCat2.setCatId(144);
      yinliaoCat2.setName("咖啡制品");
      
      CmCategoryDTO yinliaoCat3 = new CmCategoryDTO();
      yinliaoCat3.setCatId(151);
      yinliaoCat3.setName("茶/茶包");
      
      CmCategoryDTO yinliaoCat4 = new CmCategoryDTO();
      yinliaoCat4.setCatId(160);
      yinliaoCat4.setName("早餐食品");
      
      CmCategoryDTO yinliaoCat5 = new CmCategoryDTO();
      yinliaoCat5.setCatId(256);
      yinliaoCat5.setName("派/糕点");
      
      CmCategoryDTO yinliaoCat6 = new CmCategoryDTO();
      yinliaoCat6.setCatId(267);
      yinliaoCat6.setName("饼干");
      
      CmCategoryDTO yinliaoCat7 = new CmCategoryDTO();
      yinliaoCat7.setCatId(275);
      yinliaoCat7.setName("巧克力");
      
      CmCategoryDTO yinliaoCat8 = new CmCategoryDTO();
      yinliaoCat8.setCatId(283);
      yinliaoCat8.setName("糖果");
      
      CmCategoryDTO yinliaoCat9 = new CmCategoryDTO();
      yinliaoCat9.setCatId(292);
      yinliaoCat9.setName("果铺蜜饯");
      
      CmCategoryDTO yinliaoCat10 = new CmCategoryDTO();
      yinliaoCat10.setCatId(306);
      yinliaoCat10.setName("果冻布丁");
      
      CmCategoryDTO yinliaoCat11 = new CmCategoryDTO();
      yinliaoCat11.setCatId(308);
      yinliaoCat11.setName("风味小食");
      
      CmCategoryDTO yinliaoCat12 = new CmCategoryDTO();
      yinliaoCat12.setCatId(320);
      yinliaoCat12.setName("膨化食品");
      
      CmCategoryDTO yinliaoCat13 = new CmCategoryDTO();
      yinliaoCat13.setCatId(328);
      yinliaoCat13.setName("坚果炒货");
      
      CmCategoryDTO yinliaoCat14 = new CmCategoryDTO();
      yinliaoCat14.setCatId(345);
      yinliaoCat14.setName("水");

      CmCategoryDTO yinliaoCat15 = new CmCategoryDTO();
      yinliaoCat15.setCatId(350);
      yinliaoCat15.setName("茶饮料");
      
      CmCategoryDTO yinliaoCat16 = new CmCategoryDTO();
      yinliaoCat16.setCatId(357);
      yinliaoCat16.setName("功能/其他饮料");
      
      CmCategoryDTO yinliaoCat17 = new CmCategoryDTO();
      yinliaoCat17.setCatId(362);
      yinliaoCat17.setName("果汁饮料");
      
      CmCategoryDTO yinliaoCat18 = new CmCategoryDTO();
      yinliaoCat18.setCatId(370);
      yinliaoCat18.setName("碳酸饮料");
      
      CmCategoryDTO yinliaoCat19 = new CmCategoryDTO();
      yinliaoCat19.setCatId(376);
      yinliaoCat19.setName("酒类");
      
      CmCategoryDTO yinliaoCat20 = new CmCategoryDTO();
      yinliaoCat20.setCatId(437);
      yinliaoCat20.setName("冰激淋");
      
      CmCategoryDTO yinliaoCat21 = new CmCategoryDTO();
      yinliaoCat21.setCatId(440);
      yinliaoCat21.setName("新鲜果汁");
      
      CmCategoryDTO yinliaoCat22 = new CmCategoryDTO();
      yinliaoCat22.setCatId(521);
      yinliaoCat22.setName("茶类饮料");
      
      CmCategoryDTO yinliaoCat23 = new CmCategoryDTO();
      yinliaoCat23.setCatId(525);
      yinliaoCat23.setName("苏打水/气泡水");
      
      CmCategoryDTO yinliaoCat24 = new CmCategoryDTO();
      yinliaoCat24.setCatId(529);
      yinliaoCat24.setName("欧蕾");
      
      CmCategoryDTO yinliaoCat25 = new CmCategoryDTO();
      yinliaoCat25.setCatId(532);
      yinliaoCat25.setName("咖啡");
      
      CmCategoryDTO yinliaoCat26 = new CmCategoryDTO();
      yinliaoCat26.setCatId(534);
      yinliaoCat26.setName("果昔/奶昔");
      
      CmCategoryDTO yinliaoCat27 = new CmCategoryDTO();
      yinliaoCat27.setCatId(556);
      yinliaoCat27.setName("果切");
      
      yinliaoCatList1.add(yinliaoCat1);
      yinliaoCatList1.add(yinliaoCat2);
      yinliaoCatList1.add(yinliaoCat3);
      yinliaoCatList1.add(yinliaoCat4);
      yinliaoCatList1.add(yinliaoCat5);
      yinliaoCatList1.add(yinliaoCat6);
      yinliaoCatList1.add(yinliaoCat7);
      yinliaoCatList1.add(yinliaoCat8);
      yinliaoCatList1.add(yinliaoCat9);
      yinliaoCatList1.add(yinliaoCat10);
      yinliaoCatList1.add(yinliaoCat11);
      yinliaoCatList1.add(yinliaoCat12);
      yinliaoCatList1.add(yinliaoCat13);
      yinliaoCatList1.add(yinliaoCat14);
      yinliaoCatList1.add(yinliaoCat15);
      yinliaoCatList1.add(yinliaoCat16);
      yinliaoCatList1.add(yinliaoCat17);
      yinliaoCatList1.add(yinliaoCat18);
      yinliaoCatList1.add(yinliaoCat19);
      yinliaoCatList1.add(yinliaoCat20);
      yinliaoCatList1.add(yinliaoCat21);
      yinliaoCatList1.add(yinliaoCat22);
      yinliaoCatList1.add(yinliaoCat23);
      yinliaoCatList1.add(yinliaoCat24);
      yinliaoCatList1.add(yinliaoCat25);
      yinliaoCatList1.add(yinliaoCat26);
      yinliaoCatList1.add(yinliaoCat27);
      
      List<CmCategoryDTO> waimaiCatList1 = Lists.newArrayList();
      CmCategoryDTO waimaiCat1 = new CmCategoryDTO();
      waimaiCat1.setCatId(505);
      waimaiCat1.setName("主食料理");
      
      CmCategoryDTO waimaiCat2 = new CmCategoryDTO();
      waimaiCat2.setCatId(482);
      waimaiCat2.setName("精致轻餐");
      
      CmCategoryDTO waimaiCat5 = new CmCategoryDTO();
      waimaiCat5.setCatId(556);
      waimaiCat5.setName("新鲜果切");
      
      CmCategoryDTO waimaiCat3 = new CmCategoryDTO();
      waimaiCat3.setCatId(440);
      waimaiCat3.setName("果汁咖啡");
      
      CmCategoryDTO waimaiCat4 = new CmCategoryDTO();
      waimaiCat4.setCatId(462);
      waimaiCat4.setName("小食甜点");
      
      waimaiCatList1.add(waimaiCat1);
      waimaiCatList1.add(waimaiCat2);
      waimaiCatList1.add(waimaiCat5);
      waimaiCatList1.add(waimaiCat3);
      waimaiCatList1.add(waimaiCat4);
      
      List<CmCategoryDTO> hongpeiCatList1 = Lists.newArrayList();
      CmCategoryDTO hongpeiCat1 = new CmCategoryDTO();
      hongpeiCat1.setCatId(544);
      hongpeiCat1.setName("蛋糕");
      
      
      hongpeiCatList1.add(hongpeiCat1);
      
      put(10000,yinliaoCatList1);
      put(10001,chuweiCatList1);
      put(10002,waimaiCatList1);
      put(10003,hongpeiCatList1);
    }
  };
}
