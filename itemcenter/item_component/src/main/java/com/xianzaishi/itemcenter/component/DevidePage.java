package com.xianzaishi.itemcenter.component;

/**
 * 分页
 * 
 * @author dongpo
 * 
 */
public class DevidePage {

  /**
   * 每页显示的条数
   */
  private Integer pageSize;

  /**
   * 记录的总条数
   */
  private Integer recordCount;

  /**
   * 当前页
   */
  private Integer currentPage;

  /**
   * 总页数
   */
  private Integer pageCount;

  public DevidePage(Integer pageSize, Integer recordCount, Integer currentPage) {
    this.pageSize = pageSize;
    this.recordCount = recordCount;
    this.setCurrentPage(currentPage);
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

  public Integer getRecordCount() {
    return recordCount;
  }

  public void setRecordCount(Integer recordCount) {
    this.recordCount = recordCount;
  }

  /**
   * 获取总页数
   * 
   * @return
   */
  public Integer getPageCount() {
    pageCount = recordCount / pageSize;
    int mod = recordCount % pageSize;
    if (mod != 0) {
      pageCount++;
    }
    return pageCount == 0 ? 1 : pageCount;
  }

  /**
   * 获取当前页
   * 
   * @return
   */
  public Integer getCurrentPage() {
    return currentPage;
  }

  /**
   * 设置定位在当前页
   */
  private void setCurrentPage(Integer currentPage) {
    Integer activePage = currentPage <= 0 ? 1 : currentPage;
    activePage = activePage > getPageCount() ? getPageCount() : activePage;
    this.currentPage = activePage;
  }

  /**
   * 获取返回的起始商品序号
   * 
   * @return
   */
  public Integer getFromIndex() {
    return (currentPage - 1) * pageSize;
  }

  /**
   * 获取返回的结束商品序号
   * 
   * @return
   */
  public Integer getToIndex() {
    return Math.min(recordCount, currentPage * pageSize);
  }

}
