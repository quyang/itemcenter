package com.xianzaishi.itemcenter.component.presale;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.presale.ItemPreSaleService;
import com.xianzaishi.itemcenter.client.presale.dto.ItemPreSaleDTO;
import com.xianzaishi.itemcenter.client.presale.query.ItemPreSaleQuery;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.itemcenter.dal.presale.dao.ItemPreSaleDOMapper;
import com.xianzaishi.itemcenter.dal.presale.dataobject.ItemPreSaleDO;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * 预售接口功能实现
 *
 * @author jianmo
 * @create 2016-12-23 上午 10:21
 **/
@Service("itemPreSaleService")
public class ItemPreSaleServiceImpl implements ItemPreSaleService {

    private static Logger logger = Logger.getLogger(ItemPreSaleServiceImpl.class);

    @Autowired
    private ItemPreSaleDOMapper itemPreSaleDOMapper;

    @Override
    public Result<ItemPreSaleDTO> queryItemPreSaleById(Long preSaleId) {
        if(null==preSaleId) return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
        ItemPreSaleDO itemPreSaleDO =  itemPreSaleDOMapper.selectByPrimaryKey(preSaleId);
        ItemPreSaleDTO itemPreSaleDTO =  new ItemPreSaleDTO();
        BeanCopierUtils.copyProperties(itemPreSaleDO,itemPreSaleDTO);
        return Result.getSuccDataResult(itemPreSaleDTO);
    }

    @Override
    public Result<ItemPreSaleDTO> queryItemPreSaleBySkuId(Long preSaleSkuId) {
        if(null==preSaleSkuId) return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
        ItemPreSaleDO itemPreSaleDO =  itemPreSaleDOMapper.queryBySkuId(preSaleSkuId);
        ItemPreSaleDTO itemPreSaleDTO =  new ItemPreSaleDTO();
        BeanCopierUtils.copyProperties(itemPreSaleDO,itemPreSaleDTO);
        return Result.getSuccDataResult(itemPreSaleDTO);
    }

    @Override
    public Result<List<ItemPreSaleDTO>> queryItemPreSales(ItemPreSaleQuery query) {
        if(null == query) return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
        List<ItemPreSaleDO>  itemPreSaleDOs = itemPreSaleDOMapper.queryItemPreSales(query);
        if(CollectionUtils.isEmpty(itemPreSaleDOs)) return Result.getSuccDataResult(Collections.<ItemPreSaleDTO>emptyList());
        List<ItemPreSaleDTO> itemPreSaleDTOs = Lists.newArrayList();
        BeanCopierUtils.copyListBean(itemPreSaleDOs,itemPreSaleDTOs,ItemPreSaleDTO.class);
        return Result.getSuccDataResult(itemPreSaleDTOs);
    }

    @Override
    public Result<Integer> queryItemPreSalesCount(ItemPreSaleQuery query) {
        if(null == query) return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
        Integer count = itemPreSaleDOMapper.queryItemPreSalesCount(query);
        return Result.getSuccDataResult(count);
    }
}
