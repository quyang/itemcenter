package com.xianzaishi.itemcenter.component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 返回result<map>类型
 * 
 * @author dongpo
 * 
 */
public class ResultData {


  @SuppressWarnings({"unchecked", "rawtypes"})
  public static Result<Map> getSuccessData(Object object) {
    Map map = new HashMap<>();
    map.put("data", object);// 将当前页的数据添加到map中
    int count = object instanceof List ? ((List<Object>) object).size() : 1;// list总数量
    map.put("count", count); // 返回数量
    return Result.getSuccDataResult(map);
  }

  @SuppressWarnings("rawtypes")
  public static Result<Map> getErrorData(int resultCode, String errorMsg) {
    String msgString = "传入参数不符合查询条件";
    errorMsg = errorMsg == null ? msgString : errorMsg;
    return Result.getErrDataResult(resultCode, errorMsg);
  }
}
