package com.xianzaishi.itemcenter.component.item;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.ProductService;
import com.xianzaishi.itemcenter.client.item.dto.ItemProductDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.pic.PicUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemProductSkuDO;
import com.xianzaishi.itemcenter.dal.product.dataobject.ItemProductDO;

@Service("productService")
public class ProductServiceImpl extends ItemService implements ProductService {

  private static final Logger logger = Logger.getLogger(ProductServiceImpl.class);
  
  @Override
  public Result<Long> insertItemProduct(ItemProductDTO productDTO) {
    if (null == productDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input itemProduct parameter is null");
    }

    if (null == productDTO.getCategoryId() || productDTO.getCategoryId() <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert product catid error");
    }

    if (CollectionUtils.isEmpty(productDTO.getItemProductSkuList())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert product skuList is null");
    }

    Long productSkuId = getItemCodeId(productDTO.getCategoryId());
    
    Long productId = super.getProductId();
    ItemProductDO productDO = new ItemProductDO();
    BeanCopierUtils.copyProperties(productDTO, productDO);// Dto转换为DO
    productDO.setSku(String.valueOf(productSkuId) + ItemProductDO.SPLIT_CHARATER);
    productDO.setPic(PicUtil.joinPicStr(PicUtil.processPicListHeader(productDTO.getPicList(), false)));
    productDO.setIntroduction(processIntroductPicHeader(productDTO.getIntroduction(), false));

    productDO.setProductId(productId);
    ItemProductSkuDTO productSkuDTO = productDTO.getItemProductSkuList().get(0);
    productSkuDTO.setSkuCode(productSkuId);
    productSkuDTO.setSkuId(productSkuId);
    productSkuDTO.setItemId(productId);
    ItemProductSkuDO productSkuDO = new ItemProductSkuDO();
    BeanCopierUtils.copyProperties(productSkuDTO, productSkuDO);// Dto转换为DO

    int insertProductSkuCount = itemProductSkuDOMapper.insert(productSkuDO);
    if (insertProductSkuCount != 1) {
      return Result
          .getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert ProductSku db error");
    }

    int count = itemProductDOMapper.insert(productDO);
    if (count == 1) {
      logSkuUpPrice(productSkuDO,null,null,0L,true);
      return Result.getSuccDataResult(productSkuId);
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR_PRODUCT_ITEM_INSERT,
          "insert product db error");
    }
  }

  @Override
  public Result<Boolean> updateItemProduct(ItemProductDTO productDTO) {
    if (null == productDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "update itemProduct parameter is null");
    }

    ItemProductDO productDO = new ItemProductDO();
    BeanCopierUtils.copyProperties(productDTO, productDO);// Dto转换为DO
    
    if (null == productDO.getCategoryId() || productDO.getCategoryId() <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert product catid error");
    }

    if (CollectionUtils.isEmpty(productDTO.getItemProductSkuList())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert product skuList is null");
    }
    
    productDO.setPic(PicUtil.joinPicStr(PicUtil.processPicListHeader(productDTO.getPicList(), false)));
    productDO.setIntroduction(processIntroductPicHeader(productDTO.getIntroduction(), false));
    
    ItemProductSkuDTO productSkuDTO = productDTO.getItemProductSkuList().get(0);

    if(null != productSkuDTO){
      ItemProductSkuDO productSkuDO = new ItemProductSkuDO();
      BeanCopierUtils.copyProperties(productSkuDTO, productSkuDO);// Dto转换为DO
      
      List<ItemProductSkuDO> queryResult = itemProductSkuDOMapper.query(SkuQuery.querySkuById(productSkuDTO.getSkuId()));
      if(CollectionUtils.isEmpty(queryResult)){
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update productsku,query is null,id is:" + productSkuDTO.getSkuId());
      }
      ItemProductSkuDO dbSku = queryResult.get(0);
      int updateProductSkuCount = itemProductSkuDOMapper.update(productSkuDO);
      if (updateProductSkuCount != 1) {
        return Result
            .getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update ProductSku db error");
      }else{
        logSkuUpPrice(productSkuDO,dbSku,null,0L,true);
      }
    }
    
    
    int count = itemProductDOMapper.update(productDO);
    if (count == 1) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR_PRODUCT_ITEM_UPDATE,
          "update product db error");
    }
  }

  @Override
  public Result<ItemProductDTO> queryItemProduct(Long productId, Boolean includeSku) {
    if (null == productId || productId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input productId parameter is null or productId <= 0");
    }
    ItemProductDO product = itemProductDOMapper.query(productId);
    if (null == product) {
      return Result.getSuccDataResult(null);
    }
    ItemProductDTO result = new ItemProductDTO();
    BeanCopierUtils.copyProperties(product, result);
    result.setPicList(PicUtil.processPicListHeader(PicUtil.splitPicStr(product.getPic()), true));
    result.setIntroduction(processIntroductPicHeader(product.getIntroduction(),true));
    
    if (null != includeSku && includeSku) {
      List<ItemProductSkuDO> tmpResult =
          itemProductSkuDOMapper.query(SkuQuery.querySkuByItem(productId));
      if (CollectionUtils.isNotEmpty(tmpResult)) {
        ItemProductSkuDO tmpProductSku = tmpResult.get(0);
        ItemProductSkuDTO tmpProductSkuDTO = new ItemProductSkuDTO();
        BeanCopierUtils.copyProperties(tmpProductSku, tmpProductSkuDTO);
        result.setItemProductSkuList(Arrays.asList(tmpProductSkuDTO));
      }
    }

    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<ItemProductDTO> queryItemProductBySku(Long skuId, Boolean includeSku) {
    if (null == skuId || skuId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input skuId parameter is null or productId <= 0");
    }
    List<ItemProductSkuDO> skuDOlist =
        itemProductSkuDOMapper.query(SkuQuery.querySkuBySkuIds(Arrays.asList(skuId)));
    if (CollectionUtils.isEmpty(skuDOlist)) {
      return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "Query product not exit");
    }
    ItemProductSkuDO skuDO = skuDOlist.get(0);
    Long productId = skuDO.getItemId();
    if (productId <= 0) {
      Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "db error data,skuid=" + skuId
          + " item id < 0");
    }

    Result<ItemProductDTO> result = this.queryItemProduct(productId, false);
    if (null != result && result.getSuccess()) {
      if (includeSku) {
        ItemProductSkuDTO tmpProductSkuDTO = new ItemProductSkuDTO();
        BeanCopierUtils.copyProperties(skuDO, tmpProductSkuDTO);
        result.getModule().setItemProductSkuList(Arrays.asList(tmpProductSkuDTO));
      }
      return result;
    }
    return Result.getErrDataResult(ServerResultCode.ITEM_UNEXIST, "Query product not exit");
  }

  @Override
  public Result<ItemProductDTO> queryItemProductByItemCode(Long itemCode, Boolean includeSku) {
    if (null == itemCode || itemCode <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input itemCode parameter is null or productId <= 0");
    }
    List<ItemProductSkuDO> skuDOlist =
        itemProductSkuDOMapper.query(SkuQuery.querySkuById(itemCode));
    if (CollectionUtils.isEmpty(skuDOlist)) {
      return Result.getSuccDataResult(null);
    }
    ItemProductSkuDO skuDO = skuDOlist.get(0);
    Long productId = skuDO.getItemId();
    if (productId <= 0) {
      Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "db error data,itemCode=" + itemCode
          + " item id < 0");
    }

    Result<ItemProductDTO> result = this.queryItemProduct(productId, false);
    if (null != result && result.getSuccess()) {
      if (includeSku) {
        ItemProductSkuDTO tmpProductSkuDTO = new ItemProductSkuDTO();
        BeanCopierUtils.copyProperties(skuDO, tmpProductSkuDTO);
        result.getModule().setItemProductSkuList(Arrays.asList(tmpProductSkuDTO));
      }
      return result;
    }
    return Result.getSuccDataResult(null);
  }

  @Override
  public Result<List<ItemProductDTO>> queryItemProductByIdList(List<Long> productIds) {
    if (CollectionUtils.isEmpty(productIds)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query param is null");
    }
    List<ItemProductDO> products = itemProductDOMapper.batchSelectByIds(productIds);
    if (CollectionUtils.isEmpty(products)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "Query product not exit");
    }
    Map<Long, String> productPictures = Maps.newHashMap();
    Map<Long, String> productIntroductions = Maps.newHashMap();
    for(ItemProductDO itemProductDO : products){
      productPictures.put(itemProductDO.getProductId(), itemProductDO.getPic());
      productIntroductions.put(itemProductDO.getProductId(), itemProductDO.getIntroduction());
    }
    List<ItemProductDTO> itemProductDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(products, itemProductDTOs, ItemProductDTO.class);
    for (ItemProductDTO itemProductDTO : itemProductDTOs) {
      itemProductDTO.setPicList(PicUtil.processPicListHeader(PicUtil.splitPicStr(productPictures.get(itemProductDTO.getProductId())), true));
      itemProductDTO.setIntroduction(processIntroductPicHeader(productIntroductions.get(itemProductDTO.getProductId()),true));
    }

    return Result.getSuccDataResult(itemProductDTOs);
  }

}
