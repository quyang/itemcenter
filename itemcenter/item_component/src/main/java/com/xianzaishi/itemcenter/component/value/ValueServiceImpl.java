package com.xianzaishi.itemcenter.component.value;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.client.value.dto.ValueDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.itemcenter.dal.value.dao.BaseValueDOMapper;
import com.xianzaishi.itemcenter.dal.value.dao.PropertyValueDOMapper;
import com.xianzaishi.itemcenter.dal.value.dao.ValueDOMapper;
import com.xianzaishi.itemcenter.dal.value.dataobject.BaseValueDO;
import com.xianzaishi.itemcenter.dal.value.dataobject.PropertyValueDO;
import com.xianzaishi.itemcenter.dal.value.dataobject.ValueDO;

/**
 * 属性值实现类
 * 
 * @author dongpo
 * 
 */
@Service("valueservice")
public class ValueServiceImpl implements ValueService {
  private static Logger LOGGER = Logger.getLogger(ValueServiceImpl.class);

  @Autowired
  private BaseValueDOMapper baseValueDOMapper;

  @Autowired
  private ValueDOMapper valueDOMapper;

  @Autowired
  private PropertyValueDOMapper propertyValueDOMapper;

  /**
   * 缓存一份标准属性值的id与名称对应关系
   */
  private Map<Integer,String> baseValueName = Maps.newHashMap();

  public Integer insertBaseValue(BaseValueDO valueDO) {

    Integer insertNumber = baseValueDOMapper.insertBaseValue(valueDO);
    Integer valueId = insertNumber > 0 ? valueDO.getValueId() : 0;

    return valueId;
  }


  public Boolean updateBaseValue(BaseValueDO valueDO) {
    Integer updateNumber = baseValueDOMapper.updateBaseValue(valueDO);
    Boolean isUpdate = updateNumber > 0 ? true : false;

    return isUpdate;
  }

  @Override
  public Result<List<ValueDTO>> queryValue(Integer catId, Integer propertyId, Integer valueId) {
    if (catId != null && propertyId != null) {
      List<ValueDTO> valueDtos = new ArrayList<ValueDTO>();
      BeanCopierUtils.copyProperties(valueDOMapper.selectValueByPropertyId(catId, propertyId),
          valueDtos);
      return Result.getSuccDataResult(valueDtos);
    } 
    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_UNSUPPORTEDQUERY, null);
  }

  @Override
  public Result<Integer> queryValueIdByValueName(String valueName) {
    if (StringUtils.isEmpty(valueName)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input parameter is null");
    }
    BaseValueDO value = baseValueDOMapper.selectBaseValueByName(valueName);
    if(null != value){
      return Result.getSuccDataResult(value.getValueId());
    }else{
      return Result.getSuccDataResult(0);
    }
  }

  @Override
  public Result<String> queryValueNameByValueId(Integer valueId) {
    String name = baseValueName.get(valueId);
    if(StringUtils.isEmpty(name)){
      List<BaseValueDO> queryValueResult = baseValueDOMapper.selectBaseValueByIdList(Arrays.asList(valueId));
      if(CollectionUtils.isNotEmpty(queryValueResult) && queryValueResult.size() > 0){
        BaseValueDO value = queryValueResult.get(0);
        this.baseValueName.put(valueId, value.getValueData());
        return Result.getSuccDataResult(value.getValueData());
      }else{
        return Result.getErrDataResult(-1, "Query value retun null");
      }
    }
    return Result.getSuccDataResult(name);
  }

  @Override
  public Result<Map<Integer,String>> queryValueNameListByValueId(List<Integer> valueId) {
    List<Integer> tmpIdList = Lists.newArrayList();
    for(Integer id:valueId){
      if(!baseValueName.containsKey(id)){
        tmpIdList.add(id);
      }
    }
    
    if(CollectionUtils.isNotEmpty(tmpIdList)){
      List<BaseValueDO> queryValueResult = baseValueDOMapper.selectBaseValueByIdList(tmpIdList);
      if(CollectionUtils.isNotEmpty(queryValueResult) && queryValueResult.size() > 0){
        for(BaseValueDO tmpValue:queryValueResult){
          baseValueName.put(tmpValue.getValueId(), tmpValue.getValueData());
        }
      }
    }
    
    Map<Integer,String> result  = Maps.newHashMap();
    for(Integer id:valueId){
      String name = baseValueName.get(id);
      if(StringUtils.isEmpty(name)){
        name = "";
      }
      result.put(id, name);
    }
    return Result.getSuccDataResult(result);
  }

  public Integer insertValue(ValueDO valueDO) {

    Integer insertNumber = valueDOMapper.insertValue(valueDO);
    Integer valueId = insertNumber > 0 ? valueDO.getValueId() : 0;

    return valueId;
  }


  public Boolean updateValue(ValueDO valueDO) {

    Integer updateNumber = valueDOMapper.updateValue(valueDO);
    Boolean isUpdate = updateNumber > 0 ? true : false;

    return isUpdate;
  }


  @Override
  public Result<Integer> insertPropertyValue(ValueDTO valueDTO) {
    BaseValueDO baseValueDO = baseValueDOMapper.selectBaseValueByName(valueDTO.getValueData());
    ValueDO valueDO = new ValueDO();
    Integer valueId = null;
    if (null != baseValueDO) {
      valueId = baseValueDO.getValueId();
    } else {
      BaseValueDO baseValueDO2 = new BaseValueDO();
      BeanCopierUtils.copyProperties(valueDTO, baseValueDO2);// 获取基本属性值对象

      valueId = insertBaseValue(baseValueDO2);// 插入基本属性值对象，并获取自增的值id
    }
    BeanCopierUtils.copyProperties(valueDTO, valueDO);// 获取属性值对象
    if (valueId != null && valueId != 0) { // 基本属性值插入成功才能插入属性值
      valueDO.setValueId(valueId); // 设置属性值id
      try {

        valueId = insertValue(valueDO); // 插入属性值，并获取属性值id，此时valueId为0则说明插入属性值失败
      } catch (Exception e) {
        LOGGER.info(JackSonUtil.getJson(valueDO) + "属性值插入失败");
      }
      if (valueId != 0) {
        valueId = insertItemPropertyValue(valueDO);
      }
    }

    return Result.getSuccDataResult(valueId);
  }

  private Integer insertItemPropertyValue(ValueDO valueDO) {
    PropertyValueDO propertyValueDO =
        propertyValueDOMapper.selectByPrimaryKey(valueDO.getPropertyId(), valueDO.getValueId());
    Integer valueId = 0;
    if(null == propertyValueDO){
      
      PropertyValueDO propertyValueDO2 = new PropertyValueDO();
      propertyValueDO2.setPropertyId(valueDO.getPropertyId());
      propertyValueDO2.setValueId(valueDO.getValueId());
      propertyValueDO2.setStatus((short) 1);
      Integer insertNumber = propertyValueDOMapper.insertPropertyValue(propertyValueDO2);
      valueId = insertNumber > 0 ? propertyValueDO2.getValueId() : 0;
    }
    return valueId;
  }

  @Override
  public Result<Boolean> updatePropertyValue(ValueDTO valueDTO) {
    BaseValueDO baseValueDO = new BaseValueDO();
    ValueDO valueDO = new ValueDO();
    BeanCopierUtils.copyProperties(valueDTO, baseValueDO);// 获取基本属性值对象
    BeanCopierUtils.copyProperties(valueDTO, valueDO);// 获取属性值对象

    Boolean isUpdate = updateBaseValue(baseValueDO);
    if (isUpdate) {
      isUpdate = updateValue(valueDO);
    }
    return Result.getSuccDataResult(isUpdate);
  }



}
