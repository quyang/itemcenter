package com.xianzaishi.itemcenter.component.sysproperty;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.itemcenter.dal.sysproperty.dao.SystemConfigDOMapper;
import com.xianzaishi.itemcenter.dal.sysproperty.dataobject.SystemConfigDO;

@Service("systempropertyservice")
public class SystemPropertyServiceImpl implements SystemPropertyService {

  @Autowired
  private SystemConfigDOMapper systemConfigDOMapper;

  @Override
  public Result<SystemConfigDTO> getSystemConfig(Integer configId) {
    SystemConfigDO queryJson = systemConfigDOMapper.selectByPrimaryKey(configId);
    if (null != queryJson && StringUtils.isNotEmpty(queryJson.getConfigInfo())) {
      SystemConfigDTO config = new SystemConfigDTO();
      config.setConfigId(configId);
      config.setConfigInfo(queryJson.getConfigInfo());
      return Result.getSuccDataResult(config);
    } else {
      return Result.getErrDataResult(-1, "Query config not exit");
    }
  }

  @Override
  public Result<Boolean> updateSystemConfig(SystemConfigDTO config) {
    if(null == config){
      return Result.getErrDataResult(-1, "update parameter error");
    }
    SystemConfigDO result = new SystemConfigDO();
    result.setConfigId(config.getConfigId());
    result.setConfigInfo(config.getConfigInfo());
    int count = systemConfigDOMapper.updateByPrimaryKey(result);
    if(count == 1){
      return Result.getSuccDataResult(true);
    }
    return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update db error");
  }
  
}
