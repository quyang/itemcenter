package com.xianzaishi.itemcenter.component.stdcategory;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.itemcenter.dal.property.dao.PropertyDOMapper;
import com.xianzaishi.itemcenter.dal.stdcategory.dao.CategoryDOMapper;
import com.xianzaishi.itemcenter.dal.stdcategory.dataobject.CategoryDO;
import com.xianzaishi.itemcenter.dal.value.dao.ValueDOMapper;

/**
 * 后台类目实现类
 * 
 * @author dongpo
 * 
 */
@Service("stdcategoryservice")
public class StdCategoryServiceImpl implements StdCategoryService {

  @Autowired
  private CategoryDOMapper categoryDOMapper;

  @Autowired
  private PropertyDOMapper propertyDOMapper;

  @Autowired
  private ValueDOMapper valueDOMapper;


  @Override
  public Result<List<CategoryDTO>> queryCategoriesByIdList(List<Integer> catIds) {
    if(CollectionUtils.isEmpty(catIds)){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "query param is null");
    }
    List<CategoryDO> categoryDOs = categoryDOMapper.selectCategoriesByIdList(catIds);// 获取持久层数据
    List<CategoryDTO> categoryDTOs = new ArrayList<CategoryDTO>();// 返回客户端的数据类型
    BeanCopierUtils.copyListBean(categoryDOs, categoryDTOs, CategoryDTO.class);// DO数据类型转换为Dto数据类型

    return Result.getSuccDataResult(categoryDTOs);
  }


  @Override
  public Result<CategoryDTO> queryCategoryByName(String catName) {
    CategoryDO categoryDO = categoryDOMapper.selectCategoryByName(catName);// 通过类目Id获取数据
    CategoryDTO categoryDto = new CategoryDTO();
    BeanCopierUtils.copyProperties(categoryDO, categoryDto);// do层数据转换为dto层数据

    return Result.getSuccDataResult(categoryDto);
  }


  @Override
  public Result<CategoryDTO> queryCategoryById(Integer catId) {
    CategoryDO categoryDO = categoryDOMapper.selectCategoryById(catId);// 通过类目Id获取数据
    CategoryDTO categoryDto = new CategoryDTO();
    BeanCopierUtils.copyProperties(categoryDO, categoryDto);// do层数据转换为dto层数据

    return Result.getSuccDataResult(categoryDto);
  }

  @Override
  public Result<List<CategoryDTO>> querySubcategoryById(Integer catId) {
    List<CategoryDO> categoryDOs = categoryDOMapper.selectSubcategoryById(catId);// 获取持久层数据

    List<CategoryDTO> categoryDtos = new ArrayList<CategoryDTO>();// 返回的数据类型
    BeanCopierUtils.copyListBean(categoryDOs, categoryDtos, CategoryDTO.class);// do层数据转换为dto层数据


    return Result.getSuccDataResult(categoryDtos);
  }


  @Override
  public Result<Integer> insertCategory(CategoryDTO categoryDto) {
    CategoryDO categoryDO = new CategoryDO();
    BeanCopierUtils.copyProperties(categoryDto, categoryDO);
    Integer insertNumber = categoryDOMapper.insertCategory(categoryDO);
    Integer catId = insertNumber > 0 ? categoryDO.getCatId() : 0;

    return Result.getSuccDataResult(catId);
  }


  @Override
  public Result<Boolean> updateCategory(CategoryDTO categoryDto) {
    CategoryDO categoryDO = new CategoryDO();
    BeanCopierUtils.copyProperties(categoryDto, categoryDO);
    Integer updateNumber = categoryDOMapper.updateCategory(categoryDO);
    Boolean isUpdate = updateNumber > 0 ? true : false;
    return Result.getSuccDataResult(isUpdate);
  }


  @Override
  public Result<List<CategoryDTO>> queryCategoriesByCatCode(Long catCode) {
    if(null == catCode){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    List<CategoryDO> categoryDOs = categoryDOMapper.selectCategoryDOsByCatCode(catCode);
    if(null == categoryDOs){
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "category is not exit");
    }
    List<CategoryDTO> categoryDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(categoryDOs, categoryDTOs, CategoryDTO.class);
    
    return Result.getSuccDataResult(categoryDTOs);
  }

}
