package com.xianzaishi.itemcenter.component.item;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.dto.IntroductDataDTO;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO.SkuTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.pic.PicConstants;
import com.xianzaishi.itemcenter.common.pic.PicUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.itemcenter.component.common.SpringContextUtil;
import com.xianzaishi.itemcenter.dal.common.dao.SequenceDOMapper;
import com.xianzaishi.itemcenter.dal.item.dao.ItemDOMapper;
import com.xianzaishi.itemcenter.dal.item.dataobject.ItemDO;
import com.xianzaishi.itemcenter.dal.itemsku.dao.ItemCommoditySkuDOMapper;
import com.xianzaishi.itemcenter.dal.itemsku.dao.ItemProductSkuDOMapper;
import com.xianzaishi.itemcenter.dal.itemsku.dao.ItemSkuRelationDOMapper;
import com.xianzaishi.itemcenter.dal.itemsku.dao.ItemSkuRelationScheduleDOMapper;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemCommoditySkuDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemProductSkuDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuRelationDO;
import com.xianzaishi.itemcenter.dal.log.dao.ItemProcessLogDOMapper;
import com.xianzaishi.itemcenter.dal.log.dataobject.ItemProcessLogDO;
import com.xianzaishi.itemcenter.dal.product.dao.ItemProductDOMapper;
import com.xianzaishi.itemcenter.dal.stdcategory.dao.CategoryDOMapper;
import com.xianzaishi.itemcenter.dal.stdcategory.dataobject.CategoryDO;
import com.xianzaishi.msg.domain.RpcResult;
import com.xianzaishi.service.MessageService;

public class ItemService {

  private static final Logger logger = Logger.getLogger(ItemService.class);

  @Autowired
  protected ItemDOMapper itemDOMapper;
  @Autowired
  protected ItemCommoditySkuDOMapper itemCommoditySkuDOMapper;
  @Autowired
  protected ItemProductDOMapper itemProductDOMapper;
  @Autowired
  protected ItemProductSkuDOMapper itemProductSkuDOMapper;
  @Autowired
  protected ItemSkuRelationDOMapper itemSkuRelationDOMapper;
  @Autowired
  protected ItemSkuRelationScheduleDOMapper itemSkuRelationScheduleDOMapper;
  @Autowired
  protected StdCategoryService stdcategoryservice;
  @Autowired
  private CategoryDOMapper categoryDOMapper;
  @Autowired
  protected SequenceDOMapper sequenceDOMapper;
  @Autowired
  protected ValueService valueService;
  @Autowired
  protected ItemProcessLogDOMapper itemProcessLogDOMapper;
  @Autowired
  private MessageService messageService;

  protected static final int DEFAULT_PIRCE = 999999;

  protected static final String SPLIT_PROPERTY_GROUT_KEY = "\n";

  protected static final String SPLIT_PROPERTY_KEY = ";";

  protected static final String SPLIT_KVPROPERTY_KEY = "&";

  protected static final String TAG_DETAIL_KEY = "tg";// 商品标详情信息，json格式

  // 逗号分隔符
  protected static final String SPLIT_COMMA_KEY = ",";

  /**
   * 检查并更新sku的信息
   * 
   * @param sku
   * @param skuId
   * @param pluId
   */
  public Result<Boolean> checkAddUpdateSkuInfo(ItemCommoditySkuDTO sku, Long skuId, Long itemId) {
    if (null == sku) {
      return Result.getErrDataResult(-1, "input sku object is null");
    }
    if (null == sku.getSkuId() || sku.getSkuId() <= 0) {
      if (null == skuId || skuId <= 0) {
        return Result.getErrDataResult(-1, "input skuId is error");
      }
      sku.setSkuId(skuId);
    }
    if (null == sku.getItemId() || sku.getItemId() <= 0) {
      if (null == itemId || itemId <= 0) {
        return Result.getErrDataResult(-1, "input itemId is error");
      }
      sku.setItemId(itemId);
    }
    if (null == sku.getSku69code() || sku.getSku69code() <= 0) {
      sku.setSku69code(null);// 不能设置0，保持唯一性
    }
    if (null == sku.getSupplierId() || sku.getSupplierId() <= 0) {
      return Result.getErrDataResult(-1, "input supplierId is error");
    }
    if (null == sku.getSkuCode() || sku.getSkuCode() <= 0) {
      if (sku.getSkuId() < 40000) {// 非加工类
        return Result.getErrDataResult(-1, "input buySku is error");
      }
    }
    sku.setSkuPluCode(sku.getSkuId());
    Date now = new Date();
    if (null == sku.getGmtCreate()) {
      sku.setGmtCreate(now);
    }
    if (null == sku.getGmtModified()) {
      sku.setGmtModified(now);
    }
    if (sku.getSkuId() < 40000) {
      sku.setSkuType(SkuTypeConstants.ORIGINAL_GOODS_SKU);
    } else {
      sku.setSkuType(SkuTypeConstants.ORIGINAL_GOODS_SKU);
    }
    if (null == sku.getSaleStatus()) {
      sku.setSaleStatus(SaleStatusConstants.SALE_ONLY_MARKET);
    }
    if (null == sku.getTags()) {
      sku.setTags(0);
    }
    if (null == sku.getQuality() || sku.getQuality() <= 0) {
      sku.setQuality(1);
    }
    sku.setInventory(1);
    return Result.getSuccDataResult(true);
  }

  /**
   * 获取后台类目对象
   * 
   * @param catId
   * @return
   */
  protected CategoryDO getCategoryDO(int catId) {
    CategoryDO catDO = categoryDOMapper.selectCategoryById(catId);
    return catDO;
  }

  /**
   * 获取当前类目id的根类目
   * 
   * @param catId
   * @return
   */
  protected CategoryDO getRootCategoryDO(int catId) {
    CategoryDO catDO = null;
    int currentId = catId;
    while (true) {
      catDO = categoryDOMapper.selectCategoryById(currentId);
      currentId = catDO.getParentId();
      if (catDO.getParentId() == null || catDO.getParentId() <= 0) {
        break;
      }
    }

    return catDO;
  }

  /**
   * 系统产生一个商品货号，统一全部商品使用，按照类目格式产生
   * 
   * @return
   */
  protected Long getItemCodeId(int currentCatId) {
    CategoryDO catDO = this.getCategoryDO(currentCatId);
    if (catDO == null) {
      return -1L;
    }
    stdcategoryservice.queryCategoryById(currentCatId);
    long catCode = catDO.getCatCode();
    long productIdRange = catCode * 10000 + 1;
    Long resultId =
        sequenceDOMapper.getNext(productIdRange, SequenceDOMapper.SEQUENCE_TYPE_PROCUCT);

    if (resultId >= (catCode + 1) * 10000) {
      // 商品货号设计规范超过长度
      return -3l;
    } else if (resultId == 0) {
      // 商品货号sequence出错
      return 0l;
    } else if (resultId >= productIdRange) {
      return resultId;
    } else {
      // 系统错误
      return -1l;
    }
  }

  /**
   * 查询子类目列表
   * 
   * @return
   */
  protected List<CategoryDO> getSubCatByParent(int parentCatId) {
    List<CategoryDO> catList = categoryDOMapper.selectSubcategoryById(parentCatId);
    if (CollectionUtils.isNotEmpty(catList)) {
      return catList;
    } else {
      return Collections.emptyList();
    }
  }

  /**
   * 系统产生一个后台商品id
   * 
   * @return
   */
  protected Long getProductId() {
    Long id =
        sequenceDOMapper.getNext(SequenceDOMapper.SEQUENCE_START_ITEM_PROCUCT,
            SequenceDOMapper.SEQUENCE_TYPE_ITEM_PROCUCT);
    if (id > 0) {
      return id;
    } else {
      // sequence生成id出现错误
      return 0l;
    }
  }

  /**
   * 系统产生一个后台商品skuid
   * 
   * @return
   */
  // protected Long getProductSkuId() {
  // Long id =
  // sequenceDOMapper.getNext(SequenceDOMapper.SEQUENCE_START_SKU_PROCUCT,
  // SequenceDOMapper.SEQUENCE_TYPE_SKU_PROCUCT);
  // if (id > 0) {
  // return id;
  // } else {
  // // sequence生成id出现错误
  // return 0l;
  // }
  // }

  /**
   * 系统产生一个前台商品id
   * 
   * @return
   */
  protected Long getCommodityId() {
    Long id =
        sequenceDOMapper.getNext(SequenceDOMapper.SEQUENCE_START_ITEM_COMMODITY,
            SequenceDOMapper.SEQUENCE_TYPE_ITEM_COMMODITY);
    if (id > 0) {
      return id;
    } else {
      // sequence生成id出现错误
      return 0l;
    }
  }

  /**
   * 系统产生一个前台商品skuid
   * 
   * @return
   */
  // protected Long getCommodityIdSkuId() {
  // Long id =
  // sequenceDOMapper.getNext(SequenceDOMapper.SEQUENCE_START_SKU_COMMODITY,
  // SequenceDOMapper.SEQUENCE_TYPE_SKU_COMMODITY);
  // if (id > 0) {
  // return id;
  // } else {
  // // sequence生成id出现错误
  // return 0l;
  // }
  // }

  /**
   * 系统plu码，针对销售商品有效
   * 
   * @return
   */
  protected Long getSkuPluId(int currentCatId) {
    CategoryDO catDO = this.getRootCategoryDO(currentCatId);
    if (catDO == null) {
      return -1L;
    }
    long rootCatCode = (catDO.getCatCode() * 10000) + 1;
    Long pluId = sequenceDOMapper.getNext(rootCatCode, SequenceDOMapper.SEQUENCE_TYPE_PLU_SKU);
    return pluId;
  }

  /**
   * 根据前台台商品对象产生一个前台商品对象.未设置商品id
   * 
   * @param itemProduct
   * @return
   */
  protected ItemDO getCommdityDOByCommdityDTO(ItemDTO itemDTO, Long itemId, Long itemSkuId,
      boolean insertFlag) {
    ItemDO item = new ItemDO();
    BeanCopierUtils.copyProperties(itemDTO, item);

    String picUrl = "";
    if (CollectionUtils.isNotEmpty(itemDTO.getPicList())) {
      picUrl = PicUtil.joinPicStr(PicUtil.processPicListHeader(itemDTO.getPicList(), false));
    } else {
      if (StringUtils.isNotEmpty(itemDTO.getPicUrl())) {
        picUrl = itemDTO.getPicUrl().replaceFirst(PicConstants.PIC_PREFIX, "");
      }
    }
    item.setPic(picUrl);
    item.setItemId(itemId);
    item.setSubtitle(itemDTO.getSubtitle());
    item.setSku(itemSkuId.toString());
    item.setOwnerId(item.getCreator());
    item.setProductId(itemDTO.getProductId());
    item.setShopId(1);
    item.setIntroduction(processIntroductPicHeader(itemDTO.getIntroduction(), false));
    item.setCheckinfo("");
    CategoryDO cateDO = this.getCategoryDO(item.getCategoryId());
    if (null != cateDO && cateDO.getParentId() != null) {
      item.setCmCat2(cateDO.getParentId());
    }

    Date now = new Date();
    if (insertFlag) {
      item.setGmtCreate(now);
    } else {
      item.setGmtCreate(item.getGmtCreate());
    }
    item.setGmtModified(now);

    return item;
  }

  /**
   * 根据前台台商品对象产生一个前台商品对象.未设置商品id
   * 
   * @param itemProduct
   * @return
   */
  protected ItemDO getCommdityDOByInputCommdityDTO(ItemDTO itemDTO, Long itemId, Long itemSkuId,
      boolean insertFlag) {
    ItemDO item = new ItemDO();
    BeanCopierUtils.copyProperties(itemDTO, item);

    String picUrl = "";
    if (CollectionUtils.isNotEmpty(itemDTO.getPicList())) {
      picUrl = PicUtil.joinPicStr(PicUtil.processPicListHeader(itemDTO.getPicList(), false));
    } else {
      if (StringUtils.isNotEmpty(itemDTO.getPicUrl())) {
        picUrl = itemDTO.getPicUrl().replaceFirst(PicConstants.PIC_PREFIX, "");
      }
    }
    item.setPic(picUrl);
    item.setItemId(itemId);
    item.setSubtitle(itemDTO.getSubtitle());
    item.setSku(itemSkuId.toString());
    item.setOwnerId(item.getCreator());
    item.setProductId(itemDTO.getProductId());
    item.setShopId(1);
    item.setIntroduction(processIntroductPicHeader(itemDTO.getIntroduction(), false));
    item.setCheckinfo("");
    CategoryDO cateDO = this.getCategoryDO(item.getCategoryId());
    if (null != cateDO && cateDO.getParentId() != null) {
      item.setCmCat2(cateDO.getParentId());
    }

    Date now = new Date();
    if (insertFlag) {
      item.setGmtCreate(now);
    } else {
      item.setGmtCreate(item.getGmtCreate());
    }
    item.setGmtModified(now);

    return item;
  }


  /**
   * 非加工类获取进售价关系
   * 
   * @param productSku
   * @return
   */
  protected ItemSkuRelationDO getSkuRelationByPSSku(Long pskuId, ItemCommoditySkuDO commoditySku,
      Short relationType, ItemSkuRelationDO skuRelation, Integer bprice, Integer price) {
    if (null == skuRelation) {
      skuRelation = new ItemSkuRelationDO();
    }

    skuRelation.setPskuId(pskuId);
    skuRelation.setCskuId(commoditySku.getSkuId());
    skuRelation.setItemCode(commoditySku.getSkuCode());
    skuRelation.setPcProportion(new BigDecimal(commoditySku.getSkuCount()).multiply(
        new BigDecimal(1000)).intValue());
    skuRelation.setRelationType(relationType);
    if (null != bprice && bprice > 0) {
      skuRelation.setbPrice(bprice);
    } else {
      skuRelation.setbPrice(DEFAULT_PIRCE);
    }
    if (null != price && price > 0) {
      skuRelation.setPrice(price);
    } else {
      skuRelation.setPrice(DEFAULT_PIRCE);
    }

    skuRelation.setBoxCost(new BigDecimal(skuRelation.getbPrice()).multiply(
        new BigDecimal(commoditySku.getSkuCount())).intValue());
    skuRelation.setSaleEnd(DateUtils.addYears(skuRelation.getSaleStart(), 1));// 默认销售截止时间明年
    skuRelation.setBuyEnd(DateUtils.addYears(skuRelation.getBuyStart(), 1));
    skuRelation.setSaleStart(new Date());
    skuRelation.setBuyStart(new Date());
    return skuRelation;
  }

  /**
   * 加工类获取进售价关系
   * 
   * @param productSku
   * @return
   */
  protected ItemSkuRelationDO getSkuRelationByBoomSku(ItemCommoditySkuDO commoditySku,
      Short relationType, ItemSkuRelationDO skuRelation) {
    if (null == skuRelation) {
      skuRelation = new ItemSkuRelationDO();
    }
    skuRelation.setPskuId(0L);
    skuRelation.setCskuId(commoditySku.getSkuId());
    skuRelation.setItemCode(commoditySku.getSkuCode());
    skuRelation.setPcProportion(1000);// 默认1比1，直接扩大1000倍存储
    skuRelation.setRelationType(relationType);

    Map<String, String> property = commoditySku.queryUnStandardCatPropertyValueMap();
    String bPrice = property.get("22");// 进价
    if (StringUtils.isNotEmpty(bPrice)) {
      skuRelation.setbPrice(new BigDecimal(bPrice).intValue());
    }
    if (relationType.equals(RelationTypeConstants.RELATION_TYPE_DISCOUNT)) {
      skuRelation.setPrice(commoditySku.getDiscountUnitPrice());
    } else {
      skuRelation.setPrice(commoditySku.getUnitPrice());
    }
    skuRelation.setSaleEnd(DateUtils.addYears(skuRelation.getSaleStart(), 1));// 默认销售截止时间明年
    skuRelation.setBuyEnd(DateUtils.addYears(skuRelation.getBuyStart(), 1));
    skuRelation.setSaleStart(new Date());
    skuRelation.setBuyStart(new Date());
    skuRelation.setBoxCost(skuRelation.getbPrice());
    skuRelation.setAvgCost(skuRelation.getbPrice());

    return skuRelation;
  }

  /**
   * 获取格式化后的商品详情
   * 
   * @param inputIntroduction
   * @return
   */
  protected List<IntroductDataDTO> getIntroductDataDTOList(String inputIntroduction, boolean isAdd) {
    if (StringUtils.isEmpty(inputIntroduction)) {
      return Collections.emptyList();
    }
    List<IntroductDataDTO> introList =
        (List<IntroductDataDTO>) JackSonUtil.jsonToList(inputIntroduction, IntroductDataDTO.class);
    if (CollectionUtils.isEmpty(introList)) {
      return Collections.emptyList();
    }
    for (IntroductDataDTO data : introList) {
      if (data.getType() == IntroductDataDTO.PIC_FLAG) {
        String source = data.getSource();
        if (!source.startsWith(PicConstants.PIC_PREFIX) && isAdd) {
          data.setSource(PicConstants.PIC_PREFIX + source);
        } else if (source.startsWith(PicConstants.PIC_PREFIX) && !isAdd) {
          data.setSource(source.replaceFirst(PicConstants.PIC_PREFIX, ""));
        }
      }
    }
    return introList;
  }

  /**
   * 处理图片前缀
   * 
   * @param inputIntroduction
   * @param isAdd 增加前缀或者删除前缀
   * @return
   */
  protected String processIntroductPicHeader(List<IntroductDataDTO> introList, boolean isAdd) {
    if (CollectionUtils.isEmpty(introList)) {
      return "";
    }
    for (IntroductDataDTO data : introList) {
      if (data.getType() == IntroductDataDTO.PIC_FLAG) {
        String source = data.getSource();
        if (!source.startsWith(PicConstants.PIC_PREFIX) && isAdd) {
          data.setSource(PicConstants.PIC_PREFIX + source);
        } else if (source.startsWith(PicConstants.PIC_PREFIX) && !isAdd) {
          data.setSource(source.replaceFirst(PicConstants.PIC_PREFIX, ""));
        }
      }
    }
    return JackSonUtil.getJson(introList);
  }

  /**
   * 处理图片前缀
   * 
   * @param inputIntroduction
   * @param isAdd 增加前缀或者删除前缀
   * @return
   */
  protected String processIntroductPicHeader(String inputIntroduction, boolean isAdd) {
    if (StringUtils.isEmpty(inputIntroduction)) {
      return "";
    }
    List<IntroductDataDTO> introList =
        (List<IntroductDataDTO>) JackSonUtil.jsonToList(inputIntroduction, IntroductDataDTO.class);
    if (CollectionUtils.isEmpty(introList)) {
      return inputIntroduction;
    }
    boolean isUpdate = false;
    for (IntroductDataDTO data : introList) {
      if (data.getType() == IntroductDataDTO.PIC_FLAG) {
        String source = data.getSource();
        if (!source.startsWith(PicConstants.PIC_PREFIX) && isAdd) {
          data.setSource(PicConstants.PIC_PREFIX + source);
          isUpdate = true;
        } else if (source.startsWith(PicConstants.PIC_PREFIX) && !isAdd) {
          data.setSource(source.replaceFirst(PicConstants.PIC_PREFIX, ""));
          isUpdate = true;
        }
      }
    }
    if (isUpdate) {
      return JackSonUtil.getJson(introList);
    } else {
      return inputIntroduction;
    }
  }

  protected void logSkuUpPrice(ItemSkuDTO sku, ItemSkuDTO dbSku, Boolean isDiscount, Long userId,
      boolean isProductSku) {
    if (null == sku) {
      return;
    }

    if (null == dbSku) {
      int type = 0;
      if (isProductSku) {
        type = ItemProcessLogDO.ITEM_UPDATE_TYPE_PRODUCT_PRICE;
      } else {
        type = ItemProcessLogDO.ITEM_UPDATE_TYPE_COMMODITY_PRICE;
      }
      if (userId == null) {
        userId = 5L;
      }
      if (1 != itemProcessLogDOMapper.insert(getItemProcessLogDO(sku.getSkuId(), userId,
          String.valueOf(-1), String.valueOf(sku.getUnitPrice()), type))) {
        logger.error("[Item_update_price],update price " + sku.getSkuId() + " from " + -1 + " to "
            + sku.getPrice());
      }
      return;
    }

    Integer newPrice = sku.getDiscountUnitPrice();
    if (null == newPrice) {
      newPrice = sku.getUnitPrice();
    }
    Integer oldPrice = dbSku.getDiscountUnitPrice();
    if (null == oldPrice) {
      oldPrice = dbSku.getUnitPrice();
    }
    if (null != newPrice && null != oldPrice && !(oldPrice.equals(newPrice))) {
      if (null == userId) {
        userId = 0L;
      }
      int type = ItemProcessLogDO.ITEM_UPDATE_TYPE_COMMODITY_PRICE;
      if (isProductSku) {
        type = ItemProcessLogDO.ITEM_UPDATE_TYPE_PRODUCT_PRICE;
      }
      if (!oldPrice.equals(newPrice)) {
        if (1 != itemProcessLogDOMapper.insert(getItemProcessLogDO(sku.getSkuId(), userId,
            String.valueOf(oldPrice), String.valueOf(newPrice), type))) {
          logger.error("[Item_update_price],update discount_price " + sku.getSkuId() + " from "
              + oldPrice + " to " + newPrice);
        } else {
          boolean is69Code = (sku.getSku69code() != null && sku.getSku69code() > 0);
          sendPriceChangeMsg(sku.getSkuId(), userId, sku.getTitle(), oldPrice, newPrice, type,
              is69Code);
        }
      }
    }
  }

  protected Result<Boolean> updateSkuRelation(ItemSkuRelationDTO skuRelation, Long userId) {
    if (null == skuRelation) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }
    ItemSkuRelationDO dbRelation = itemSkuRelationDOMapper.query(skuRelation.getId());
    if (null == dbRelation) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "query skuRelation after update get return null, db failed");
    }

    ItemSkuRelationDO itemSkuRelationDO = null;
    if(skuRelation instanceof ItemSkuRelationDO){
      itemSkuRelationDO = (ItemSkuRelationDO)skuRelation;
    }else{
      itemSkuRelationDO = new ItemSkuRelationDO();
      BeanCopierUtils.copyProperties(skuRelation, itemSkuRelationDO);
    }
    
    int updateRelationCount = itemSkuRelationDOMapper.update(itemSkuRelationDO);
    if (updateRelationCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert db failed");
    }

    boolean isDiscount = false;
    if (null != skuRelation.getRelationType() && 1 == skuRelation.getRelationType().intValue()) {
      isDiscount = true;
    }

    Integer newBuyPrice = skuRelation.getbPrice();
    Integer oldBuyPrice = dbRelation.getbPrice();
    Integer newPrice = skuRelation.getPrice();
    Integer oldPrice = dbRelation.getPrice();

    // boolean updateBuyPriceFlag = checkPriceAndUpdate(oldBuyPrice, newBuyPrice);
    // boolean updatePriceFlag = checkPriceAndUpdate(oldPrice, newPrice);

    if (null == userId) {
      userId = 0l;
    }

    Long skuId = skuRelation.getPskuId();
    if (null != skuId && skuId > 0) {
      List<ItemProductSkuDO> productSkuDOList =
          itemProductSkuDOMapper.query(SkuQuery.querySkuById(skuId));
      if (CollectionUtils.isNotEmpty(productSkuDOList)) {
        ItemProductSkuDO dbSku = productSkuDOList.get(0);
        if (isDiscount) {
          dbSku.setDiscountPrice(newBuyPrice);
          dbSku.setDiscountUnitPrice(newBuyPrice);
        } else {
          dbSku.setPrice(newBuyPrice);
          dbSku.setUnitPrice(newBuyPrice);
        }
        int updateCount = itemProductSkuDOMapper.update(dbSku);
        if (updateCount != 1) {
          logger.error("[Error update price] pskuId=" + skuId + ",price from " + oldBuyPrice
              + " to " + newBuyPrice + ",userId:" + userId);
          return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
              "update product sku return falied, db failed");
        } else {
          int type = ItemProcessLogDO.ITEM_UPDATE_TYPE_PRODUCT_PRICE;
          if (isDiscount) {
            type = ItemProcessLogDO.ITEM_UPDATE_TYPE_PRODUCT_DISCOUNT_PRICE;
          }
          logger.error("[Item_update_price],update productprice " + skuId + " from " + oldBuyPrice
              + " to " + newBuyPrice + ",userId:" + userId);

          if (1 != itemProcessLogDOMapper.insert(getItemProcessLogDO(skuId, userId,
              String.valueOf(oldBuyPrice), String.valueOf(newBuyPrice), type))) {
            logger.error("[Item_update_price],update productprice " + skuId + " from "
                + oldBuyPrice + " to " + newBuyPrice + ",userId:" + userId);
          } else {
            // boolean is69Code = (dbSku.getSku69code() != null && dbSku.getSku69code() > 0);
            // sendPriceChangeMsg(dbSku.getSkuId(),userId,dbSku.getTitle(),oldPrice,newPrice,type,is69Code);
          }
        }
      }
    } else {
      // 加工类商品没有采购商品，只记录进价更新记录
      skuId = skuRelation.getCskuId();
      int type = ItemProcessLogDO.ITEM_UPDATE_TYPE_PRODUCT_PRICE;
      if (isDiscount) {
        type = ItemProcessLogDO.ITEM_UPDATE_TYPE_PRODUCT_DISCOUNT_PRICE;
      }
      skuId = skuRelation.getCskuId();
      if (null != skuId && skuId > 0) {
        List<ItemCommoditySkuDO> commoditySkuDOList =
            itemCommoditySkuDOMapper.query(SkuQuery.querySkuById(skuId),null);
        if (CollectionUtils.isNotEmpty(commoditySkuDOList)) {
          ItemCommoditySkuDO dbSku = commoditySkuDOList.get(0);
          if (1 != itemProcessLogDOMapper.insert(getItemProcessLogDO(skuId, userId,
              String.valueOf(oldBuyPrice), String.valueOf(newBuyPrice), type))) {
            logger.error("[Item_update_price],update productprice " + skuId + " from "
                + oldBuyPrice + " to " + newBuyPrice + ",userId:" + userId);
          } else {
            // sendPriceChangeMsg(skuId,userId,dbSku.getTitle(),oldPrice,newPrice,type,false);
          }
        }
      }
    }

    skuId = skuRelation.getCskuId();
    if (null != skuId && skuId > 0) {
      List<ItemCommoditySkuDO> commoditySkuDOList =
          itemCommoditySkuDOMapper.query(SkuQuery.querySkuById(skuId),null);
      if (CollectionUtils.isNotEmpty(commoditySkuDOList)) {
        ItemCommoditySkuDO dbSku = commoditySkuDOList.get(0);
        Integer commodityOldPrice = null;
        if (isDiscount) {
          commodityOldPrice = dbSku.getDiscountUnitPrice();
          dbSku.setDiscountPrice(newPrice);
          dbSku.setDiscountUnitPrice(newPrice);
        } else {
          commodityOldPrice = dbSku.getUnitPrice();
          dbSku.setPrice(newPrice);
          dbSku.setUnitPrice(newPrice);
        }

        if (isSteelyardSku(dbSku)) {
          int saleRangeNum = getSaleRangeNum(dbSku);
          if (saleRangeNum > 0) {
            if (isDiscount) {
              dbSku.setDiscountPrice(new BigDecimal(saleRangeNum)
                  .multiply(new BigDecimal(newPrice)).divide(new BigDecimal(1000)).intValue());
            } else {
              dbSku.setPrice(new BigDecimal(saleRangeNum).multiply(new BigDecimal(newPrice))
                  .divide(new BigDecimal(1000)).intValue());
            }
          } else {// 默认1斤
            if (isDiscount) {
              dbSku.setDiscountPrice(new BigDecimal(newPrice).divide(new BigDecimal(2)).intValue());
            } else {
              dbSku.setPrice(new BigDecimal(newPrice).divide(new BigDecimal(2)).intValue());
            }
          }
        }

        int updateCount = itemCommoditySkuDOMapper.update(dbSku);
        if (updateCount != 1) {
          logger.error("[Error update price] cskuId=" + skuId + ",price from " + oldPrice + " to "
              + newPrice);
          return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
              "update commodity sku return falied, db failed");
        } else {
          int type = ItemProcessLogDO.ITEM_UPDATE_TYPE_COMMODITY_PRICE;
          if (isDiscount) {
            type = ItemProcessLogDO.ITEM_UPDATE_TYPE_COMMODITY_DISCOUNT_PRICE;
          }
          logger.error("[Item_update_price],update productprice " + skuId + " from " + oldPrice
              + " to " + newPrice + ",userId:" + userId);
          if (1 != itemProcessLogDOMapper.insert(getItemProcessLogDO(skuId, userId,
              String.valueOf(oldPrice), String.valueOf(newPrice), type))) {
            logger.error("[Item_update_price],update productprice " + skuId + " from " + oldPrice
                + " to " + newPrice + ",userId:" + userId);
          } else {
            if (!commodityOldPrice.equals(newPrice)) {// 价格变动全部覆盖，但可以不发短信
              boolean is69Code = (dbSku.getSku69code() != null && dbSku.getSku69code() > 0);
              sendPriceChangeMsg(dbSku.getSkuId(), userId, dbSku.getTitle(), oldPrice, newPrice,
                  type, is69Code);
            }
          }
        }

        ItemDO dbItem = itemDOMapper.queryItem(dbSku.getItemId());
        dbItem.setPrice(dbSku.getPrice());
        dbItem.setDiscountPrice(dbSku.getDiscountPrice());
        if (itemDOMapper.updateItem(dbItem) != 1) {
          logger.error("[Error update price] itemId=" + dbSku.getItemId() + ",price from "
              + oldPrice + " to " + newPrice);
          return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
              "update commodity item price return falied, db failed");
        }
      }
    }

    return Result.getSuccDataResult(true);
  }

  /**
   * 获取日志对象
   * 
   * @param skuId
   * @param userId
   * @param from
   * @param to
   * @param type
   * @return
   */
  protected ItemProcessLogDO getItemProcessLogDO(long skuId, long userId, String from, String to,
      int type) {
    ItemProcessLogDO log = new ItemProcessLogDO();
    Date now = new Date();
    log.setOldInfo(from);
    log.setNewInfo(to);
    log.setLogType(type);
    log.setUserId(userId);
    log.setGmtCreate(now);
    log.setGmtModified(now);
    log.setSkuId(skuId);
    return log;
  }

  private boolean checkPriceAndUpdate(Integer oldPrice, Integer newPrice) {
    if (null == newPrice && null == oldPrice) {
      newPrice = DEFAULT_PIRCE;
      return true;
    } else if (null != newPrice && null != oldPrice) {
      if (!(newPrice.equals(oldPrice))) {
        return true;
      }
    } else {
      if (null == newPrice) {
        newPrice = DEFAULT_PIRCE;
        return true;
      }
    }
    return false;
  }

  /**
   * 是否称重类型商品
   * 
   * @return
   */
  protected boolean isSteelyardSku(ItemCommoditySkuDTO sku) {
    if (null != sku && sku.getSkuUnit() != null && sku.getSkuUnit() == 3) {
      return true;
    }
    return false;
  }

  /**
   * 获取最小起定量字段值
   * 
   * @return
   */
  protected int getSaleRangeNum(ItemCommoditySkuDTO sku) {
    if (null != sku && StringUtils.isNotEmpty(sku.getProperty())) {
      Map<String, String> prpM = getPropertyMap(sku.getProperty());
      String saleRangeNumStr = prpM.get("26");
      if (StringUtils.isNotEmpty(saleRangeNumStr) && StringUtils.isNumeric(saleRangeNumStr)) {
        return Integer.valueOf(saleRangeNumStr);
      }
    }
    return 0;
  }

  /**
   * 取属性字段kv对
   * 
   * @return
   */
  protected Map<String, String> getPropertyMap(String property) {
    Map<String, String> tmpProM = Maps.newHashMap();
    if (StringUtils.isNotEmpty(property)) {
      String propertyArrays[] = property.split(SPLIT_PROPERTY_GROUT_KEY);
      for (String tmp : propertyArrays) {
        if (StringUtils.isNotEmpty(tmp)) {
          String keyValue[] = tmp.split("=");
          if (keyValue.length != 2) {
            continue;
          } else {
            tmpProM.put(keyValue[0], keyValue[1]);
          }
        }
      }
    }
    Map<String, String> proM = Maps.newHashMap();
    if (CollectionUtils.isNotEmpty(tmpProM.keySet())) {
      for (String key : tmpProM.keySet()) {
        String tmpProperty = tmpProM.get(key);
        if (StringUtils.isEmpty(tmpProperty)) {
          continue;
        }
        String tmpPropertyArray[] = tmpProperty.split(SPLIT_PROPERTY_KEY);
        for (String kv : tmpPropertyArray) {
          String tmpkvArray[] = kv.split(SPLIT_KVPROPERTY_KEY);
          if (tmpkvArray.length != 2) {
            continue;
          }
          proM.put(tmpkvArray[0], tmpkvArray[1]);
        }
      }
    }
    return proM;
  }

  /**
   * 取属性字段kv对
   * 
   * @return
   */
  protected Map<String, String> queryFeatureMap(String feature) {
    Map<String, String> featureKVMap = Maps.newHashMap();
    if (StringUtils.isNotEmpty(feature)) {
      String featureArrays[] = feature.split(SPLIT_PROPERTY_GROUT_KEY);
      for (String tmp : featureArrays) {
        if (StringUtils.isNotEmpty(tmp)) {
          String keyValue[] = tmp.split(SPLIT_KVPROPERTY_KEY);
          if (keyValue.length != 2) {
            continue;
          } else {
            featureKVMap.put(keyValue[0], keyValue[1]);
          }
        }
      }
    }
    return featureKVMap;
  }

  /**
   * 价格变动时给相关人员发送短信.模板格式 鲜在时运维 商品plu:${code},title:${title}发生${tag}变动，原始值${oldprice},更新值${newpirce}
   * 
   * @param skuId skuId,包含采购商品和销售商品
   * @param processor 操作人
   * @param title sku名称
   * @param oldPrice 变动前价格
   * @param newPrice 变动后价格
   * @param type 变动类型 参考 ItemProcessLogDO定义
   * @param skuInclude69Code 是否有69码商品
   */
  protected void sendPriceChangeMsg(long skuId, long processor, String title, Integer oldPrice,
      Integer newPrice, int type, boolean skuInclude69Code) {
    Map<String, String> para = Maps.newHashMap();
    String code = String.valueOf(skuId);
    if (!skuInclude69Code) {
      code = code + "_无69码";
    } else {
      code = code + "_有69码";
    }
    para.put("code", code);
    para.put("title", title);
    para.put("oldprice", String.valueOf(oldPrice));
    para.put("newpirce", String.valueOf(newPrice));

    if(SpringContextUtil.isOnline()){
      
    }
    // 采购号码，展苍
    List<Long> purchasePhoneList = Arrays.asList(13486128568L,18236706216L);
    //店内号码，展苍
    List<Long> shopPhoneList = Arrays.asList(13486128568L,18236706216L);
    //加工档号码，展苍
    List<Long> productPhoneList = Arrays.asList(13486128568L,18236706216L);
    
    if(SpringContextUtil.isOnline()){
      // 采购号码，蒲耶、小柴
      purchasePhoneList = Arrays.asList(13916597912L);
      //店内号码，强森，木槿，喧哗
      shopPhoneList = Arrays.asList(18663778477L,13486128568L,13381890891L,13661477513L);
      //加工档号码，小米，展苍
      productPhoneList = Arrays.asList(13761626935L);
    }
    if (type == 1 || type == 2) {// 成本变化
    // para.put("tag", "采购成本");
    // if(skuId < 40000 || skuId > 50000){
    // for(long send:purchasePhoneList){
    // sendMsg(1, send, para);
    // }
    // }else{
    // for(long send:productPhoneList){
    // sendMsg(1, send, para);
    // }
    // }
    } else if (type == 3) {// 原价
      para.put("tag", "销售原价");
    } else if (type == 4) {// 优惠价
      para.put("tag", "销售优惠价");
      if (!skuInclude69Code) {// 没有69码
        if ((skuId < 40000 || skuId > 50000)) {
          for (long send : purchasePhoneList) {// 采购收消息
            sendMsg(1, send, para);
          }
        } else {
          for (long send : productPhoneList) {// 加工档口收消息
            sendMsg(1, send, para);
          }
        }
        for (long send : shopPhoneList) {// 店内收消息
          sendMsg(1, send, para);
        }
      }
    }
  }

  /**
   * @param sendType 发送业务逻辑，1：发送变价短信通知 调用发短信接口
   */
  protected void sendMsg(int sendType, long phone, Map<String, String> para) {
    String template = "";
    if (sendType == 1) {
      template = "SMS_35760091";
    }
    RpcResult<Boolean> result =
        messageService.sendSmsMessage(String.valueOf(phone), template, para);
    if (null == result || !result.isSuccess()) {
      logger.error("Item center send msg failed," + JackSonUtil.getJson(result));
      return;
    } else {
      logger.error("Item center send msg success,msg" + JackSonUtil.getJson(para) + ",phone:"
          + phone);
    }
  }


}
