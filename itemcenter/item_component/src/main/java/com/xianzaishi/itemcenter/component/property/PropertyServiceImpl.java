package com.xianzaishi.itemcenter.component.property;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.property.PropertyService;
import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;
import com.xianzaishi.itemcenter.client.value.dto.ValueDTO;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.dal.property.dao.BasePropertyDOMapper;
import com.xianzaishi.itemcenter.dal.property.dao.PropertyDOMapper;
import com.xianzaishi.itemcenter.dal.property.dataobject.BasePropertyDO;
import com.xianzaishi.itemcenter.dal.property.dataobject.PropertyDO;
import com.xianzaishi.itemcenter.dal.value.dao.PropertyValueDOMapper;
import com.xianzaishi.itemcenter.dal.value.dao.ValueDOMapper;
import com.xianzaishi.itemcenter.dal.value.dataobject.ValueDO;

/**
 * 属性接口实现类
 * 
 * @author dongpo
 * 
 */
@Service("propertyservice")
public class PropertyServiceImpl implements PropertyService {

  private static Logger logger = Logger.getLogger(PropertyServiceImpl.class);

  @Autowired
  private BasePropertyDOMapper basePropertyDOMapper;

  @Autowired
  private PropertyDOMapper propertyDOMapper;

  @Autowired
  private ValueDOMapper valueDOMapper;
  
  /**
   * 缓存一份标准属性的id与名称对应关系
   */
  private Map<Integer,String> basePropertyName = Maps.newHashMap();

  public Integer insertBaseProperty(BasePropertyDO propertyDO) {

    Integer insertNumber = basePropertyDOMapper.insertBaseProperty(propertyDO);// 在持久层增加数据，返回增加数目
    Integer propertyId = insertNumber > 0 ? propertyDO.getPropertyId() : 0; // 获取属性id

    return propertyId;
  }


  public Boolean updateBaseProperty(BasePropertyDO propertyDO) {

    Integer updateNumber = basePropertyDOMapper.updateBaseProperty(propertyDO);// 更新持久层数据
    Boolean isUpdate = updateNumber > 0 ? true : false; // 返回更新是否成功
    return isUpdate;
  }



  @Override
  public Result<List<PropertyDTO>> queryPropertyByCatId(Integer catId, Boolean includeValue) {
    List<PropertyDTO> propertyDtos = new ArrayList<PropertyDTO>();// 定义类目属性对象list
    List<PropertyDO> propertyDOs = propertyDOMapper.selectPropertyByCatId(catId);// 获取持久层类目属性数据
    BeanCopierUtils.copyListBean(propertyDOs, propertyDtos,PropertyDTO.class);// do层数据复制到dto层数据
    if(null != includeValue && includeValue){
      for(PropertyDTO property:propertyDtos){
        List<ValueDO> valueList = valueDOMapper.selectValueByPropertyId(catId, property.getPropertyId());
        List<ValueDTO> tValueList = Lists.newArrayList();
        BeanCopierUtils.copyListBean(valueList, tValueList, ValueDTO.class);
        if(CollectionUtils.isNotEmpty(tValueList)){
          property.setCategoryPropertyValues(tValueList);
        }
      }
    }
    
    return Result.getSuccDataResult(propertyDtos);// 返回类目属性list
  }
  

  @Override
  public Result<String> queryPropertyNameById(Integer propertyId) {
    String name = basePropertyName.get(propertyId);
    if(StringUtils.isEmpty(name)){
      BasePropertyDO propertyDO = basePropertyDOMapper.selectBasePropertyById(propertyId);
      if(null != propertyDO){
        String dbName = propertyDO.getPropertyName();
        this.basePropertyName.put(propertyId, dbName);
        return Result.getSuccDataResult(dbName);
      }
    }
    
    return Result.getSuccDataResult(name);
  }

  @Override
  public Result<PropertyDTO> queryPropertyByPropertyId(Integer catId, Integer propertyId,
      Boolean hasPropertyValue) {
    PropertyDTO propertyDto = new PropertyDTO();
    PropertyDO propertyDO = propertyDOMapper.selectPropertyByPropertyId(catId, propertyId);
    if (null == propertyDO) {
      return Result.getSuccDataResult(null);
    }
    BeanCopierUtils.copyProperties(propertyDO, propertyDto);// 将从持久层获取的数据复制到dto层的数据类型
    if (hasPropertyValue) {// 是否包含当前属性下所有属性值
      List<ValueDTO> valueDtos = new ArrayList<>();
      BeanCopierUtils.copyProperties(valueDOMapper.selectValueByPropertyId(catId, propertyId),
          valueDtos);
      propertyDto.setCategoryPropertyValues(valueDtos);// 返回当前属性下的所有属性值
    }
    return Result.getSuccDataResult(propertyDto);
  }

  @Override
  public Result<PropertyDTO> queryPropertyByPropertyName(Integer catId, String name,
      Boolean hasValue) {
    // TODO Auto-generated method stub
    PropertyDTO propertyDto = new PropertyDTO();
    PropertyDO propertyDO = propertyDOMapper.selectPropertyByPropertyName(catId, name);
    if (null == propertyDO) {
      return Result.getSuccDataResult(null);
    }
    BeanCopierUtils.copyProperties(propertyDO, propertyDto);// 将从持久层获取的数据复制到dto层的数据类型
    if (hasValue) {// 是否包含当前属性下所有属性值
      List<ValueDTO> valueDtos = new ArrayList<>();
      BeanCopierUtils.copyProperties(
          valueDOMapper.selectValueByPropertyId(catId, propertyDto.getPropertyId()), valueDtos);
      propertyDto.setCategoryPropertyValues(valueDtos);// 返回当前属性下的所有属性值
    }
    return Result.getSuccDataResult(propertyDto);
  }


  @Override
  public Result<List<PropertyDTO>> queryPropertyByTags(Integer catId, Integer tags) {
    List<PropertyDTO> propertyDtos = new ArrayList<>();
    BeanCopierUtils
        .copyProperties(propertyDOMapper.selectPropertyByTags(catId, tags), propertyDtos);
    return Result.getSuccDataResult(propertyDtos);
  }



  public Integer insertProperty(PropertyDO propertyDO) {

    Integer insertNumber = propertyDOMapper.insertProperty(propertyDO);// 插入数据到持久层
    Integer propertyId = insertNumber > 0 ? propertyDO.getPropertyId() : 0;// 返回属性id，如果插入失败，则返回0
    return propertyId;
  }


  public Boolean updateProperty(PropertyDO propertyDO) {
    // TODO Auto-generated method stub
    Integer updateNumber = propertyDOMapper.updateProperty(propertyDO);// 更新持久层数据
    Boolean isUpdate = updateNumber > 0 ? true : false;// 返回是否更新成功
    return isUpdate;
  }


  @Override
  public Result<Integer> insertProperty(PropertyDTO propertyDTO) {
    BasePropertyDO basePropertyDO =
        basePropertyDOMapper.selectBasePropertyByName(propertyDTO.getPropertyNameAlias());
    Integer propertyId = null;
    if(null != basePropertyDO){
      propertyId = basePropertyDO.getPropertyId();
    }else{
      BasePropertyDO basePropertyDO2 = new BasePropertyDO();
      BeanCopierUtils.copyProperties(propertyDTO, basePropertyDO2);// 获取基本属性值对象
      basePropertyDO2.setPropertyName(propertyDTO.getPropertyNameAlias());
      propertyId = insertBaseProperty(basePropertyDO2);// 插入基本属性值对象，并获取自增的值id
    }
    PropertyDO propertyDO = new PropertyDO();
    BeanCopierUtils.copyProperties(propertyDTO, propertyDO);// dto到do数据类型转换，获取类目属性对象
    if (propertyId != null && propertyId != 0) {
      propertyDO.setPropertyId(propertyId);
      try{
        
        propertyId = insertProperty(propertyDO);
      }catch(Exception e){
        
      }
      logger.info(propertyId);
    }
    return Result.getSuccDataResult(propertyId);
  }


  @Override
  public Result<Boolean> updateProperty(PropertyDTO propertyDTO) {
    PropertyDO propertyDO = new PropertyDO();
    BasePropertyDO basePropertyDO = new BasePropertyDO();
    BeanCopierUtils.copyProperties(propertyDTO, propertyDO);// dto到do数据类型转换，获取类目属性对象
    BeanCopierUtils.copyProperties(propertyDTO, basePropertyDO);// 获取基础属性对象
    Boolean isUpdate = updateBaseProperty(basePropertyDO);
    if (isUpdate) {
      isUpdate = updateProperty(propertyDO);
      logger.info(isUpdate);
    }
    return Result.getSuccDataResult(isUpdate);
  }


}
