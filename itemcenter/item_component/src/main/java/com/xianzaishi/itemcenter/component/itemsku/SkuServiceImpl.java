package com.xianzaishi.itemcenter.component.itemsku;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.dto.ItemBaseDTO.SaleStatusConstants;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.constants.SkuConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationScheduleDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO.SkuTagDefinitionConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuWeightInfoDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.SkuTagDetail;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationAvaliableScheduleQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationScheduleQuery;
import com.xianzaishi.itemcenter.client.property.PropertyService;
import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.itemcenter.component.item.ItemService;
import com.xianzaishi.itemcenter.dal.item.dataobject.ItemDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemCommoditySkuDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemProductSkuDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuRelationDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuRelationScheduleDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuTagsDO;
import com.xianzaishi.itemcenter.dal.property.dao.BasePropertyDOMapper;
import com.xianzaishi.itemcenter.dal.value.dao.BaseValueDOMapper;

/**
 * sku对外服务，包括后台sku对外服务和前台sku对外服务，及sku关联关系
 * 
 * @author zhancang
 */
@Service("skuService")
public class SkuServiceImpl extends ItemService implements SkuService {

  private static final Logger logger = Logger.getLogger(SkuServiceImpl.class);

  @Autowired
  private BasePropertyDOMapper basePropertyDOMapper;
  @Autowired
  private BaseValueDOMapper baseValueDOMapper;
  @Autowired
  private PropertyService propertyservice;

  private static final int SKU69CODEPROPERTYKEY = 5;// 69码对应的属性key

  @Override
  public Result<Long> insertProductSku(ItemProductSkuDTO productSku, Integer catId) {
    if (null == productSku || null == catId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input itemProduct or catId parameter is null");
    }

    if (null == productSku.getSku69code() && null == productSku.getSkuPluCode()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input skuPluCode and sku69code parameter is null");
    }

    ItemProductSkuDO itemProductSku = new ItemProductSkuDO();
    BeanCopierUtils.copyProperties(productSku, itemProductSku);
    int insertCount = itemProductSkuDOMapper.insert(itemProductSku);
    if (insertCount == 1) {
      return Result.getSuccDataResult(itemProductSku.getSkuId());
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert db failed");
    }
  }

  @Override
  public Result<Boolean> updateProductSku(ItemProductSkuDTO productSku) {
    ItemProductSkuDO itemProductSku = new ItemProductSkuDO();
    BeanCopierUtils.copyProperties(productSku, itemProductSku);
    // TODO sku关联商品变动时，修改商品表sku属性
    int updateCount = itemProductSkuDOMapper.update(itemProductSku);
    if (updateCount == 1) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update db failed");
    }
  }

  @Override
  public Result<List<ItemProductSkuDTO>> queryProductSku(SkuQuery query) {
    return Result.getSuccDataResult(queryProductSkuList(query));
  }

  @Override
  public Result<Integer> queryProductSkuCount(SkuQuery query) {
    Integer count = itemProductSkuDOMapper.queryCount(query);
    return Result.getSuccDataResult(count);
  }

  /**
   * 实现具体查询一个后台sku
   * 
   * @param query
   * @return
   */
  private List<ItemProductSkuDTO> queryProductSkuList(SkuQuery query) {
    List<ItemProductSkuDO> doList = itemProductSkuDOMapper.query(query);
    if (CollectionUtils.isEmpty(doList)) {
      return Collections.<ItemProductSkuDTO>emptyList();
    }
    List<ItemProductSkuDTO> resultList = new ArrayList<ItemProductSkuDTO>();
    BeanCopierUtils.copyListBean(doList, resultList, ItemProductSkuDTO.class);
    return resultList;
  }

  @Override
  public Result<Long> insertItemSku(ItemCommoditySkuDTO itemSku) {
    if (null == itemSku) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input itemSku parameter is null");
    }

    if (null == itemSku.getSkuCode()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input itemSkuCode parameter is null");
    }

    if (null == itemSku.getSku69code() && null == itemSku.getSkuPluCode()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input skuPluCode and sku69code parameter is null");
    }

    ItemCommoditySkuDO itemitemProductSku = new ItemCommoditySkuDO();
    BeanCopierUtils.copyProperties(itemSku, itemitemProductSku);
    int insertCount = itemCommoditySkuDOMapper.insert(itemitemProductSku);
    if (insertCount == 1) {
      return Result.getSuccDataResult(itemSku.getSkuId());
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert db failed");
    }
  }

  @Override
  public Result<Boolean> updateItemSku(ItemCommoditySkuDTO itemSku) {
    ItemCommoditySkuDO itemitemProductSku = new ItemCommoditySkuDO();
    BeanCopierUtils.copyProperties(itemSku, itemitemProductSku);
    int insertCount = itemCommoditySkuDOMapper.update(itemitemProductSku);
    if (insertCount == 1) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert db failed");
    }
  }

  /**
   * 临时方法，更新69码
   * 
   * @param itemSku
   * @return
   */
  public Result<Boolean> updateSku69Code(long cskuId, long sku69Code) {
    if (cskuId <= 0 || sku69Code <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input parameter is error");
    }
    String errorInfo = "";
    List<ItemCommoditySkuDO> skuDOList =
        itemCommoditySkuDOMapper.query(SkuQuery.querySkuById(cskuId),null);
    if (CollectionUtils.isEmpty(skuDOList) || skuDOList.size() != 1) {
      errorInfo =
          "Update sku69code error.Query sku by id result error,input commodity skuId:" + cskuId;
      logger.error(errorInfo);
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, errorInfo);
    }
    ItemCommoditySkuDO commoditySkuDO = skuDOList.get(0);

    List<ItemSkuRelationDO> skuRelationDOList =
        itemSkuRelationDOMapper.queryList(SkuRelationQuery.getQueryByCskuIds(Arrays.asList(cskuId),
            RelationTypeConstants.RELATION_TYPE_NORMAL));
    if (CollectionUtils.isEmpty(skuRelationDOList) || skuRelationDOList.size() != 1) {
      errorInfo =
          "Update sku69code error.Query skurelation by id result error,input commodity skuId:"
              + cskuId;
      logger.error(errorInfo);
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, errorInfo);
    }

    Long pskuId = skuRelationDOList.get(0).getPskuId();
    if (null == pskuId || pskuId <= 0) {
      errorInfo =
          "Update sku69code error.Query productsku by id skuid error,input commodity skuId:"
              + cskuId;
      logger.error(errorInfo);
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, errorInfo);
    }

    List<ItemProductSkuDO> productSkuDOList =
        itemProductSkuDOMapper.query(SkuQuery.querySkuById(pskuId));
    if (CollectionUtils.isEmpty(productSkuDOList) || productSkuDOList.size() != 1) {
      errorInfo =
          "Update sku69code error.Query productsku by id result error,input commodity skuId:"
              + pskuId;
      logger.error(errorInfo);
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, errorInfo);
    }

    ItemProductSkuDO productSkuDO = productSkuDOList.get(0);
    productSkuDO.addInputCatProperty(SKU69CODEPROPERTYKEY, String.valueOf(sku69Code));
    productSkuDO.setSku69code(sku69Code);

    commoditySkuDO.addInputCatProperty(SKU69CODEPROPERTYKEY, String.valueOf(sku69Code));
    commoditySkuDO.setSku69code(sku69Code);

    int updateCount = itemProductSkuDOMapper.update(productSkuDO);
    if (updateCount != 1) {
      errorInfo = "Update sku69code error.update productSku error,input commodity skuId:" + pskuId;
      logger.error(errorInfo);
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR_PRODUCT_ITEM_UPDATE,
          errorInfo);
    }
    updateCount = itemCommoditySkuDOMapper.update(commoditySkuDO);
    if (updateCount != 1) {
      errorInfo =
          "Update sku69code error.update commoditysku error,input commodity skuId:" + cskuId;
      logger.error(errorInfo);
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR_PRODUCT_ITEM_UPDATE,
          errorInfo);
    }
    return Result.getSuccDataResult(true);
  }

  @Override
  public Result<List<ItemCommoditySkuDTO>> queryItemSkuList(SkuQuery query) {
    return Result.getSuccDataResult(getItemSkuList(query));
  }

  @Override
  public Result<Integer> queryItemSkuCount(SkuQuery query) {
    return Result.getSuccDataResult(itemCommoditySkuDOMapper.queryCount(query));
  }

  @Override
  public Result<ItemCommoditySkuDTO> queryItemSku(SkuQuery query) {
    List<ItemCommoditySkuDTO> tmpResult = this.getItemSkuList(query);
    if (CollectionUtils.isEmpty(tmpResult)) {
      return Result.getSuccDataResult(null);
    }
    ItemCommoditySkuDTO skuDTO = tmpResult.get(0);

    Map<String, String> unStandardCatPropertyValueMap = skuDTO.queryUnStandardCatPropertyValueMap();
    // 设置前台可见属性合集
    for (int displayPropertyId : PropertyDTO.FRONT_PROPERTY_NAME) {
      String name = propertyservice.queryPropertyNameById(displayPropertyId).getModule();
      String value = unStandardCatPropertyValueMap.get(String.valueOf(displayPropertyId));
      if (StringUtils.isNotEmpty(name) && StringUtils.isNotEmpty(value)) {
        skuDTO.getFrontProperty().put(name, value);
      }
    }
    return Result.getSuccDataResult(skuDTO);
  }

  /**
   * 实现查询销售端sku信息
   * 
   * @param query
   * @return
   */
  private List<ItemCommoditySkuDTO> getItemSkuList(SkuQuery query) {
    StringBuilder sBuilder = new StringBuilder();
    String inOrderIds = null;
    if (null != query.getInOrderBySkuId() && query.getInOrderBySkuId()) {
      // 结果按照id的顺序输出
      logger.error("query:" + JackSonUtil.getJson(query));
      List<Long> skuIds = query.getSkuIds();
      List<Long> sku69Codes = query.getSku69Codes();
      if(CollectionUtils.isNotEmpty(sku69Codes)){
        for(Long sku69Code : sku69Codes){
          if (null == sku69Code) {
            continue;
          }
          sBuilder.append(sku69Code);
          if (sku69Codes.indexOf(sku69Code) != (sku69Codes.size() - 1)) {
            sBuilder.append(",");
          }
        }
      }
      if(StringUtils.isNotEmpty(sBuilder.toString())){
        inOrderIds = sBuilder.toString();
        sBuilder = new StringBuilder();
      }
      if(CollectionUtils.isNotEmpty(skuIds)){
        for (Long skuId : skuIds) {
          if (null == skuId) {
            continue;
          }
          sBuilder.append(skuId);
          if (skuIds.indexOf(skuId) != (skuIds.size() - 1)) {
            sBuilder.append(",");
          }
        }
      }
      if(StringUtils.isNotEmpty(sBuilder.toString())){
        inOrderIds = sBuilder.toString();
      }
    }
    List<ItemCommoditySkuDO> doList = itemCommoditySkuDOMapper.query(query,inOrderIds);
    if (CollectionUtils.isEmpty(doList)) {
      return Collections.<ItemCommoditySkuDTO>emptyList();
    }
    List<ItemCommoditySkuDTO> resultList = new ArrayList<ItemCommoditySkuDTO>();
    BeanCopierUtils.copyListBean(doList, resultList, ItemCommoditySkuDTO.class);

    return resultList;
  }

  @Override
  public Result<Boolean> insertItemSkuRelation(ItemSkuRelationDTO skuRelation) {
    ItemSkuRelationDO itemSkuRelationDO = new ItemSkuRelationDO();
    BeanCopierUtils.copyProperties(skuRelation, itemSkuRelationDO);
    int insertCount = itemSkuRelationDOMapper.insert(itemSkuRelationDO);
    if (insertCount == 1) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert db failed");
    }
  }
  
  @Override
  public Result<Boolean> insertItemSkuScheduleRelation(ItemSkuRelationScheduleDTO skuRelation) {
    if(null == skuRelation){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "para is null");
    }
    ItemSkuRelationScheduleDO itemSkuRelationDO = new ItemSkuRelationScheduleDO();
    BeanCopierUtils.copyProperties(skuRelation, itemSkuRelationDO);
    int insertCount = itemSkuRelationScheduleDOMapper.insert(itemSkuRelationDO);
    if (insertCount == 1) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert db failed");
    }
  }

  
  @Override
  public Result<Boolean> batchInsertItemSkuScheduleRelation(
      List<ItemSkuRelationScheduleDTO> skuRelationList) {
    if(CollectionUtils.isEmpty(skuRelationList)){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "para is null");
    }
    List<ItemSkuRelationScheduleDO> itemSkuRelationDO = Lists.newArrayList();
    BeanCopierUtils.copyListBean(skuRelationList, itemSkuRelationDO, ItemSkuRelationScheduleDO.class);
    int insertCount = itemSkuRelationScheduleDOMapper.batchInsert(itemSkuRelationDO);
    if (insertCount == skuRelationList.size()) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert db failed");
    }
  }

  @Override
  public Result<Boolean> deleteItemSkuScheduleRelation(long id) {
    if(id <= 0){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "para is null");
    }
    itemSkuRelationScheduleDOMapper.deleteByPrimaryKey(id);
    return Result.getSuccDataResult(true);
  }

  
  @Override
  public Result<Boolean> updateItemSkuScheduleRelation(ItemSkuRelationScheduleDTO skuRelation) {
    if (null == skuRelation) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }
    ItemSkuRelationScheduleDO itemSkuRelationDO = new ItemSkuRelationScheduleDO();
    BeanCopierUtils.copyProperties(skuRelation, itemSkuRelationDO);
    int insertCount = itemSkuRelationScheduleDOMapper.update(itemSkuRelationDO);
    if (insertCount == 1) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert db failed");
    }
  }

  @Override
  public Result<Boolean> updateItemSkuRelation(ItemSkuRelationDTO skuRelation) {
    if (null == skuRelation) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }
    return super.updateSkuRelation(skuRelation, skuRelation.getProcessor());
  }
  
  @Override
  public Result<Boolean> updateItemSkuRelationPrice(ItemSkuRelationDTO skuRelation) {
    if (null == skuRelation) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }
    Long id = skuRelation.getId();
    ItemSkuRelationDTO dbRelationDO = itemSkuRelationDOMapper.query(id);
    if(null == dbRelationDO){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }
    Integer bPrice = skuRelation.getbPrice();
    if(bPrice == null){
      bPrice= 0;
    }
    Integer sPrice = skuRelation.getPrice();
    if(sPrice == null){
      sPrice= 0;
    }
    Date now = new Date();
    Date buyStart = skuRelation.getBuyStart();
    Date buyEnd = skuRelation.getBuyEnd();
    Date saleStart = skuRelation.getSaleStart();
    Date saleEnd = skuRelation.getSaleEnd();
    if(null == buyStart){
      buyStart = now;
    }
    if(null == buyEnd){
      buyEnd = DateUtils.addYears(now, 2);
    }
    if(null == saleStart){
      saleStart = now;
    }
    if(null == saleEnd){
      saleEnd = DateUtils.addYears(now, 2);
    }
    Integer pcPromotion = dbRelationDO.getPcProportion();
    if(null == pcPromotion){//默认箱规为1000
      pcPromotion = 1000;
    }
    Integer boxCost = new BigDecimal(pcPromotion).multiply(new BigDecimal(bPrice)).divide(new BigDecimal(1000),3, BigDecimal.ROUND_HALF_EVEN).intValue();
    dbRelationDO.setAvgCost(bPrice);
    dbRelationDO.setBoxCost(boxCost);
    dbRelationDO.setBuyStart(buyStart);
    dbRelationDO.setBuyEnd(buyEnd);
    dbRelationDO.setSaleStart(saleStart);
    dbRelationDO.setSaleEnd(saleEnd);
    dbRelationDO.setPrice(sPrice);
    dbRelationDO.setbPrice(bPrice);
    return super.updateSkuRelation(dbRelationDO, skuRelation.getProcessor());
  }

  protected void logSkuUpPrice(ItemSkuRelationDTO skuRelation) {
    if (null == skuRelation) {
      return;
    }
    ItemSkuRelationDO dbRelation = itemSkuRelationDOMapper.query(skuRelation.getId());
    if (null == dbRelation) {
      return;
    }
    boolean isDiscount = false;
    if (null != skuRelation.getRelationType() && 1 == skuRelation.getRelationType().intValue()) {
      isDiscount = true;
    }
    Integer newBuyPrice = skuRelation.getbPrice();
    Integer oldBuyPrice = dbRelation.getbPrice();
    Integer newPromotion = skuRelation.getPcProportion();
    Integer oldPromotion = dbRelation.getPcProportion();
    Integer newPrice = skuRelation.getPrice();
    Integer oldPrice = dbRelation.getPrice();

    String errorLogBuyPrice = "";
    String errorLogPrice = "";
    String errorLogPromotion = "";
    if (null == newBuyPrice && null == oldBuyPrice) {
    } else if (null != newBuyPrice && null != oldBuyPrice) {
      if (!(newBuyPrice.equals(oldBuyPrice))) {
        errorLogBuyPrice =
            "[Item_update_price], update buy price from " + oldBuyPrice + " to " + newBuyPrice
                + " is Discount " + isDiscount;
      }
    } else {
      errorLogBuyPrice =
          "[Item_update_price], update buy price from " + oldBuyPrice + " to " + newBuyPrice
              + " is Discount " + isDiscount;
    }

    if (null == newPromotion && null == oldPromotion) {
    } else if (null != newPromotion && null != oldPromotion) {
      if (!(newPromotion.equals(oldPromotion))) {
        errorLogBuyPrice =
            "[Item_update_price], update promotion from " + oldBuyPrice + " to " + newBuyPrice
                + " is Discount " + isDiscount;
      }
    } else {
      errorLogBuyPrice =
          "[Item_update_price], update promotion from " + oldBuyPrice + " to " + newBuyPrice
              + " is Discount " + isDiscount;
    }

    if (null == newPrice && null == oldPrice) {
    } else if (null != newPrice && null != oldPrice) {
      if (!(newPrice.equals(oldPrice))) {
        errorLogBuyPrice =
            "[Item_update_price], update promotion from " + oldPrice + " to " + newPrice
                + " is Discount " + isDiscount;
      }
    } else {
      errorLogBuyPrice =
          "[Item_update_price], update promotion from " + oldPrice + " to " + newPrice
              + " is Discount " + isDiscount;
    }

    if (StringUtils.isNotEmpty(errorLogBuyPrice)) {
      logger.error(errorLogBuyPrice);
    }
    if (StringUtils.isNotEmpty(errorLogPrice)) {
      logger.error(errorLogPrice);
    }
    if (StringUtils.isNotEmpty(errorLogPromotion)) {
      logger.error(errorLogPromotion);
    }
  }

  /**
   * 追加属性
   * 
   * @param itemSkuRelationDTO
   */
  private void appendItemSkuRelationDTO(List<ItemSkuRelationDTO> itemSkuRelationDTOList) {
    if (CollectionUtils.isEmpty(itemSkuRelationDTOList)) {
      return;
    }

    Map<Long, ItemCommoditySkuDTO> cskuObjectM = Maps.newHashMap();
    Map<Long, ItemProductSkuDTO> pskuObjectM = Maps.newHashMap();

    List<Long> cskuList = Lists.newArrayList();
    List<Long> pskuList = Lists.newArrayList();

    for (ItemSkuRelationDTO skuRelation : itemSkuRelationDTOList) {
      long csku = skuRelation.getCskuId();
      long psku = skuRelation.getPskuId();
      if (csku > 0) {
        cskuList.add(csku);
      }
      if (psku > 0) {
        pskuList.add(psku);
      }
    }

    if (CollectionUtils.isNotEmpty(cskuList)) {
      SkuQuery cskuQuery = SkuQuery.querySkuBySkuIds(cskuList);
      List<ItemCommoditySkuDTO> result = getItemSkuList(cskuQuery);
      for (ItemCommoditySkuDTO csku : result) {
        cskuObjectM.put(csku.getSkuId(), csku);
      }
    }
    if (CollectionUtils.isNotEmpty(pskuList)) {
      SkuQuery pskuQuery = SkuQuery.querySkuBySkuIds(pskuList);
      List<ItemProductSkuDTO> result = queryProductSkuList(pskuQuery);
      for (ItemProductSkuDTO psku : result) {
        pskuObjectM.put(psku.getSkuId(), psku);
      }
    }

    for (ItemSkuRelationDTO skuRelation : itemSkuRelationDTOList) {
      long csku = skuRelation.getCskuId();
      long psku = skuRelation.getPskuId();
      if (csku > 0) {
        skuRelation.setItemCommoditySku(cskuObjectM.get(csku));
      }
      if (psku > 0) {
        skuRelation.setItemProductSku(pskuObjectM.get(psku));
      }
    }
  }

  @Override
  public Result<ItemSkuRelationDTO> querySkuRelation(Long id) {
    ItemSkuRelationDO tmp = itemSkuRelationDOMapper.query(id);
    if (null != tmp) {
      ItemSkuRelationDTO result = new ItemSkuRelationDTO();
      BeanCopierUtils.copyProperties(tmp, result);
      return Result.getSuccDataResult(result);
    }
    return Result.getSuccDataResult(null);
  }

  @Override
  public Result<List<ItemSkuRelationDTO>> querySkuRelationList(SkuRelationQuery skuRelationQuery) {
    if (null == skuRelationQuery) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input parameter is null");
    }

    if (null != skuRelationQuery.getSupplier() && skuRelationQuery.getSupplier() > 0) {
      SkuQuery sk = SkuQuery.querySkuBySupplier(skuRelationQuery.getSupplier());
      List<ItemCommoditySkuDTO> tmpCommodityResult = getItemSkuList(sk);
      List<Long> cskuids = new ArrayList<Long>();
      if (CollectionUtils.isNotEmpty(tmpCommodityResult)) {
        for (ItemCommoditySkuDTO sku : tmpCommodityResult) {
          cskuids.add(sku.getSkuId());
        }
        skuRelationQuery =
            SkuRelationQuery.getQueryByCskuIds(cskuids, skuRelationQuery.getRelationType());
      }
    }

    List<ItemSkuRelationDO> tmpResult = itemSkuRelationDOMapper.queryList(skuRelationQuery);
    List<ItemSkuRelationDTO> result = new ArrayList<ItemSkuRelationDTO>();
    BeanCopierUtils.copyListBean(tmpResult, result, ItemSkuRelationDTO.class);
    appendItemSkuRelationDTO(result);
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<List<ItemSkuRelationScheduleDTO>> querySkuRelationScheduleDetailList(
      SkuRelationScheduleQuery skuRelationQuery) {
    if (null == skuRelationQuery) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input parameter is null");
    }

    List<ItemSkuRelationScheduleDO> tmpResult = itemSkuRelationScheduleDOMapper.queryList(skuRelationQuery);
    List<ItemSkuRelationScheduleDTO> result = new ArrayList<ItemSkuRelationScheduleDTO>();
    BeanCopierUtils.copyListBean(tmpResult, result, ItemSkuRelationScheduleDTO.class);
    return Result.getSuccDataResult(result);
  }
  
  @Override
  public Result<List<ItemSkuRelationScheduleDTO>> queryAvailableSkuRelationScheduleList(
      SkuRelationAvaliableScheduleQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input parameter is null");
    }

    List<ItemSkuRelationScheduleDO> tmpResult = itemSkuRelationScheduleDOMapper.queryAvaliableSchedule(query,query.getType(),query.getRelationType());
    List<ItemSkuRelationScheduleDTO> result = new ArrayList<ItemSkuRelationScheduleDTO>();
    BeanCopierUtils.copyListBean(tmpResult, result, ItemSkuRelationScheduleDTO.class);
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<Integer> querySkuRelationCount(SkuRelationQuery skuRelationQuery) {
    if (null == skuRelationQuery) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input parameter is null");
    }

    if (null != skuRelationQuery.getSupplier() && skuRelationQuery.getSupplier() > 0) {
      SkuQuery sk = SkuQuery.querySkuBySupplier(skuRelationQuery.getSupplier());
      List<ItemCommoditySkuDTO> tmpCommodityResult = getItemSkuList(sk);
      List<Long> cskuids = new ArrayList<Long>();
      if (CollectionUtils.isNotEmpty(tmpCommodityResult)) {
        for (ItemCommoditySkuDTO sku : tmpCommodityResult) {
          cskuids.add(sku.getSkuId());
        }
        skuRelationQuery =
            SkuRelationQuery.getQueryByCskuIds(cskuids, skuRelationQuery.getRelationType());
      }
    }

    return Result.getSuccDataResult(itemSkuRelationDOMapper.queryCount(skuRelationQuery));
  }

  @Override
  public Result<Boolean> updateItemSkuTags(Long skuId, Integer tag, Integer addRemoveFlag) {
    List<ItemCommoditySkuDO> skuList = itemCommoditySkuDOMapper.query(SkuQuery.querySkuById(skuId),null);
    if (CollectionUtils.isEmpty(skuList)) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR_COMMODITY_SKU_NOT_EXIT,
          "Query sku not exit,skuId:" + skuId);
    }
    ItemCommoditySkuDO sku = skuList.get(0);
    Integer skuTag = sku.getTags();
    if (skuTag == null) {
      skuTag = 0;
    }
    int skuNewTag = skuTag;
    if (SkuConstants.SKU_TAG_FLAG_ADD.equals(addRemoveFlag)) {
      skuNewTag = skuTag | tag;
    } else {
      skuNewTag = skuTag & (~tag);
    }
    if (skuTag.equals(skuNewTag)) {
      return Result.getSuccDataResult(true);
    } else {
      sku.setTags(skuNewTag);
      int updateCount = itemCommoditySkuDOMapper.update(sku);
      if (updateCount == 1) {
        return Result.getSuccDataResult(true);
      } else {
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR_COMMODITY_SKU_UPDATE,
            "Update sku failed,skuId:" + skuId);
      }
    }
  }

  @Override
  public Result<List<ItemSkuTagsDTO>> querySkuTagsDetail(List<Long> skuIds) {
    if (CollectionUtils.isEmpty(skuIds)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error");
    }
    List<ItemSkuTagsDO> itemSkuTagsDOs = itemCommoditySkuDOMapper.queryItemTagsDetail(skuIds);
    if (CollectionUtils.isEmpty(itemSkuTagsDOs)) {
      return Result
          .getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "data is not exist");
    }
    List<ItemSkuTagsDTO> itemSkuTagsDTOs = Lists.newArrayList();

    // 不能直接使用数据库返回的数据结构，需要这种格式解析一次
    // 数据库do不需要要定义很多值，只要feature字段和tag字段，新格式的skuTagDetailList以json格式存入数据库即可
    // ItemSkuTagsDTO 对外暴露的 queryFeatureMap 方法都要干掉，这种写法不好的，这个版本开始不要这么写了
    for (ItemSkuTagsDO skuDO : itemSkuTagsDOs) {
      ItemSkuTagsDTO skuTagDTO = getItemSkuTagsDTOFromDB(skuDO);
      itemSkuTagsDTOs.add(skuTagDTO);
    }
    return Result.getSuccDataResult(itemSkuTagsDTOs);
  }

  /**
   * 翻译服务端商品标的详细内容
   * 
   * @return
   */
  private ItemSkuTagsDTO getItemSkuTagsDTOFromDB(ItemSkuTagsDO skuTagsDO) {
    ItemSkuTagsDTO result = new ItemSkuTagsDTO();
    result.setFeature(skuTagsDO.getFeature());
    result.setSkuId(skuTagsDO.getSkuId());
    result.setTags(skuTagsDO.getTags());
    List<SkuTagDetail> skuTagDetailList = null;
    String feature = skuTagsDO.getFeature();
    Map<String, String> featureM = super.queryFeatureMap(feature);
    logger.error(JackSonUtil.getJson(featureM));
    if (null != featureM) {
      String skuTagDetailListJson = featureM.get(TAG_DETAIL_KEY);
      if (StringUtils.isNotEmpty(skuTagDetailListJson)) {
        logger.error(skuTagDetailListJson);
        skuTagDetailList = JackSonUtil.jsonToList(skuTagDetailListJson, SkuTagDetail.class);
      }
    }
    if (CollectionUtils.isEmpty(skuTagDetailList)) {
      logger.error("tag detail数据为空");
    }
    // 根据商品标设置是否可加购，是否可下单等属性
    Integer tags = skuTagsDO.getTags();
    if (null == tags) {
      tags = 0;
    }
    Boolean mayPlus = result.getMayPlus();
    if (null == mayPlus) {
      mayPlus = true;
    }
    Boolean mayOrder = result.getMayOrder();
    if (null == mayOrder) {
      mayOrder = true;
    }
    Boolean mayInventoryEnough = result.getMayInventoryEnough();
    if (null == mayInventoryEnough) {
      mayInventoryEnough = true;
    }
    Boolean mayPromotionSku = result.getMayPromotionSku();
    if (null == mayPromotionSku) {
      mayPromotionSku = false;
    }
    Boolean mayLimitBuySku = result.getMayLimitBuySku();
    if (null == mayLimitBuySku) {
      mayLimitBuySku = false;
    }
    // 32的标表示线上满40减20
    if ((tags & 0x20) != 0) {
      mayPromotionSku |= true;
    }
    // 1的标表示线上满99减40
    if ((tags & 0x1) != 0) {
      mayPromotionSku |= true;
    }
    // 2的标表示线下满40减20
    if ((tags & 0x2) != 0) {
      mayPromotionSku |= true;
    }
    // 16的标表示线下满99减40
    if ((tags & 0x10) != 0) {
      mayPromotionSku |= true;
    }
    // // 4的标表示可交易有特殊标
    // if ((tags & 0x4) != 0) {
    // mayPlus &= true;
    // mayOrder &= true;
    // mayInventoryEnough &= true;
    // }
    // // 64的标表示可交易但是没有特殊标
    // if ((tags & 0x40) != 0) {
    // mayPlus &= true;
    // mayOrder &= true;
    // mayInventoryEnough &= true;
    // }
    // 128标表示不可交易但是可以有特殊标
    if ((tags & 0x80) != 0) {
      mayPlus &= false;
      mayOrder &= false;
    }
    // 256的标表示没有库存
    if ((tags & 0x100) != 0) {
      mayInventoryEnough &= false;// 库存为空
    }
    // 512的标表示商品优惠标
    if ((tags & 0x200) != 0) {
      mayPromotionSku |= true;
    }
    // 4096的标表示商品限购标
    if ((tags & 0x800) != 0) {
      mayLimitBuySku |= true;
    }
    result.setMayInventoryEnough(mayInventoryEnough);
    result.setMayLimitBuySku(mayLimitBuySku);
    result.setMayOrder(mayOrder);
    result.setMayPlus(mayPlus);
    result.setMayPromotionSku(mayPromotionSku);
    result.setSkuTagDetailList(getSkuTagDetailList(skuTagDetailList, tags));
    return result;
  }

  /**
   * 根据tags补充商品标详细列表
   * 
   * @param skuTagDetailList
   * @param tags
   * @return
   */
  private List<SkuTagDetail> getSkuTagDetailList(List<SkuTagDetail> skuTagDetailList, Integer tags) {
    if (null == tags) {
      return skuTagDetailList;
    }

    if (null == skuTagDetailList) {
      skuTagDetailList = Lists.newArrayList();
    }
    // 解析前9位标内容
    for (int i = 1; i <= 1048576; i = (i << 1)) {
      if ((tags & i) == i) {
        SkuTagDetail sd = new SkuTagDetail();
        if (i == 0x1) {
          // 扩展字段中的skuTagDetailList没有满99减40的tagId,所以不需要判断是否已存在
          sd.setTagId(SkuTagDefinitionConstants.BUY_DISCOUNT_99_40);
          sd.setChannel(SkuTagDetail.CHANNEL_ONLINE);
          sd.setTagName("线上满99减40元");
        } else if (i == 0x2) {
          // 扩展字段中的skuTagDetailList没有满40减20的tagId,所以不需要判断是否已存在
          sd.setTagId(SkuTagDefinitionConstants.BUY_DISCOUNT_40_20);
          sd.setChannel(SkuTagDetail.CHANNEL_OFFLINE);
          sd.setTagName("线下满40减20元");
        } else if (i == 0x4) {
          if (!tagIdExist(SkuTagDefinitionConstants.DES_TAG, skuTagDetailList)) {
            sd.setTagId(SkuTagDefinitionConstants.DES_TAG);
          }
        } else if (i == 0x8) {
          if (!tagIdExist(SkuTagDefinitionConstants.LIMITED_TIME_PREFERENCE, skuTagDetailList)) {
            sd.setTagId(SkuTagDefinitionConstants.LIMITED_TIME_PREFERENCE);
          }
        } else if (i == 0x10) {
          // 扩展字段中的skuTagDetailList没有满99减40的tagId,所以不需要判断是否已存在
          sd.setTagId(SkuTagDefinitionConstants.BUY_DISCOUNT_99_40);
          sd.setChannel(SkuTagDetail.CHANNEL_OFFLINE);
          sd.setTagName("线下满99减40元");
        } else if (i == 0x20) {
          // 扩展字段中的skuTagDetailList没有满40减20的tagId,所以不需要判断是否已存在
          sd.setTagId(SkuTagDefinitionConstants.BUY_DISCOUNT_40_20);
          sd.setChannel(SkuTagDetail.CHANNEL_ONLINE);
          sd.setTagName("线上满40减20元");
        } else if (i == 0x40) {
          if (!tagIdExist(SkuTagDefinitionConstants.NOT_ALLOW_PROMOTION, skuTagDetailList)) {
            sd.setTagId(SkuTagDefinitionConstants.NOT_ALLOW_PROMOTION);
          }
        } else if (i == 0x80) {
          if (!tagIdExist(SkuTagDefinitionConstants.NOT_ALLOW_CHARGE, skuTagDetailList)) {
            sd.setTagId(SkuTagDefinitionConstants.NOT_ALLOW_CHARGE);
          }
        } else if (i == 0x100) {
          if (!tagIdExist(SkuTagDefinitionConstants.NOT_ENOUGH_INVENTORY, skuTagDetailList)) {
            sd.setTagId(SkuTagDefinitionConstants.NOT_ENOUGH_INVENTORY);
          }
        }
        if (null != sd && StringUtils.isNotBlank(sd.getTagId())) {
          skuTagDetailList.add(sd);
        }
      }
    }
    return skuTagDetailList;
  }

  /**
   * 判断tag id是否已经存在skuTagDetailList
   * 
   * @param desTag
   * @param skuTagDetailList
   * @return
   */
  private boolean tagIdExist(String tagId, List<SkuTagDetail> skuTagDetailList) {
    if (CollectionUtils.isEmpty(skuTagDetailList)) {
      return false;
    }
    List<String> tagIds = Lists.newArrayList();
    for (SkuTagDetail skuTagDetail : skuTagDetailList) {
      tagIds.add(skuTagDetail.getTagId());
    }
    if (CollectionUtils.isNotEmpty(tagIds) && tagIds.contains(tagId.toString())) {
      return true;
    }
    return false;
  }

  @Override
  public Result<Boolean> updateItemSkuChannel(Long skuId, Short saleChannel) {
    if(null == skuId || null == saleChannel){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "input parameter is null");
    }
    if(!SaleStatusConstants.isCorrectParameter(saleChannel)){
      logger.error("saleStatus not in(1,2,3)");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "input parameter is error");
    }
    List<ItemCommoditySkuDO> itemCommoditySkuDOs = itemCommoditySkuDOMapper.query(SkuQuery.querySkuById(skuId),null);
    if(CollectionUtils.isEmpty(itemCommoditySkuDOs)){
      logger.error("item sku is not exist with this query sku id:" + skuId);
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "item sku is not exist with this query sku id:" + skuId);
    }
    ItemCommoditySkuDO itemCommoditySkuDO = itemCommoditySkuDOs.get(0);
    itemCommoditySkuDO.setSaleStatus(saleChannel);
    ItemDO itemDO = itemDOMapper.queryItem(itemCommoditySkuDO.getItemId());
    if(null == itemDO){
      logger.error("data is error,item is not exist with this query sku id:" + skuId);
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "item is not exist with this query sku id:" + skuId);
    }
    itemDO.setSaleStatus(saleChannel);
    boolean updateResult = false;
    int updateItemSaleChannel = itemDOMapper.updateItem(itemDO);
    if(updateItemSaleChannel > 0){
      updateResult = itemCommoditySkuDOMapper.update(itemCommoditySkuDO) > 0 ? true : false;
    }
    return Result.getSuccDataResult(updateResult);
  }

  @Override
  public Result<ItemSkuWeightInfoDTO> queryWeighItemInfo(Long skuId, Integer weight) {
    if(null == skuId){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "the query sku id is null");
    }
    if(null == weight){
      weight = 0;
    }
    Result<ItemCommoditySkuDTO> itemSkuResult = queryItemSku(SkuQuery.querySkuById(skuId));
    if(null == itemSkuResult || !itemSkuResult.getSuccess() || null == itemSkuResult.getModule()){
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "item not exist with this query:" + skuId);
    }
    ItemCommoditySkuDTO itemCommoditySkuDTO = itemSkuResult.getModule();
    Integer price = itemCommoditySkuDTO.getPrice();
    Integer discountPrice = itemCommoditySkuDTO.getDiscountPrice();
    String count = new BigDecimal(weight).divide(new BigDecimal(itemCommoditySkuDTO.getSaleRangeNum()), 3, BigDecimal.ROUND_DOWN).toString();
    Integer priceTotal = new BigDecimal(price).multiply(new BigDecimal(count)).setScale(0, BigDecimal.ROUND_UP).intValue();
    Integer discountPriceTotal = new BigDecimal(discountPrice).multiply(new BigDecimal(count)).setScale(0, BigDecimal.ROUND_UP).intValue();
    ItemSkuWeightInfoDTO itemSkuWeightInfoDTO = new ItemSkuWeightInfoDTO();
    itemSkuWeightInfoDTO.setCount(count);
    itemSkuWeightInfoDTO.setDiscountPrice(discountPrice);
    itemSkuWeightInfoDTO.setDiscountPriceTotal(discountPriceTotal);
    itemSkuWeightInfoDTO.setPrice(price);
    itemSkuWeightInfoDTO.setPriceTotal(priceTotal);
    itemSkuWeightInfoDTO.setSkuId(skuId);
    return Result.getSuccDataResult(itemSkuWeightInfoDTO);
  }
}
