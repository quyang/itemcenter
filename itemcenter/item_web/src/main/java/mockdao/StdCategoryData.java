package mockdao;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.caucho.hessian.client.HessianProxyFactory;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;

/**
 * 导入类目数据
 * 
 * @author dongpo
 * 
 */
public class StdCategoryData {

  private static Logger LOGGER = Logger.getLogger(StdCategoryData.class);

  private static final String[] catName1 = {"非销售"};

  private static final String[] catName2 = {"原物料"};

  private static final String[] catName3 = {"加工原物料"};

  private static final String[] catName4 = {"包材", "配料"};
  private static final Long[] catCode1 = {5L};
  private static final Long[] catCode2 = {5001L};
  private static final Long[] catCode3 = {5001001L};
  private static final Long[] catCode4 = {50010010001L, 50010010002L};
  private static final String[] features = {"l=1", "l=2", "l=3", "l=4"};
  private static final String VALUE_URL = "http://10.10.0.12:7009/hessian/stdcategoryservice";

  private StdCategoryService createService() {
    HessianProxyFactory factory = new HessianProxyFactory();
    StdCategoryService service = null;
    try {
      service = (StdCategoryService) factory.create(StdCategoryService.class, VALUE_URL);
    } catch (MalformedURLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }// 创建IService接口的实例对象
    return service;
  }

  public void insertStdCategory() {

    for (int i = 0; i < catCode1.length; i++) {
      CategoryDTO categoryDto = new CategoryDTO();
      categoryDto.setCatType((short) 1);
      categoryDto.setCreatorId(1);
      categoryDto.setStatus((short) 1);
      Integer parentId = 0;
      categoryDto.setCatCode(catCode1[i]);
      categoryDto.setCatName(catName1[i]);
      categoryDto.setFeatures(features[i]);
      Short isLeaf = 0;
      categoryDto.setLeaf(isLeaf);
      categoryDto.setMemo(catName1[i]);
      categoryDto.setParentId(parentId);
      categoryDto.setSortValue((short) 0);
      depthCategory(categoryDto);
    }
    insertProperty();
  }

  public void insertProperty() {
    StdCategoryService service = createService();
    for (String catName : catName4) {
      Result<CategoryDTO> result = service.queryCategoryByName(catName);
      CategoryDTO categoryDTO = result.getModule();
      if (null != categoryDTO && categoryDTO.getLeaf() == 1) {
        PropertyData propertyData = new PropertyData(categoryDTO.getCatCode());
        propertyData.insertPropertyData(categoryDTO.getCatId());
      }
    }
  }

  /**
   * 获取子节点
   * 
   * @param node
   * @return
   */
  private List<CategoryDTO> getChildrenCategory(CategoryDTO node) {
    List<CategoryDTO> categoryDTOs = new ArrayList<>();
    Long catCode = node.getCatCode();
    List<String> catNameList = new ArrayList<>();
    List<Long> catCodeList = new ArrayList<>();
    List<String> featuresList = new ArrayList<>();
    for (int k = 0; k < catCode1.length; k++) {
      if (catCode.equals(catCode1[k])) {
        for (int i = 0; i < catName2.length; i++) {
          Long xCode = catCode2[i] / 1000L;
          if (xCode.equals(catCode)) {
            catNameList.add(catName2[i]);
            catCodeList.add(catCode2[i]);
            featuresList.add(features[1]);
          }
        }
      } else {
        for (int i = 0; i < catName3.length; i++) {
          if (i < catName2.length && catCode.equals(catCode2[i])) {
            for (int j = 0; j < catCode3.length; j++) {
              Long xcode = catCode3[j] / 1000L;
              if (xcode.equals(catCode)) {

                catCodeList.add(catCode3[j]);
                catNameList.add(catName3[j]);
                featuresList.add(features[2]);
              }
            }
          } else if (catCode.equals(catCode3[i])) {
            for (int j = 0; j < catCode4.length; j++) {
              Long xcode = catCode4[j] / 10000L;
              if (xcode.equals(catCode)) {
                catCodeList.add(catCode4[j]);
                catNameList.add(catName4[j]);
                featuresList.add(features[3]);
              }
            }
          }
        }
      }
    }

    for (int i = 0; i < catNameList.size(); i++) {
      CategoryDTO categoryDto = new CategoryDTO();
      categoryDto.setCatType((short) 1);
      categoryDto.setCreatorId(1);
      categoryDto.setStatus((short) 1);
      categoryDto.setCatCode(catCodeList.get(i));
      categoryDto.setCatName(catNameList.get(i));
      categoryDto.setFeatures(featuresList.get(i));
      Short isLeaf = 0;
      if (featuresList.get(i).equals("l=4")) {
        isLeaf = (short) 1;
      }
      categoryDto.setLeaf(isLeaf);
      categoryDto.setMemo(catNameList.get(i));
      categoryDto.setParentId(node.getCatId());
      categoryDto.setSortValue((short) i);
      categoryDTOs.add(categoryDto);
    }

    return categoryDTOs;
  }


  /**
   * 深度遍历
   * 
   * @param node
   */
  public void depthCategory(CategoryDTO parentNode) {
    StdCategoryService service = createService();
    Stack<CategoryDTO> nodeStack = new Stack<CategoryDTO>();
    nodeStack.add(parentNode);
    while (!nodeStack.isEmpty()) {
      parentNode = nodeStack.pop();
      Result<Integer> result = null;
      Result<CategoryDTO> categoryResult = service.queryCategoryByName(parentNode.getCatName());
      CategoryDTO categoryDTO = categoryResult.getModule();
      if (null == categoryDTO.getCatId()
          || (null != categoryDTO.getCatCode() && !categoryDTO.getCatCode().equals(
              parentNode.getCatCode()))) {
        result = service.insertCategory(parentNode);
        if (null != result) {
          parentNode.setCatId(result.getModule());
          LOGGER.info(result.getModule());
        }
      } else {
        parentNode.setCatId(categoryDTO.getCatId());
      }
      List<CategoryDTO> children = getChildrenCategory(parentNode);// 获得节点的子节点
      if (children != null && !children.isEmpty()) {
        for (CategoryDTO child : children) {
          nodeStack.push(child);
        }
      }
    }
  }

  /**
   * 插入单个子类目
   * 
   * @param parentId
   * @param catName
   * @return
   */
  public Result<Integer> insertSubCategoryDate(Integer parentId, String catName) {
    if (null == parentId || null == catName) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空");
    }
    StdCategoryService stdCategoryService = createService();
    Result<CategoryDTO> catNameResult = stdCategoryService.queryCategoryByName(catName);
    LOGGER.error("数据库是否存在该类目名：" + JackSonUtil.getJson(catNameResult));
    if (null != catNameResult && catNameResult.getSuccess() && null != catNameResult.getModule()
        && null != catNameResult.getModule().getCatId()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_AREADY_EXIST,
          "该类目名已经存在数据库，请换一个类目名称");
    }
    Result<List<CategoryDTO>> categoryResult = stdCategoryService.querySubcategoryById(parentId);
    Long catCode = 0L;
    Integer level = 0;
    Short sortValue = 1;
    Map<String, Object> mapResult = getCategoryCode(parentId);
    if (null == categoryResult || !categoryResult.getSuccess()
        || CollectionUtils.isEmpty(categoryResult.getModule())) {
      catCode = (Long) mapResult.get("subCatCode");// 获取类目编码
      level = Integer.valueOf((String) mapResult.get("level")) + 1;
    } else {
      List<CategoryDTO> categoryDTOs = categoryResult.getModule();
      CategoryDTO catCodeMax = categoryDTOs.get(0);
      for (CategoryDTO categoryDTO : categoryDTOs) {
        if (categoryDTO.getCatCode().longValue() > catCodeMax.getCatCode().longValue()) {
          catCodeMax = categoryDTO;
        }
      }
      catCode = catCodeMax.getCatCode() + 1;// 类目编码最大的加1
      level = Integer.valueOf(catCodeMax.getFeatures().trim().split("=")[1]);
      sortValue = (short) (catCodeMax.getSortValue() + 1);
    }
    CategoryDTO categoryDTO = new CategoryDTO();
    categoryDTO.setCatCode(catCode);
    categoryDTO.setCatName(catName);
    categoryDTO.setCatType((short) 1);
    categoryDTO.setCreatorId(1);

    Short isLeaf = 0;
    if (level == 4) {
      isLeaf = 1;
    }
    String features = "l=" + level;
    categoryDTO.setFeatures(features);
    categoryDTO.setGmtCreate(new Date());
    categoryDTO.setGmtModified(new Date());
    categoryDTO.setLeaf(isLeaf);
    categoryDTO.setMemo(catName);
    categoryDTO.setParentId(parentId);
    categoryDTO.setSortValue(sortValue);
    categoryDTO.setStatus((short) 1);
    Result<Integer> catIdResult = stdCategoryService.insertCategory(categoryDTO);
    if (null != categoryDTO && categoryDTO.getLeaf() == 1) {
      PropertyData propertyData = new PropertyData(categoryDTO.getCatCode());
      propertyData.insertPropertyData(catIdResult.getModule());
    }
    return catIdResult;
  }

  /**
   * 获取类目编码
   * 
   * @param parentId
   * @return
   */
  private Map<String, Object> getCategoryCode(Integer parentId) {
    Long subCatCode = 0L;
    String level = "0";
    Map<String, Object> map = Maps.newHashMap();
    if (parentId != 0) {
      StdCategoryService stdCategoryService = createService();
      Result<CategoryDTO> categoryResult = stdCategoryService.queryCategoryById(parentId);
      LOGGER.error("父类目：" + JackSonUtil.getJson(categoryResult));
      if (null != categoryResult && categoryResult.getSuccess()
          && null != categoryResult.getModule()) {
        CategoryDTO categoryDTO = categoryResult.getModule();
        String features = categoryDTO.getFeatures();
        if(null == features){
          features = "l=0";
        }
        Long catCode = categoryDTO.getCatCode();
        level = features.trim().split("=")[1];
        if (StringUtils.isBlank(level)) {
          map.put("subCatCode", subCatCode);
          map.put("level", "0");
          return map;
        }
        switch (level) {
          case "1":
            subCatCode = catCode * 1000 + 1L;
            break;
          case "2":
            subCatCode = catCode * 1000 + 1L;
            break;
          case "3":
            subCatCode = catCode * 10000 + 1L;
            break;
        }
      } else {
        subCatCode = 1L;
      }
    }
    map.put("subCatCode", subCatCode);
    map.put("level", level);
    return map;
  }


}
