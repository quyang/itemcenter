package mockdao;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.caucho.hessian.client.HessianProxyFactory;
import com.xianzaishi.itemcenter.client.property.PropertyService;
import com.xianzaishi.itemcenter.client.property.dto.PropertyDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 导入属性数据
 * @author dongpo
 *
 */
public class PropertyData {
  private static Logger LOGGER = Logger.getLogger(PropertyData.class);

  private static final String VALUE_URL = "http://10.10.0.12:7009/hessian/propertyservice";
  private static final String[] propertyData1 = {"进口/外采品", "品牌", "国家", "产地", "外部条码", "件装数",
      "件装单位属性", "件装规格", "件装属性", "采购规格", "销售规格", "销售外包装长度", "销售外包装宽度", "销售外包装高度", "外箱长度", "外箱宽度",
      "外箱高度", "质量等级", "保质期", "销项税", "进项税", "正常进价", "进价货币单位", "正常售价", "售价货币单位", "最小起定量", "是否免农业税",
      "温度条件标识", "是否可以退货", "是否可销售", "销售类型", "增加1个单位重量", "标价类型", "价签类型", "生产厂商名称", "生产厂商地址",
      "称重类自然属性", "称重比例", "订货地址", "是否包含加工费", "加工费用类型", "加工费用"};
  private static final String[] propertyData2 = {"加工档口", "可食用时间", "食用类型", "品牌", "销售规格", "销售外包装长度",
      "销售外包装宽度", "销售外包装高度", "销项税", "进项税", "正常进价", "进价货币单位", "正常售价", "售价货币单位", "温度条件标识", "是否可以退货",
      "是否可销售", "销售类型", "标价类型", "价签类型", "是否包含加工费", "加工费用类型", "加工费用"};
  private static final Integer[] tags1 = {1011, 1101, 1101, 1101, 1101, 1101, 1011, 1101, 1011,
      1011, 1011, 1101, 1101, 1101, 1100, 1100, 1100, 1010, 1101, 1101, 1101, 1101, 1011, 1101,
      1011, 1100, 1011, 1011, 1011, 1011, 1010, 1100, 1010, 1010, 1101, 1101, 1010, 1100, 1011,
      1010, 1010, 1100};
  private static final Integer[] tags2 = {1101, 1101, 1011, 1101, 1011, 1101, 1101, 1101, 1101,
      1101, 1101, 1011, 1101, 1011, 1011, 1011, 1011, 1010, 1010, 1010, 1010, 1010, 1100};

  private static final String[] valueData1 = {"普通外采品", "进口直采", "kg", "把", "板", "包", "杯", "本", "部",
      "串", "打", "袋", "碟", "顶", "对", "朵", "份", "封", "幅", "副", "个", "根", "管", "罐", "锅", "盒", "壶",
      "加仑", "架", "件", "节", "斤", "卷", "卡", "棵", "颗", "块", "篮", "粒", "联", "两", "辆", "笼", "篓", "枚",
      "排", "盘", "片", "品脱", "瓶", "勺", "束", "双", "台", "坛", "套", "提", "条", "听", "桶", "筒", "头", "碗",
      "箱", "扎", "张", "支", "只", "组", "抽", "克", "一级", "特级", "二级", "CNY", "是", "不是", "活物", "常温", "热藏>63℃",
      "冷藏", "冷冻", "线上线下同步", "仅线上", "仅线下", "大标签", "小标签+保质期+追溯", "小标签+日期+追溯", "小标无日期", "纸小", "纸中",
      "纸大", "电小", "电大", "鲜在时大仓", "鲜在时陆家嘴仓", "鲜在时加工中心", "鲜在时东方路店", "固定费用", "商品价格百分比"};

  private static final String[] valueData2 = {"即食", "加热后食用", "kg", "把", "板", "包", "杯", "本", "部", "串",
      "打", "袋", "碟", "顶", "对", "朵", "份", "封", "幅", "副", "个", "根", "管", "罐", "锅", "盒", "壶", "加仑",
      "架", "件", "节", "斤", "卷", "卡", "棵", "颗", "块", "篮", "粒", "联", "两", "辆", "笼", "篓", "枚", "排",
      "盘", "片", "品脱", "瓶", "勺", "束", "双", "台", "坛", "套", "提", "条", "听", "桶", "筒", "头", "碗", "箱",
      "扎", "张", "支", "只", "组", "抽", "克", "一级", "特级", "二级", "CNY", "是", "不是", "活物", "常温", "热藏>63℃",
      "冷藏", "冷冻", "线上线下同步", "仅线上", "仅线下", "大标签", "小标签+保质期+追溯", "小标签+日期+追溯", "小标无日期", "纸小", "纸中",
      "纸大", "电小", "电大", "鲜在时大仓", "鲜在时陆家嘴仓", "鲜在时加工中心", "鲜在时东方路店", "固定费用", "商品价格百分比"};
  private static final String[] valueData3 = {"加工后食用"};

  private static final String[] memo1 = {"来源描述", "商品品牌", "产地国家", "产地区域", "69码", "箱入数。一个大箱里面多少个小件",
      "箱入属性。对应W3", "箱内每组规格，如大箱内套小箱,如果没有组数则填写1", null, "储存规格和采购规格一致", "销售规格(如果又按单件卖，又可以整组卖，请新建单品)",
      "单位：CM 件装长度", "单位：CM 件装宽度", "单位：CM 件装宽度", "单位：CM 件装长度", "单位：CM 件装宽度", "单位：CM 件装宽度", "质量信息描述",
      null, null, null, null, null, "含税金额", null, null, null, null, "是否支持退货", null, null,
      "散装称重商品销售时增加一个销售单元实际增加重量", "商品PLU码打印出来以后的类型", "消费者展示方式", null, null, "称重商品，自然形态单位",
      "一个自然形态，对应多少件装数", null, "商品销售时，是否包含加工费", "加工费用类型",
      "加工费数值(加工费类型为价格百分比时，代表百分比数值；加工费类型为固定时，代表绝对加工费用，元为单价)"};

  private static final String[] memo2 = {null, null, null, "商品品牌", "销售规格(如果又按单件卖,又可以整组卖,请新建单品)",
      "单位：CM 件装长度", "单位：CM 件装宽度", "单位：CM 件装宽度", null, null, null, null, "含税金额", null, null,
      "是否支持退货", null, null, "商品PLU码打印出来以后的类型", "消费者展示方式", "商品销售时，是否包含加工费", "加工费用类型",
      "加工费数值(加工费类型为价格百分比时，代表百分比数值；加工费类型为固定时，代表绝对加工费用，元为单价)"};

  private String propertyData[] = null;
  private Integer tags[] = null;
  private String valueData[] = null;
  private String memo[] = null;
  private Boolean isDealClass = false;

  public PropertyData(Long catCode) {
    if (catCode / 10000000000L == 4L) {
      propertyData = propertyData2;
      tags = tags2;
      valueData = valueData2;
      memo = memo2;
      isDealClass = true;
    } else {
      propertyData = propertyData1;
      tags = tags1;
      valueData = valueData1;
      memo = memo1;
      isDealClass = false;
    }
  }

  private PropertyService createService() {
    HessianProxyFactory factory = new HessianProxyFactory();
    PropertyService service = null;
    try {
      service = (PropertyService) factory.create(PropertyService.class, VALUE_URL);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }// 创建IService接口的实例对象
    return service;
  }

  /**
   * 插入属性数据
   * 
   * @param catId
   * @return
   */
  public String insertPropertyData(Integer catId) {
    PropertyService service = createService();
    int insertCount = 0;

    for (int i = 0; i < propertyData.length; i++) {
      // Result<PropertyDTO> propertyResult = service.queryPropertyByPropertyName(catId,
      // propertyData[i], false);
      // if(null == propertyResult.getModule()){
      PropertyDTO propertyDTO = new PropertyDTO();
      propertyDTO.setCatId(catId);
      propertyDTO.setCreatorId(1);
      propertyDTO.setStatus((short) 1);
      propertyDTO.setFormatTags(tags[i]);
      propertyDTO.setPropertyNameAlias(propertyData[i]);
      propertyDTO.setSortValue((short) (i + 1));
      propertyDTO.setTags(tags[i]);
      propertyDTO.setFeatures(propertyData[i]);
      if (null != memo[i]) {
        propertyDTO.setMemo(memo[i]);
      } else {
        propertyDTO.setMemo(" ");
      }
      Result<Integer> inserResult = service.insertProperty(propertyDTO);
      Integer propertyId = inserResult.getModule();
      if (propertyId > 0) {
        insertCount++;
      }

      ValueData value = new ValueData();
      if(isDealClass){
        String valueResult =
            value.insertValueData(catId, propertyId,
                getValueDataWithDeal(propertyDTO.getPropertyNameAlias()));
        LOGGER.info("插入属性值数量：" + valueResult);
      }else{
        String valueResult =
            value.insertValueData(catId, propertyId,
                getValueDataWithoutDeal(propertyDTO.getPropertyNameAlias()));
        LOGGER.info("插入属性值数量：" + valueResult);
      }
      // }

    }

    return toJsonData(Result.getSuccDataResult(insertCount));
  }

  public String updatePropertyData(String oldName, String newName) {
    PropertyService service = createService();
    PropertyDTO oldPropertyDTO = new PropertyDTO();
    Result<PropertyDTO> propertyResult = service.queryPropertyByPropertyName(4, oldName, false);
    Result<Boolean> updateResult = null;
    if (null != propertyResult) {
      oldPropertyDTO = propertyResult.getModule();
      PropertyDTO newPropertyDTO = new PropertyDTO();
      newPropertyDTO.setPropertyNameAlias(newName);
      newPropertyDTO.setPropertyId(oldPropertyDTO.getPropertyId());
      updateResult = service.updateProperty(newPropertyDTO);
    }
    return toJsonData(updateResult);
  }

  private List<String> getValueDataWithDeal(String name) {

    List<String> value = new ArrayList<>();
    if (name.equals(propertyData[2])) {
      for (int i = 0; i < 2; i++) {
        value.add(valueData[i]);
        value.add(valueData3[0]);
      }
    } else if (name.equals(propertyData[4])) {
      for (int i = 0; i < 69; i++) {
        value.add(valueData[i + 2]);
      }
    } else if (name.equals(propertyData[11]) || name.equals(propertyData[13])) {
      value.add(valueData[74]);

    } else if (name.equals(propertyData[15]) || name.equals(propertyData[16]) || name.equals(propertyData[20]) ) {
      for (int i = 0; i < 2; i++) {
        value.add(valueData[i + 75]);
      }
    } else if (name.equals(propertyData[14])) {
      for (int i = 0; i < 5; i++) {
        value.add(valueData[i + 77]);
      }
    } else if (name.equals(propertyData[17])) {
      for (int i = 0; i < 3; i++) {
        value.add(valueData[i + 82]);
      }
    } else if (name.equals(propertyData[18])) {
      for (int i = 0; i < 4; i++) {
        value.add(valueData[i + 85]);
      }
    } else if (name.equals(propertyData[19])) {
      for (int i = 0; i < 5; i++) {
        value.add(valueData[i + 89]);
      }
    }else if (name.equals(propertyData[21])){
      for (int i = 0; i < 2; i++) {
        value.add(valueData[i + 94]);
      }
    }
    return value;
  }

  private List<String> getValueDataWithoutDeal(String name) {

    List<String> value = new ArrayList<>();
    if (name.equals(propertyData[0])) {
      for (int i = 0; i < 2; i++) {
        value.add(valueData[i]);
      }
    } else if (name.equals(propertyData[6]) || name.equals(propertyData[10])
        || name.equals(propertyData[36]) || name.equals(propertyData[8])
        || name.equals(propertyData[9])) {
      for (int i = 0; i < 69; i++) {
        value.add(valueData[i + 2]);
      }
    } else if (name.equals(propertyData[17])) {
      for (int i = 0; i < 3; i++) {
        value.add(valueData[i + 71]);
      }
    } else if (name.equals(propertyData[22]) || name.equals(propertyData[24])) {
      value.add(valueData[74]);

    } else if (name.equals(propertyData[26]) || name.equals(propertyData[28])
        || name.equals(propertyData[29]) || name.equals(propertyData[39])) {
      for (int i = 0; i < 2; i++) {
        value.add(valueData[i + 75]);
      }
    } else if (name.equals(propertyData[27])) {
      for (int i = 0; i < 5; i++) {
        value.add(valueData[i + 77]);
      }
    } else if (name.equals(propertyData[30])) {
      for (int i = 0; i < 3; i++) {
        value.add(valueData[i + 82]);
      }
    } else if (name.equals(propertyData[32])) {
      for (int i = 0; i < 4; i++) {
        value.add(valueData[i + 85]);
      }
    } else if (name.equals(propertyData[33])) {
      for (int i = 0; i < 5; i++) {
        value.add(valueData[i + 89]);
      }
    } else if (name.equals(propertyData[38])) {
      for (int i = 0; i < 4; i++) {
        value.add(valueData[i + 94]);
      }
    } else if (name.equals(propertyData[40])) {
      for (int i = 0; i < 2; i++) {
        value.add(valueData[i + 98]);
      }
    }
    return value;
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }
}
