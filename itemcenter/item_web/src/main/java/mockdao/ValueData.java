package mockdao;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;

import com.caucho.hessian.client.HessianProxyFactory;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.client.value.ValueService;
import com.xianzaishi.itemcenter.client.value.dto.ValueDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;

/**
 * 导入值数据
 * @author dongpo
 *
 */
public class ValueData {

  private static final String VALUE_URL = "http://10.10.0.12:7009/hessian/valueservice";
  private static final String CATEGORY_URL = "http://10.10.0.12:7009/hessian/stdcategoryservice";
  
  private ValueService createValueService() {
    HessianProxyFactory factory = new HessianProxyFactory();
    ValueService service = null;
    try {
      service = (ValueService) factory.create(ValueService.class, VALUE_URL);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }// 创建IService接口的实例对象
    return service;
  }
  
  private StdCategoryService createCategoryService() {
    HessianProxyFactory factory = new HessianProxyFactory();
    StdCategoryService service = null;
    try {
      service = (StdCategoryService) factory.create(StdCategoryService.class, CATEGORY_URL);
    } catch (MalformedURLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }// 创建IService接口的实例对象
    return service;
  }

  public String insertValueData(Integer catId, Integer propertyId,List<String> valueData) {
    ValueService service = createValueService();
    int count = 0;
    for (int i = 0; i < valueData.size(); i++) {
      ValueDTO valueDTO = new ValueDTO();
      valueDTO.setCatId(catId);
      valueDTO.setPropertyId(propertyId);
      valueDTO.setCreatorId(1);
      valueDTO.setStatus((short) 1);
      valueDTO.setValueData(valueData.get(i));
      valueDTO.setAlias(valueData.get(i));
      valueDTO.setMemo(valueData.get(i));
      valueDTO.setSortValue((short) (i+1));
      Result<Integer> insertResult = service.insertPropertyValue(valueDTO);
      if(insertResult.getModule() > 0){
        count ++;
      }
    }
    return toJsonData(Result.getSuccDataResult(count));
  }
  
  public String updateValueData(String oldValue,String newValue){
    ValueService valueService = createValueService();
    if(null == oldValue){
      return toJsonData(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    Result<Integer> valueResult = valueService.queryValueIdByValueName(oldValue);
    Integer valueId = valueResult.getModule();
    ValueDTO valueDTO = new ValueDTO();
    valueDTO.setValueId(valueId);
    valueDTO.setCreatorId(1);
    valueDTO.setValueData(newValue);
    valueDTO.setAlias(newValue);
    valueDTO.setMemo(newValue);
    Result<Boolean> updateResult = valueService.updatePropertyValue(valueDTO);
    if(!updateResult.getSuccess()){
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "数据更新失败"));
    }
    
    
    return toJsonData(updateResult);
  }
  
  public String addValueDataWithDeal(String value, Integer propertyId){
    StdCategoryService stdCategoryService = createCategoryService();
    Result<List<CategoryDTO>> categoryResult = stdCategoryService.queryCategoriesByCatCode(4L);
    if(!categoryResult.getSuccess()){
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "没有找到相关的类目"));
    }
    List<CategoryDTO> categoryDTOs = categoryResult.getModule();
    if(null == categoryDTOs || categoryDTOs.size() == 0){
      return toJsonData(Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "没有找到相关的类目"));
    }
    String result = null;
    for(CategoryDTO categoryDTO : categoryDTOs){
      result = insertValueData(categoryDTO.getCatId(), propertyId, Arrays.asList(value));
    }
    
    return result;
  }

  /**
   * 将传进来的数据转换为json数据
   * 
   * @param result
   * @return
   */
  public String toJsonData(Object result) {
    String jsonResult = JackSonUtil.getJson(result);
    return jsonResult;
  }

}
