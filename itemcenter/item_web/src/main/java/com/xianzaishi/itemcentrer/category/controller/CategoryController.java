package com.xianzaishi.itemcentrer.category.controller;

import mockdao.StdCategoryData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;

@Controller
@RequestMapping(value = "/category")
public class CategoryController {
  
  @RequestMapping(value = "/addcategory",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String addCategoryData(Integer parentId, String catName){
    if(null == parentId || null == catName){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    
    StdCategoryData stdCategoryData = new StdCategoryData();
    Result<Integer> result = stdCategoryData.insertSubCategoryDate(parentId,catName);
    return JackSonUtil.getJson(result);
    
  }

}
