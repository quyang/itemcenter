package com.xianzaishi.itemcentrer.presale.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import com.xianzaishi.itemcenter.client.presale.ItemPreSaleService;
import com.xianzaishi.itemcenter.client.presale.dto.ItemPreSaleDTO;
import com.xianzaishi.itemcenter.client.presale.query.ItemPreSaleQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * app访问入口，一般为json格式的数据
 *
 * @author jianmo
 * @create 2016-12-23 上午 10:52
 **/
@Controller
@RequestMapping("/itemPreSale")
public class ItemPreSaleController {/*

    private static final Logger LOGGER = Logger.getLogger(ItemPreSaleController.class);
    private static final String ITEMPRESALE_KEY = "itemPreSales";

    private static final String ITEMPRESALE_COUNT_KEY = "itemPreSalesCount";

    private static final String[] RESPONSE_PROPERTIES_FILTER = {"success","resultCode","errorMsg","module","itemPreSalesCount","itemPreSales",
    "title","preSalePrice","prePersonNums","itemId","skuId"
    };


    @Autowired
    private ItemPreSaleService itemPreSaleService;


    *//**
     * 获取预售商品的列表信息
     * @return
     *//*
    @ResponseBody
    @RequestMapping(value = "/queryItemPreSales" ,produces="text/html;charset=UTF-8")
    public String queryItemPreSales(ItemPreSaleQuery query){
        if (null == query) return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
        Map<String,Object> result = new HashMap<>();
        LOGGER.info("查询参数 ==》》 【" + query.toString() + "】"+query.getStart()+"_"+query.getNum());
        //请求后端数据
        Result<List<ItemPreSaleDTO>> resultList = itemPreSaleService.queryItemPreSales(query);
        Result<Integer> resultCount = itemPreSaleService.queryItemPreSalesCount(query);
        result.put(ITEMPRESALE_KEY,resultList.getModule());
        result.put(ITEMPRESALE_COUNT_KEY,resultCount.getModule());
        return JSON.toJSONString(Result.getSuccDataResult(result), new SimplePropertyPreFilter(RESPONSE_PROPERTIES_FILTER));
//        return JackSonUtil.getJson(Result.getSuccDataResult(result));
    }

*/
}
