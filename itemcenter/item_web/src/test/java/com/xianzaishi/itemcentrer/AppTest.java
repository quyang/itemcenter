package com.xianzaishi.itemcentrer;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.caucho.hessian.client.HessianProxyFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.item.dto.ItemDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.SkuTagDetail;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO.SkuTagDefinitionConstants;
import com.xianzaishi.itemcenter.common.JackSonUtil;
//import com.xianzaishi.service.MessageService;



import mockdao.StdCategoryData;

public class AppTest {


  public static void main(String[] args) throws MalformedURLException, ParseException {

    /*
     * <servlet> <!-- 配置 HessianServlet，Servlet的名字随便配置，例如这里配置成ServiceServlet-->
     * <servlet-name>ServiceServlet</servlet-name>
     * <servlet-class>com.caucho.hessian.server.HessianServlet</servlet-class>
     * 
     * <!-- 配置接口的具体实现类 --> <init-param> <param-name>service-class</param-name>
     * <param-value>gacl.hessian.service.impl.ServiceImpl</param-value> </init-param> </servlet>
     * <!-- 映射 HessianServlet的访问URL地址--> <servlet-mapping>
     * <servlet-name>ServiceServlet</servlet-name> <url-pattern>/ServiceServlet</url-pattern>
     * </servlet-mapping>
     */
    // 在服务器端的web.xml文件中配置的HessianServlet映射的访问URL地址
    // String url = "http://localhost/itemcenter/valueservice";
    // HessianProxyFactory factory = new HessianProxyFactory();
    // PropertyService service = (PropertyService) factory.create(PropertyService.class, url);//
    // 创建IService接口的实例对象
    // String[] name = {"apple","peach","banana","grape","orange","papaya","pineapple"};
    // int j = (int) (6 * Math.random());
    // ItemQuery<String> itemQuery = new ItemQuery<>();
    // Result i = service.requestCommodies(itemQuery);
    // System.out.println(i);

    // RequestCommodiesImpl reImpl = new RequestCommodiesImpl();
    // ItemQuery itemQuery = new ItemQuery();
    // List<Long> itemIds = new ArrayList<>();
    // itemIds.add(1L);
    // itemIds.add(2L);

    // itemQuery.setItemIds(itemIds);
    // itemQuery.setStartPrice(0L);
    // itemQuery.setEndPrice(100L);
    // itemQuery.setIsReturnAll(false);
    // itemQuery.setIsItemSku(true);
    // itemQuery.setItemTitle("test");
    // Result result = service.requestCommodies(itemQuery);
    // Result result = service.requestDetailCommodity(1l);
    // System.out.println(result);
    // Boolean id = insertData(service);
    // List<Long> catIds = new ArrayList<>();
    // catIds.add(1L);
    // catIds.add(2L);
    // Integer catId = 4;
    // BasePropertyQuery propertyQuery = new BasePropertyQuery();
    // propertyQuery.setPropertyId(catId);
    // @SuppressWarnings("rawtypes")
    // Result result = service.requestBaseProperty(propertyQuery);
    // String jsonString = JackSonUtil.getJson(result);

    // System.out.println(jsonString);
    // PropertyData propertyData = new PropertyData();
    // Integer catId = 4;
    // propertyData.insertPropertyData(catId);
    // StdCategoryData stdCategoryData = new StdCategoryData();
    // stdCategoryData.insertStdCategory();

    // ItemCommoditySkuDTO dto = new ItemCommoditySkuDTO();
    // dto.setFeature("relatedItems&[酸甜#30002]");
    // System.out.println(JackSonUtil.getJson(dto.queryRelatedItems()));
//    Random r1 = new Random(new Date().getTime() + new Random().nextInt());
//    Random r2 = new Random(new Date().getTime() + new Random().nextInt());
//
//    for (int i = 0; i < 100; i++) {
//      System.out.println(r1.nextInt(10) + ", " + r2.nextInt(10));
//    }
//    StringBuffer stringBuffer = new StringBuffer();
//    String string  = "hello world";
//    stringBuffer.append(string);
//    System.out.println(replaceSpace(stringBuffer));
//    String stepId = "1\\2\\3\\4\\5\\6\\7\\8";
//    String[] stepIds = stepId.split("\\\\");
//    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//    String today = sdf.format(new Date().getTime() - 86400000);
//    System.out.println(today);
//    Integer tags = 511;
//    for(int i = 0;i<=256;i=(i<<1)){
//      if((tags & i) == i){
//        if(i == 0x1){
//          System.out.println("线上满90减40元");
//        }else if(i == 0x2){
//          System.out.println("线下满40减20元");
//        }else if(i == 0x4){
//          System.out.println("0x4");
//        }else if(i == 0x10){
//          System.out.println("线下满90减40元");
//        }else if(i == 0x20){
//          System.out.println("线上满40减20元");
//        }else if(i == 0x40){
//          System.out.println("0x40");
//        }else if(i == 0x80){
//          System.out.println("0x80");
//        }else if(i == 0x100){
//          System.out.println("0x100");
//        }
//      }
//    }
//    List<Integer> aList = Lists.newArrayList();
//    aList.add(3);
//    aList.add(3);
//    aList.add(3);
//    aList.add(3);
//    aList.add(3);
//    for(int i = 0; i < aList.size();i++){
//      Integer itemDTO = aList.get(i);
//      ++ itemDTO;
//    }
//    Map<String, String> map = Maps.newHashMap();
//    map.put("tg", "[{\"tagId\":\"1\",\"channel\":1,\"tagName\":\"线上满90减40元\",\"begin\":null,\"end\":null,\"tagRule\":null}]");
//    String skuTagDetailListJson = map.get("tg");
//    List<SkuTagDetail> skuTagDetails = JackSonUtil.jsonToList(skuTagDetailListJson, SkuTagDetail.class);
//    System.out.println(JackSonUtil.getJson(aList));
//    String url = "http://localhost:9100/hessian/messageService";
//    HessianProxyFactory factory = new HessianProxyFactory();
//    MessageService service = (MessageService) factory.create(MessageService.class, url);
//    Map<String, String> map = Maps.newHashMap();
//    String phoneString = "13777380140";
//    System.out.println(JSONObject.toJSONString(service.sendSmsMessage(phoneString, "SMS_56555434", map)));
//  }
//
//    public static String replaceSpace(StringBuffer str) {
//      
//      if(null == str){
//        return null;
//      }
//      
//      int count = 0;
//      for (int i = 0; i < str.length(); i++) {
//        if (str.charAt(i) == ' ') {
//          count += 1;
//        }
//      }
//      int oldLength = str.length();
//      int newLength = oldLength + count * 2;
//      str.setLength(newLength);
//      int j = newLength - 1;
//      for (int i = oldLength - 1; i >= 0; i--) {
//        if(str.charAt(i) != ' '){
//          str.setCharAt(j, str.charAt(i));
//          System.out.println(str.charAt(i));
//        }else{
//          str.setCharAt(j, '0');
//          str.setCharAt(--j, '2');
//          str.setCharAt(--j, '%');
//        }
//        j--;
//      }
//      return str.toString();
//
//    }
  // @SuppressWarnings("unused")
  // private static Boolean insertData(PropertyService service) throws ParseException {
  // // TODO Auto-generated method stub
  // // MockCommodiesData mockData = new MockCommodiesData();
  // // int random = (int) (10 * Math.random());
  // // ItemDto itemDto = mockData.requestItemAll(8); // 请求商品，random用于个性化处理
  // //
  // // ItemSkuDto itemSkuDto = mockData.requestItemSku(8);
  // // List<ItemSkuDto> itemSkuList = new ArrayList<>();
  // // itemSkuList.add(itemSkuDto);
  // // itemDto.setItemSkus(itemSkuList);
  // PropertyDTO propertyDto = new PropertyDTO();
  // propertyDto.setPropertyId(1);
  // propertyDto.setPropertyNameAlias("品牌");
  // propertyDto.setStatus((short) 2);
  // // propertyDto.setMemo("每个商品有不同的属性、不同的属性值。类目下包括多个类目属性，一个属性包括多个属性值。");
  // propertyDto.setCreatorId(22);
  // propertyDto.setFormatTags(11001011);
  // return service.updateProperty(propertyDto);
    StringBuilder sBuilder = new StringBuilder();
    System.out.println(sBuilder.toString());
   }
}
