package com.xianzaishi.itemcentrer.price;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.dal.itemsku.dao.ItemCommoditySkuDOMapper;
import com.xianzaishi.itemcenter.dal.itemsku.dao.ItemSkuRelationDOMapper;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemCommoditySkuDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuRelationDO;

@Service("getPriceInfo")
public class GetPriceInfo {

  private static final Logger logger = Logger.getLogger(GetPriceInfo.class);

  @Autowired
  private ItemSkuRelationDOMapper itemSkuRelationDOMapper;

  @Autowired
  private ItemCommoditySkuDOMapper itemCommoditySkuDOMapper;
  
  public void getPrice(){
    Map<Long,String> propertyM = Maps.newHashMap();
    Map<Long,Integer> bpriceM = Maps.newHashMap();
    Map<Long,Integer> spriceM = Maps.newHashMap();
    Map<Long,String> titleM = Maps.newHashMap();
    List<Long> idLists = Lists.newArrayList();
    for(int i=10001;i<50000;i++){
      List<Long> skuIdLists = Lists.newArrayList();
      for(;i%100!=0;i++){
        skuIdLists.add(Long.valueOf(i));
      }
      SkuQuery query = SkuQuery.querySkuBySkuIds(skuIdLists);
      query.setPageNum(0);
      query.setPageSize(100);
      List<ItemCommoditySkuDO> SkuDOList = itemCommoditySkuDOMapper.query(query);
      
      for(ItemCommoditySkuDO sku:SkuDOList){
        idLists.add(sku.getSkuId());
        propertyM.put(sku.getSkuId(), sku.getProperty());
        Integer price = sku.getDiscountUnitPrice();
        if(price == null || price <= 0){
          price = sku.getUnitPrice();
        }
        spriceM.put(sku.getSkuId(), price);
        titleM.put(sku.getSkuId(), sku.getTitle());
      }
      
      SkuRelationQuery relationQ = SkuRelationQuery.getQueryByCskuIds(skuIdLists, (short)0);
      relationQ.setPageNum(0);
      relationQ.setPageSize(100);
      List<ItemSkuRelationDO> relationList = itemSkuRelationDOMapper.queryList(relationQ);
      if(skuIdLists.contains(30127L)){
        System.out.println("test query");
      }
      for(ItemSkuRelationDO skuRelation:relationList){
        bpriceM.put(skuRelation.getCskuId(), skuRelation.getbPrice());
        if(skuRelation.getCskuId().longValue() == 30127L){
          int skuPrice = skuRelation.getbPrice();
          System.out.println("test query skuRelation:"+skuRelation.getbPrice());
        }
      }
      logger.error("Get price info,get "+i+" count item");
    }
    String result = "";
    for(Long skuId:idLists){
      if(skuId.equals(30127)){
        System.out.println("test start");
      }
      String property = propertyM.get(skuId);
      Integer buyPrice = bpriceM.get(skuId);
      if(null == buyPrice){
        buyPrice = 0;
      }
      Integer salePrice = spriceM.get(skuId);
      if(null == salePrice){
        salePrice = 0;
      }
      if(salePrice >= 999999){
        continue;
      }
      int buyTax = getBuyTax(property);
      int saleTax = getSaleTax(property);
      String title = titleM.get(skuId);
      title = title.replaceAll("'", "");
      int buyPriceNoTax = new BigDecimal(buyPrice).multiply(new BigDecimal(10000))
          .divide(new BigDecimal(10000+buyTax),1, BigDecimal.ROUND_HALF_EVEN).intValue();
      result = result + skuId + "###";
      result = result + buyTax + "###";
      result = result + saleTax + "###";
      result = result + buyPrice + "###";
      result = result + buyPriceNoTax + "###";
      result = result + salePrice + "###";
      result = result + title + "\r\n";
    }
    logger.error(result);
  }
  
  /**
   * 进项税 key=20
   * @param property
   * @return
   */
  private int getBuyTax(String property){
    if(property.indexOf("21&") < 0){
      return 0;
    }
    String tmp = property.substring(property.indexOf("21&")+3);
    if(tmp.indexOf(";") > 0){
      tmp = tmp.substring(0, tmp.indexOf(";"));
    }
    if(tmp.indexOf("\r\n") > 0){
      tmp = tmp.substring(0, tmp.indexOf("\r\n"));
    }
    int tax = Integer.valueOf(tmp);
    if(tax < 100 && tax > 0){
      tax = 100*tax;
    }
    return tax;
  }
  
  /**
   * 销项税 key=20
   * @param property
   * @return
   */
  private int getSaleTax(String property){
    if(property.indexOf("20&") < 0){
      return 0;
    }
    String tmp = property.substring(property.indexOf("20&")+3);
    if(tmp.indexOf(";") > 0){
      tmp = tmp.substring(0, tmp.indexOf(";"));
    }
    if(tmp.indexOf("\r\n") > 0){
      tmp = tmp.substring(0, tmp.indexOf("\r\n"));
    }
    int tax = Integer.valueOf(tmp);
    if(tax < 100 && tax > 0){
      tax = 100*tax;
    }
    return tax;
  }
}
