package mockdao;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.apache.log4j.Logger;

import com.caucho.hessian.client.HessianProxyFactory;
import com.xianzaishi.itemcenter.client.stdcategory.StdCategoryService;
import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 导入类目数据
 * @author dongpo
 *
 */
public class StdCategoryData {

  private static Logger LOGGER = Logger.getLogger(StdCategoryData.class);

  private static final String[] catName1 = {"非销售"};

  private static final String[] catName2 = {"原物料"};

  private static final String[] catName3 = {"加工原物料"};

  private static final String[] catName4 = {"包材", "配料"};
  private static final Long[] catCode1 = {5L};
  private static final Long[] catCode2 = {5001L};
  private static final Long[] catCode3 = {5001001L};
  private static final Long[] catCode4 = {50010010001L, 50010010002L};
  private static final String[] features = {"l=1", "l=2", "l=3", "l=4"};
  private static final String VALUE_URL = "http://localhost/itemcenter/hessian/stdcategoryservice";

  private StdCategoryService createService() {
    HessianProxyFactory factory = new HessianProxyFactory();
    StdCategoryService service = null;
    try {
      service = (StdCategoryService) factory.create(StdCategoryService.class, VALUE_URL);
    } catch (MalformedURLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }// 创建IService接口的实例对象
    return service;
  }

  public void insertStdCategory() {

    for(int i = 0;i < catCode1.length;i++){
      CategoryDTO categoryDto = new CategoryDTO();
      categoryDto.setCatType((short) 1);
      categoryDto.setCreatorId(1);
      categoryDto.setStatus((short) 1);
      Integer parentId = 0;
      categoryDto.setCatCode(catCode1[i]);
      categoryDto.setCatName(catName1[i]);
      categoryDto.setFeatures(features[i]);
      Short isLeaf = 0;
      categoryDto.setLeaf(isLeaf);
      categoryDto.setMemo(catName1[i]);
      categoryDto.setParentId(parentId);
      categoryDto.setSortValue((short) 0);
      depthCategory(categoryDto);
    }
    insertProperty();
  }

  public void insertProperty() {
    StdCategoryService service = createService();
    for (String catName : catName4) {
      Result<CategoryDTO> result = service.queryCategoryByName(catName);
      CategoryDTO categoryDTO = result.getModule();
      if (null != categoryDTO && categoryDTO.getLeaf() == 1) {
        PropertyData propertyData = new PropertyData(categoryDTO.getCatCode());
        propertyData.insertPropertyData(categoryDTO.getCatId());
      }
    }
  }

  /**
   * 获取子节点
   * 
   * @param node
   * @return
   */
  private List<CategoryDTO> getChildrenCategory(CategoryDTO node) {
    List<CategoryDTO> categoryDTOs = new ArrayList<>();
    Long catCode = node.getCatCode();
    List<String> catNameList = new ArrayList<>();
    List<Long> catCodeList = new ArrayList<>();
    List<String> featuresList = new ArrayList<>();
    for (int k = 0; k < catCode1.length; k++) {
      if (catCode.equals(catCode1[k])) {
        for (int i = 0; i < catName2.length; i++) {
          Long xCode = catCode2[i] / 1000L;
          if (xCode.equals(catCode)) {
            catNameList.add(catName2[i]);
            catCodeList.add(catCode2[i]);
            featuresList.add(features[1]);
          }
        }
      } else {
        for (int i = 0; i < catName3.length; i++) {
          if (i < catName2.length && catCode.equals(catCode2[i])) {
            for (int j = 0; j < catCode3.length; j++) {
              Long xcode = catCode3[j] / 1000L;
              if (xcode.equals(catCode)) {

                catCodeList.add(catCode3[j]);
                catNameList.add(catName3[j]);
                featuresList.add(features[2]);
              }
            }
          } else if (catCode.equals(catCode3[i])) {
            for (int j = 0; j < catCode4.length; j++) {
              Long xcode = catCode4[j] / 10000L;
              if (xcode.equals(catCode)) {
                catCodeList.add(catCode4[j]);
                catNameList.add(catName4[j]);
                featuresList.add(features[3]);
              }
            }
          }
        }
      }
    }

    for (int i = 0; i < catNameList.size(); i++) {
      CategoryDTO categoryDto = new CategoryDTO();
      categoryDto.setCatType((short) 1);
      categoryDto.setCreatorId(1);
      categoryDto.setStatus((short) 1);
      categoryDto.setCatCode(catCodeList.get(i));
      categoryDto.setCatName(catNameList.get(i));
      categoryDto.setFeatures(featuresList.get(i));
      Short isLeaf = 0;
      if (featuresList.get(i).equals("l=4")) {
        isLeaf = (short) 1;
      }
      categoryDto.setLeaf(isLeaf);
      categoryDto.setMemo(catNameList.get(i));
      categoryDto.setParentId(node.getCatId());
      categoryDto.setSortValue((short) i);
      categoryDTOs.add(categoryDto);
    }

    return categoryDTOs;
  }


  /**
   * 深度遍历
   * 
   * @param node
   */
  public void depthCategory(CategoryDTO parentNode) {
    StdCategoryService service = createService();
    Stack<CategoryDTO> nodeStack = new Stack<CategoryDTO>();
    nodeStack.add(parentNode);
    while (!nodeStack.isEmpty()) {
      parentNode = nodeStack.pop();
      Result<Integer> result = null;
      Result<CategoryDTO> categoryResult = service.queryCategoryByName(parentNode.getCatName());
      CategoryDTO categoryDTO = categoryResult.getModule();
      if (null == categoryDTO.getCatId()
          || (null != categoryDTO.getCatCode() && !categoryDTO.getCatCode().equals(
              parentNode.getCatCode()))) {
        result = service.insertCategory(parentNode);
        if (null != result) {
          parentNode.setCatId(result.getModule());
          LOGGER.info(result.getModule());
        }
      } else {
        parentNode.setCatId(categoryDTO.getCatId());
      }
      List<CategoryDTO> children = getChildrenCategory(parentNode);// 获得节点的子节点
      if (children != null && !children.isEmpty()) {
        for (CategoryDTO child : children) {
          nodeStack.push(child);
        }
      }
    }
  }


}
