package com.xianzaishi.itemcentrer;

import mockdao.StdCategoryData;
import mockdao.ValueData;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;


/**
 * 导入类目、属性、值数据接口
 * @author dongpo
 *
 */
@Controller
@RequestMapping(value = "/createdata")
public class CreateCategoryPropertyValueData {
  
  /**
   * 返回按查询条件query查询的结果
   * 
   * @param itemIds
   * @return
   */
  @RequestMapping(value = "/catpropertyvaluedata",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String createCatPropertyValueData() {
    StdCategoryData stdCategoryData = new StdCategoryData();
    stdCategoryData.insertStdCategory();
    return JackSonUtil.getJson("success!");
  }
  
  /**
   * 更新属性值名
   * @param oldValue
   * @param newValue
   * @return
   */
  @RequestMapping(value = "/updatevalue",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String updateValueData(String oldValue,String newValue){
    if(null == oldValue || null == newValue){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    ValueData valueData = new ValueData();
    String result = valueData.updateValueData(oldValue, newValue);
        
    return result;
  }
  
  @RequestMapping(value = "/addvalue",produces="text/html;charset=UTF-8")
  @ResponseBody
  public String addValueData(String value, Integer propertyId){
    if(null == value || null == propertyId){
      return JackSonUtil.getJson(Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数为空"));
    }
    
    ValueData valueData = new ValueData();
    String result = valueData.addValueDataWithDeal(value,propertyId);
    return result;
    
  }

}
