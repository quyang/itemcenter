package com.xianzaishi.itemcentrer.timerservice;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO.SkuSaleUnitConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuRelationDTO.RelationTypeConstants;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationBackQuery;
import com.xianzaishi.itemcenter.component.item.CommodityServiceImpl;
import com.xianzaishi.itemcenter.dal.item.dao.ItemDOMapper;
import com.xianzaishi.itemcenter.dal.item.dataobject.ItemDO;
import com.xianzaishi.itemcenter.dal.itemsku.dao.ItemCommoditySkuDOMapper;
import com.xianzaishi.itemcenter.dal.itemsku.dao.ItemSkuRelationDOMapper;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemCommoditySkuDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuRelationDO;

/**
 * 进售价价格变化定时任务,只支持天为单位的价格生效时间，小时分秒都被忽略
 * 
 * @author zhancang
 */
@Service("skuRelationTimer")
public class SkuRelationTimer {

  private static final Logger logger = Logger.getLogger(CommodityServiceImpl.class);

  @Autowired
  private ItemSkuRelationDOMapper itemSkuRelationDOMapper;

  @Autowired
  private ItemCommoditySkuDOMapper itemCommoditySkuDOMapper;

  @Autowired
  private ItemDOMapper itemDOMapper;

  public void updateSkuPrice(boolean updateAll, boolean isTest) {
    logger.error("Timer task updateSkuPrice start");
    SkuRelationBackQuery query = new SkuRelationBackQuery();
    if (!updateAll) {
      query.setSaleStartRangeEnd(this.getSaleStartRangeEnd());
      query.setSaleEndRangeBegin(this.getSaleEndRangeStart());
    }
    List<ItemSkuRelationDO> resultList = Lists.newArrayList();
    for (int i = 0;; i++) {
      query.setPageNum(i);
      query.setPageSize(50);
      List<ItemSkuRelationDO> tmp = itemSkuRelationDOMapper.backQueryList(query);
      if (CollectionUtils.isEmpty(tmp)) {
        break;
      } else {
        resultList.addAll(tmp);
      }
    }

    // 优惠进售价关系
    Map<Long, ItemSkuRelationDO> discountPriceM = Maps.newHashMap();
    // 普通进售价关系
    Map<Long, ItemSkuRelationDO> priceM = Maps.newHashMap();
    // 合并db中符合时间区间的sku列表
    List<Long> cskuIdList = Lists.newArrayList();

    // 记录需要更新sku的id列表
    List<Long> updateCskuIdList = Lists.newArrayList();
    // 记录哪些sku需要更新
    Map<Long, ItemCommoditySkuDO> updatePriceM = Maps.newHashMap();

    // 价格变更前记录
    Map<Long, Integer> oldPriceM = Maps.newHashMap();
    // 变更价格后的记录
    Map<Long, Integer> newPriceM = Maps.newHashMap();

    if (CollectionUtils.isEmpty(resultList)) {
      return;
    }
    for (ItemSkuRelationDO skurelation : resultList) {
      Long cskuId = skurelation.getCskuId();
      if (null != skurelation.getPrice() && skurelation.getPrice() > 0) {
        if (RelationTypeConstants.RELATION_TYPE_NORMAL.equals(skurelation.getRelationType())) {
          priceM.put(cskuId, skurelation);
          if (!cskuIdList.contains(cskuId)) {
            cskuIdList.add(cskuId);
          }
        } else if (RelationTypeConstants.RELATION_TYPE_DISCOUNT.equals(skurelation
            .getRelationType())) {
          discountPriceM.put(cskuId, skurelation);
          if (!cskuIdList.contains(cskuId)) {
            cskuIdList.add(cskuId);
          }
        }
      }
    }

    List<List<Long>> splitList = Lists.newArrayList();
    List<Long> tmpSubList = Lists.newArrayList();
    // 切割数据，防止取数据过多
    for (Long skuId : cskuIdList) {
      if (tmpSubList.size() < 20) {
        tmpSubList.add(skuId);
        continue;
      } else {
        splitList.add(tmpSubList);
        tmpSubList = Lists.newArrayList();
      }
    }
    if (CollectionUtils.isNotEmpty(tmpSubList)) {
      splitList.add(tmpSubList);// 最后一条记录也要加入
    }

    // 获取待更新的商品价格
    for (List<Long> idSubList : splitList) {
      List<ItemCommoditySkuDO> skuList =
          itemCommoditySkuDOMapper.query(SkuQuery.querySkuBySkuIds(idSubList));
      if (CollectionUtils.isNotEmpty(skuList)) {
        for (ItemCommoditySkuDO sku : skuList) {
          int skuUnPrice = sku.getUnitPrice();

          ItemSkuRelationDO disCountRelation = discountPriceM.get(sku.getSkuId());
          ItemSkuRelationDO relation = priceM.get(sku.getSkuId());
          //TODO  没有时间处理优惠情况，先注释再说
//          if (null != disCountRelation) {
//            int skuDiscountRelationPrice = disCountRelation.getPrice();
//            if (skuDiscountRelationPrice > 0 && skuDiscountRelationPrice != skuUnPrice) {
////            if (SkuSaleUnitConstants.NATURAL.equals(sku.querySaleUnitType())) {
//              if(sku.getSkuId().equals(10664L)){
//                int i = 1;
//                System.out.println(i);
//              }
//              updateCskuIdList.add(sku.getSkuId());
//              sku.setPrice(skuDiscountRelationPrice);
//              sku.setDiscountPrice(skuDiscountRelationPrice);
//              sku.setUnitPrice(skuDiscountRelationPrice);
//              if (SkuSaleUnitConstants.NATURAL.equals(sku.querySaleUnitType())) {
//                // 非加工类，并且属于自然不可分割称重类的
//                int rangeNum = sku.getSaleRangeNum();
//                if (rangeNum > 0) {
//                  int price = new BigDecimal(rangeNum).multiply(new BigDecimal(sku.getPrice()))
//                      .divide(new BigDecimal(1000)).intValue();
//                  sku.setPrice(price);
//                  sku.setDiscountPrice(price);
//                }
//              }
//              updatePriceM.put(sku.getSkuId(), sku);
//              oldPriceM.put(sku.getSkuId(), skuUnPrice);
//              newPriceM.put(sku.getSkuId(), skuDiscountRelationPrice);
//              continue;
//            }
//          }
          if (null != relation) {
            int skuRelationPrice = relation.getPrice();
            if (skuRelationPrice > 0 && skuRelationPrice != skuUnPrice) {
              if(sku.getSkuId().equals(10664L)){
                int i = 1;
                System.out.println(i);
              }
              updateCskuIdList.add(sku.getSkuId());
              
              sku.setPrice(skuRelationPrice);
              sku.setDiscountPrice(skuRelationPrice);
              sku.setUnitPrice(skuRelationPrice);
              if (SkuSaleUnitConstants.NATURAL.equals(sku.querySaleUnitType())) {
                // 非加工类，并且属于自然不可分割称重类的
                int rangeNum = sku.getSaleRangeNum();
                if (rangeNum > 0) {
                  int price = new BigDecimal(rangeNum).multiply(new BigDecimal(sku.getPrice()))
                      .divide(new BigDecimal(1000)).intValue();
                  sku.setPrice(price);
                  sku.setDiscountPrice(price);
                }
              }
              updatePriceM.put(sku.getSkuId(), sku);
              oldPriceM.put(sku.getSkuId(), skuUnPrice);
              newPriceM.put(sku.getSkuId(), skuRelationPrice);
              continue;
            }
          }
        }
      }
      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    for (ItemCommoditySkuDO commodity : updatePriceM.values()) {
      int count = 0;
      if (!isTest) {
        count = itemCommoditySkuDOMapper.update(commodity);
      }
      ItemDO item = itemDOMapper.queryItem(commodity.getItemId());
      item.setPrice(commodity.getPrice());
      item.setDiscountPrice(commodity.getPrice());
      if (!isTest) {
        int updatItemCount = itemDOMapper.updateItem(item);
        if (count == 1 && updatItemCount == 1) {
          logger.error("Update price success,skuId:" + commodity.getSkuId() + " oldpric:"
              + oldPriceM.get(commodity.getSkuId()) + " newpric:" + commodity.getPrice());
        } else {
          logger.error("Update price failed,skuId:" + commodity.getSkuId() + " oldpric:"
              + oldPriceM.get(commodity.getSkuId()) + " newpric:" + commodity.getPrice()
              + " skuupdate success:" + (count == 1) + " itemupdate success:"
              + (updatItemCount == 1));
        }
      } else {
        logger.error("Update price success,skuId:" + commodity.getSkuId() + " oldpric:"
            + oldPriceM.get(commodity.getSkuId()) + " newpric:" + commodity.getPrice());
      }

      try {
        Thread.sleep(10);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    logger.error("Timer task updateSkuPrice end");
  }

  /**
   * 获取今天0辰0点，定时任务晚上凌晨4点执行
   * 
   * @return
   */
  private Date getSaleStartRangeBegin() {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.add(Calendar.DAY_OF_MONTH, 0);
    return cal.getTime();
  }

  /**
   * 销售开始时间区间段在[以前某个时间------定时任务执行当天24点00分]
   * 
   * @return
   */
  private Date getSaleStartRangeEnd() {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.set(Calendar.HOUR_OF_DAY, 23);
    cal.set(Calendar.MINUTE, 59);
    cal.set(Calendar.SECOND, 59);
    cal.set(Calendar.MILLISECOND, 0);
    cal.add(Calendar.DAY_OF_MONTH, 0);
    return cal.getTime();
  }

  /**
   * 销售结束时间[定时任务执行时间当天23点59分-------以后无穷时间]
   * 
   * @return
   */
  private Date getSaleEndRangeStart() {
    Calendar cal = Calendar.getInstance();
    cal.setTime(new Date());
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    cal.add(Calendar.DAY_OF_MONTH, 0);
    return cal.getTime();
  }

  public static void main(String args[]) {
    SkuRelationTimer t = new SkuRelationTimer();
    System.out.println(t.getSaleStartRangeEnd());
  }
}
