package com.xianzaishi.itemcentrer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcentrer.price.GetPriceInfo;
import com.xianzaishi.itemcentrer.timerservice.SkuRelationTimer;

@Controller
@RequestMapping(value = "/itemtimer")
public class ItemTimerController {

  @Autowired
  private SkuRelationTimer skuRelationTimer;
  
  @Autowired
  private GetPriceInfo getPriceInfo;

  /**
   * 返回按查询条件query查询的结果
   * 
   * @param itemIds
   * @return
   */
  @RequestMapping(value = "/setprice", produces = "text/html;charset=UTF-8",
      method = RequestMethod.GET)
  @ResponseBody
  public String requestByQuery(boolean updateall,boolean istest) {
    skuRelationTimer.updateSkuPrice(updateall, istest);
    return JackSonUtil.getJson("SUCCESS");
  }
  
  /**
   * 返回按查询条件query查询的结果
   * 
   * @param itemIds
   * @return
   */
  @RequestMapping(value = "/getpriceinfo", produces = "text/html;charset=UTF-8",
      method = RequestMethod.GET)
  @ResponseBody
  public String requestPriceInfo() {
    getPriceInfo.getPrice();
    return JackSonUtil.getJson("SUCCESS");
  }
}
