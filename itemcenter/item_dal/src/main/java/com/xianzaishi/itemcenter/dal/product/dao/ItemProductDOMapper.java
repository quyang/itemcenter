package com.xianzaishi.itemcenter.dal.product.dao;

import java.util.List;

import com.xianzaishi.itemcenter.dal.product.dataobject.ItemProductDO;

public interface ItemProductDOMapper {

  /**
   * 插入后台商品对象
   * 
   * @param record
   * @return
   */
  int insert(ItemProductDO itemProductDO);

  /**
   * 更新后台对象
   * 
   * @param record
   * @return
   */
  int update(ItemProductDO itemProductDO);

  /**
   * 查询后台对象
   * 
   * @param productId
   * @return
   */
  ItemProductDO query(Long productId);
  
  /**
   * 根据id批量查询数据
   * 
   * @param productId
   * @return
   */
  List<ItemProductDO> batchSelectByIds(List<Long> productId);

}
