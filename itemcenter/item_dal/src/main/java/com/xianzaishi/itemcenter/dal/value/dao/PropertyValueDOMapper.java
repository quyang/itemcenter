package com.xianzaishi.itemcenter.dal.value.dao;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.dal.value.dataobject.PropertyValueDO;

public interface PropertyValueDOMapper {
  
  PropertyValueDO selectByPrimaryKey(@Param("propertyId")Integer propertyId,@Param("valueId")Integer valueId);
  
  int insertPropertyValue(PropertyValueDO record);
}