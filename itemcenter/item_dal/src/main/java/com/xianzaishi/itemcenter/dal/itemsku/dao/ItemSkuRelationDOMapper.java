package com.xianzaishi.itemcenter.dal.itemsku.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationBackQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationQuery;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuRelationDO;

public interface ItemSkuRelationDOMapper {

  /**
   * 插入一个sku关联关系
   * 
   * @param record
   * @return
   */
  int insert(ItemSkuRelationDO record);

  /**
   * 更新一个sku关联关系
   * 
   * @param record
   * @return
   */
  int update(ItemSkuRelationDO record);

  /**
   * 通过前台销售sku查询sku关联关系
   * 
   * @param cskuId
   * @return
   */
  List<ItemSkuRelationDO> queryList(@Param("query")SkuRelationQuery query);

  /**
   * 提供后台查询接口
   * 
   * @param cskuId
   * @return
   */
  List<ItemSkuRelationDO> backQueryList(@Param("query")SkuRelationBackQuery query);
  
  /**
   * 通过前台销售sku查询sku关联关系个数
   * @param query
   * @return
   */
  int queryCount (@Param("query")SkuRelationQuery query);
  
  /**
   * 通过前台销售sku查询sku关联关系
   * 
   * @param cskuId
   * @return
   */
  ItemSkuRelationDO query(@Param("id") Long id);
}
