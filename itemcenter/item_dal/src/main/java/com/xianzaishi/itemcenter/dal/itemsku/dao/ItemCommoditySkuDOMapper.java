package com.xianzaishi.itemcenter.dal.itemsku.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemCommoditySkuDO;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuTagsDO;

public interface ItemCommoditySkuDOMapper {

  /**
   * 插入一个前台sku对象
   */
  int insert(ItemCommoditySkuDO itemCommoditySku);

  /**
   * 更新一个前台sku信息
   * @param record
   * @return
   */
  int update(ItemCommoditySkuDO itemCommoditySku);
  
  /**
   * 查询前台sku对象列表
   * @param query
   * @return
   */
  List<ItemCommoditySkuDO> query(@Param("query")SkuQuery query, @Param("inOrderIds")String inOrderIds);
  
  /**
   * 查询前台sku对象列表个数
   * @param query
   * @return
   */
  int queryCount (@Param("query") SkuQuery query);
  
  /**
   * 查询商品标信息
   * @param skuIds
   * @return
   */
  List<ItemSkuTagsDO> queryItemTagsDetail(List<Long> skuIds);

}
