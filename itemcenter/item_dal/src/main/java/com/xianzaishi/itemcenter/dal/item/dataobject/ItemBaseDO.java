package com.xianzaishi.itemcenter.dal.item.dataobject;

import java.io.Serializable;

/**
 * 部分商品字段
 * @author dongpo
 *
 */
public class ItemBaseDO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -5075199859456630715L;

  /**
   * 商品id
   */
  private Long itemId;

  /**
   * 单个商品图片地址
   */
  private String pic;

  /**
   * 商品标题
   */
  private String title;

  /**
   * 商品副标题
   */
  private String subtitle;

  /**
   * 原价
   */
  private Integer price;

  /**
   * 商品折扣价
   */
  private Integer discountPrice;

  /**
   * sku id列表
   */
  private String sku;

  /**
   * 商品特征，包含类目标准属性，扩展结构和商品特征值，sku id列表
   */
  private String feature;

  /**
   * 商品库存
   */
  private Integer inventory;
  
  /**
   * 后台类目id
   */
  private Integer categoryId;
  
  /**
   * 前台2级类目id，暂时使用后台3级类目代替
   */
  private Integer cmCat2;

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    if (itemId != null && itemId < 0) {
      throw new IllegalArgumentException("itemId must be greater than 0");
    }
    this.itemId = itemId;
  }


  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public String getSubtitle() {
    return subtitle;
  }

  public void setSubtitle(String subtitle) {
    this.subtitle = subtitle == null ? null : subtitle.trim();
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    if (price != null && price < 0) {
      throw new IllegalArgumentException("price must be greater than 0");
    }
    this.price = price;
  }

  public Integer getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(Integer discountPrice) {
    if (discountPrice != null && discountPrice < 0) {
      throw new IllegalArgumentException("discount_price must be greater than 0");
    }
    this.discountPrice = discountPrice;
  }

  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku == null ? null : sku.trim();
  }

  public String getFeature() {
    return feature;
  }

  public void setFeature(String feature) {
    this.feature = feature == null ? null : feature.trim();
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String picUrl) {
    this.pic = picUrl == null ? null : picUrl.trim();
  }

  public Integer getInventory() {
    return inventory;
  }

  public void setInventory(Integer inventory) {
    this.inventory = inventory;
  }

  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public Integer getCmCat2() {
    return cmCat2;
  }

  public void setCmCat2(Integer cmCat2) {
    this.cmCat2 = cmCat2;
  }
  
}
