package com.xianzaishi.itemcenter.dal.stdcategory.query;

import com.xianzaishi.itemcenter.client.stdcategory.query.StdCategoryQuery;

/**
 * 类目查询条件
 * @author dongpo
 *
 */
public class StdCatQuery extends StdCategoryQuery {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -1975515429052534122L;

}
