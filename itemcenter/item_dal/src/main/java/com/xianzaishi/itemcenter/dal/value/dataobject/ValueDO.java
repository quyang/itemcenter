package com.xianzaishi.itemcenter.dal.value.dataobject;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 属性值字段
 * @author dongpo
 *
 */
public class ValueDO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = 4717505885723272441L;

  /**
   * 类目id
   */
  private Integer catId;

  /**
   * 属性id
   */
  private Integer propertyId;

  /**
   * 属性值id
   */
  private Integer valueId;

  /**
   * 排序值
   */
  private Short sortValue;

  /**
   * 属性值别名
   */
  private String alias;

  /**
   * 属性值状态码
   */
  private Short status;

  /**
   * 创建人
   */
  private Integer creatorId;

  /**
   * 属性值描述
   */
  private String memo;

  /**
   * 属性值创建时间
   */
  private Date gmtCreate;

  /**
   * 属性值修改时间
   */
  private Date gmtModified;

  /**
   * 属性值特征
   */
  private String features;

  public Integer getCatId() {
    return catId;
  }

  public void setCatId(Integer catId) {
    this.catId = catId;
  }

  public Integer getPropertyId() {
    return propertyId;
  }

  public void setPropertyId(Integer propertyId) {
    this.propertyId = propertyId;
  }

  
  public Integer getValueId() {
    return valueId;
  }

  
  public void setValueId(Integer valueId) {
    this.valueId = valueId;
  }

  public Short getSortValue() {
    return sortValue;
  }

  public void setSortValue(Short sortValue) {
    this.sortValue = sortValue;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias == null ? null : alias.trim();
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Integer getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(Integer creatorId) {
    Preconditions.checkNotNull(creatorId, "creatorId is null");
    Preconditions.checkArgument(creatorId > 0, "creatorId must be greater than 0");
    this.creatorId = creatorId;
  }

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    this.memo = memo == null ? null : memo.trim();
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getFeatures() {
    return features;
  }

  public void setFeatures(String features) {
    this.features = features == null ? null : features.trim();
  }

}
