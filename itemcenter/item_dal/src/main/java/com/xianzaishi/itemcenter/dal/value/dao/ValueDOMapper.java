package com.xianzaishi.itemcenter.dal.value.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.dal.value.dataobject.ValueDO;

/**
 * 属性值接口
 * @author dongpo
 *
 */
public interface ValueDOMapper {

  List<ValueDO> selectValueByPropertyId(@Param("catId")Integer catId, @Param("propertyId")Integer propertyId);

  int insertValue(ValueDO valueDO);

  int updateValue(ValueDO valueDO);
}
