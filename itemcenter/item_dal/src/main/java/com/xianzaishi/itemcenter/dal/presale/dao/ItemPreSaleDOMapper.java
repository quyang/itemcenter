package com.xianzaishi.itemcenter.dal.presale.dao;

import com.xianzaishi.itemcenter.client.presale.query.ItemPreSaleQuery;
import com.xianzaishi.itemcenter.dal.presale.dataobject.ItemPreSaleDO;

import java.util.List;

public interface ItemPreSaleDOMapper {

    /**
     *  根据指定主键获取一条数据库记录,item_pre_sale
     *
     * @param id
     */
    ItemPreSaleDO selectByPrimaryKey(Long id);

    /**
     * 通过skuId来获取预售的相关信息
     * @param skuId
     * @return
     */
    ItemPreSaleDO queryBySkuId(Long skuId);


    /**
     * 查询所有的预售商品
     * @return
     */
    List<ItemPreSaleDO>  queryItemPreSales(ItemPreSaleQuery query);


    /**
     * 预售商品数量
     * @param query
     * @return
     */
    Integer queryItemPreSalesCount(ItemPreSaleQuery query);

    /**
     *  根据主键删除数据库的记录,item_pre_sale
     *
     * @param id
     */
    int deleteByPrimaryKey(Long id);

    /**
     *  新写入数据库记录,item_pre_sale
     *
     * @param record
     */
    int insert(ItemPreSaleDO record);

    /**
     *  动态字段,写入数据库记录,item_pre_sale
     *
     * @param record
     */
    int insertSelective(ItemPreSaleDO record);


    /**
     *  动态字段,根据主键来更新符合条件的数据库记录,item_pre_sale
     *
     * @param record
     */
    int updateByPrimaryKeySelective(ItemPreSaleDO record);

    /**
     *  根据主键来更新符合条件的数据库记录,item_pre_sale
     *
     * @param record
     */
    int updateByPrimaryKey(ItemPreSaleDO record);
}