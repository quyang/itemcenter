package com.xianzaishi.itemcenter.dal.itemsku.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationAvaliableScheduleQuery;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuRelationScheduleQuery;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemSkuRelationScheduleDO;

public interface ItemSkuRelationScheduleDOMapper {

  /**
   * 插入一个sku关联关系
   * 
   * @param record
   * @return
   */
  int insert(ItemSkuRelationScheduleDO record);
  
  /**
   * 插入多个sku关联关系
   * 
   * @param record
   * @return
   */
  int batchInsert(List<ItemSkuRelationScheduleDO> record);

  /**
   * 更新一个sku关联关系
   * 
   * @param record
   * @return
   */
  int update(ItemSkuRelationScheduleDO record);

  /**
   * 通过前台销售sku查询sku关联关系
   * 
   * @param cskuId
   * @return
   */
  List<ItemSkuRelationScheduleDO> queryList(@Param("query")SkuRelationScheduleQuery query);

  /**
   * 查询可以调价的价格区间
   * 
   * @param cskuId
   * @return
   */
  List<ItemSkuRelationScheduleDO> queryAvaliableSchedule(@Param("query")SkuRelationAvaliableScheduleQuery query, @Param("type")int type, @Param("relationType")int relationType);
  
  /**
   * 通过前台销售sku查询sku关联关系个数
   * @param query
   * @return
   */
  int queryCount (@Param("query")SkuRelationScheduleQuery query);
  
  /**
   * 通过前台销售sku查询sku关联关系
   * 
   * @param cskuId
   * @return
   */
  ItemSkuRelationScheduleDO query(@Param("id") Long id);
  
  /**
   * 通过前台销售sku删除sku关联关系
   * 
   * @param cskuId
   * @return
   */
  int deleteByPrimaryKey(@Param("id") Long id);
}
