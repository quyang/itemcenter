package com.xianzaishi.itemcenter.dal.value.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.dal.value.dataobject.BaseValueDO;

/**
 * 基础属性值接口
 * @author dongpo
 *
 */
public interface BaseValueDOMapper {

  /**
   * 按照属性值id查询基础属性值
   * 
   * @param valueId
   * @return
   */
  List<BaseValueDO> selectBaseValueByIdList(@Param("valueIdList") List<Integer> valueIdList);

  /**
   * 按照属性值名称查询基础属性值
   * 
   * @param valueData
   * @return
   */
  BaseValueDO selectBaseValueByName(@Param("valueData") String valueData);

  /**
   * 插入基础属性值对象
   * 
   * @param valueDO
   * @return
   */
  Integer insertBaseValue(@Param("valueDO") BaseValueDO valueDO);

  /**
   * 更新基础属性值类表
   * 
   * @param valueDO
   * @return
   */
  Integer updateBaseValue(@Param("valueDO") BaseValueDO valueDO);
}
