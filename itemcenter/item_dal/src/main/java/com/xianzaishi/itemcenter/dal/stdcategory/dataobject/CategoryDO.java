package com.xianzaishi.itemcenter.dal.stdcategory.dataobject;

import java.io.Serializable;

import com.xianzaishi.itemcenter.client.stdcategory.dto.CategoryDTO;

/**
 * 类目表字段
 * @author dongpo
 *
 */
public class CategoryDO extends CategoryDTO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = 2354473358171835321L;

}
