package com.xianzaishi.itemcenter.dal.stdcategory.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.dal.stdcategory.dataobject.CategoryDO;

/**
 * 后台类目接口
 * @author dongpo
 *
 */
public interface CategoryDOMapper {

  /**
   * 按照查询条件（类目id,类目名称）查询单个类目
   * 
   * @param stdCatQuery
   * @return
   */
  public CategoryDO selectCategoryById(@Param("catId")Integer catId);

  /**
   * 按照查询条件（类目id,类目名称）查询单个类目
   * 
   * @param stdCatQuery
   * @return
   */
  public CategoryDO selectCategoryByName(@Param("name")String name);
  
  /**
   * 按照类目id批量查询
   * 
   * @param catIds
   * @return
   */
  public List<CategoryDO> selectCategoriesByIdList(List<Integer> catIds);
  
  /**
   * 根据分类号批量查询
   * @param catCode
   * @return
   */
  public List<CategoryDO> selectCategoryDOsByCatCode(@Param("catCode") Long catCode);

  /**
   * 按照类目id查询子类目
   * 
   * @param catId
   * @return
   */
  public List<CategoryDO> selectSubcategoryById(Integer catId);

  /**
   * 插入类目对象
   * 
   * @param categoryDO
   * @return
   */
  public int insertCategory(@Param("categoryDO") CategoryDO categoryDO);

  /**
   * 更新类目列表
   * 
   * @param categoryDO
   * @return
   */
  public int updateCategory(@Param("categoryDO") CategoryDO categoryDO);
}
