package com.xianzaishi.itemcenter.dal.item.query;

import java.io.Serializable;
import java.util.List;

/**
 * 商品查询条件
 * @author dongpo
 *
 */
public class CommodityQuery implements Serializable {


  /**
	 * 
	 */
  private static final long serialVersionUID = 2885826899256501837L;

  /**
   * 商品id组
   */
  private List<Long> itemIds;

  /**
   * 商品标题
   */
  private String itemTitle;

  /**
   * 商品价格区间:左值
   */
  private Integer startPrice;

  /**
   * 商品价格区间:右值
   */
  private Integer endPrice;

  /**
   * 后台叶子类目id
   */
  private List<Integer> categoryIds;

  /**
   * 商店id
   */
  private Integer shopId;

  public List<Long> getItemIds() {
    return itemIds;
  }

  public void setItemIds(List<Long> itemIds) {
    for (Long itemId : itemIds) {
      if (itemId <= 0) {
        throw new IllegalArgumentException("itemId must be greater than 0");
      }
    }
    this.itemIds = itemIds;
  }

  public String getItemTitle() {
    return itemTitle;
  }

  public void setItemTitle(String itemTitle) {
    this.itemTitle = itemTitle;
  }


  public Integer getStartPrice() {
    return startPrice;
  }

  public void setStartPrice(Integer startPrice) {
    if (startPrice != null && startPrice < 0) {
      throw new IllegalArgumentException("startPrice can not be smaller than 0");
    }
    this.startPrice = startPrice;
  }

  public Integer getEndPrice() {
    return endPrice;
  }

  public void setEndPrice(Integer endPrice) {
    this.endPrice = endPrice;
  }

  public List<Integer> getCategoryIds() {
    return categoryIds;
  }

  public void setCategoryIds(List<Integer> categoryIds) {
    this.categoryIds = categoryIds;
  }

  public Integer getShopId() {
    return shopId;
  }

  public void setShopId(Integer shopId) {
    if (shopId != null && shopId <= 0) {
      throw new IllegalArgumentException("shopId must be greater than 0");
    }
    this.shopId = shopId;
  }

}
