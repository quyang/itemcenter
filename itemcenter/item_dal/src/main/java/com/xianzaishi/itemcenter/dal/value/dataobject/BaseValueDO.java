package com.xianzaishi.itemcenter.dal.value.dataobject;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 基本属性表字段
 * @author dongpo
 *
 */
public class BaseValueDO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -5873161732036478034L;

  /**
   * 属性值id
   */
  private Integer valueId;

  /**
   * 属性值名称
   */
  private String valueData;

  /**
   * 基础属性值创建时间
   */
  private Date gmtCreate;

  /**
   * 基础属性创建人
   */
  private Integer creatorId;

  /**
   * 基础属性修改时间
   */
  private Date gmtModified;
  
  /**
   * 属性值状态码
   */
  private Short status; 

  public Integer getValueId() {
    return valueId;
  }

  public void setValueId(Integer valueId) {
    this.valueId = valueId;
  }

  public String getValueData() {
    return valueData;
  }

  public void setValueData(String valueData) {
    this.valueData = valueData == null ? null : valueData.trim();
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Integer getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(Integer creatorId) {
    Preconditions.checkNotNull(creatorId,"creatorId is null");
    Preconditions.checkArgument(creatorId > 0, "creatorId must be greater than 0");
    this.creatorId = creatorId;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }
  
}
