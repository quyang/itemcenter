package com.xianzaishi.itemcenter.dal.log.dao;

import com.xianzaishi.itemcenter.dal.log.dataobject.ItemProcessLogDO;

public interface ItemProcessLogDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_process_log
     *
     * @mbggenerated
     */
//    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_process_log
     *
     * @mbggenerated
     */
    int insert(ItemProcessLogDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_process_log
     *
     * @mbggenerated
     */
//    int insertSelective(ItemProcessLogDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_process_log
     *
     * @mbggenerated
     */
//    ItemProcessLogDO selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_process_log
     *
     * @mbggenerated
     */
//    int updateByPrimaryKeySelective(ItemProcessLogDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table item_process_log
     *
     * @mbggenerated
     */
//    int updateByPrimaryKey(ItemProcessLogDO record);
}