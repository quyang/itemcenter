package com.xianzaishi.itemcenter.dal.property.dataobject;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 基本属性值表字段
 * @author dongpo
 *
 */
public class BasePropertyDO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = 8070002758974342585L;


  /**
   * 类目属性id
   */
  private Integer propertyId;

  /**
   * 类目属性名
   */
  private String propertyName;

  /**
   * 状态码
   */
  private Short status;

  /**
   * 基础属性创建时间
   */
  private Date gmtCreate;

  /**
   * 基础属性修改时间
   */
  private Date gmtModified;

  /**
   * 基础属性创建人
   */
  private Integer creatorId;

  /**
   * 基础属性描述信息
   */
  private String memo;

  /**
   * 从左向右，第一位代表是否必须属性，第二位代表是否产品属性，第三位代表是否sku属性，第四位代表是否输入属性，第五位代表是否下拉单选属性
   * 第六位代表是下拉单选属性并且允许增加属性值（增加的属性值自动进入属性值表，比如品牌、国家这种需要积累的属性值）。。。。。。
   */
  private Integer formatTags;

  public Integer getPropertyId() {
    return propertyId;
  }

  public void setPropertyId(Integer propertyId) {
    this.propertyId = propertyId;
  }

  public String getPropertyName() {
    return propertyName;
  }

  public void setPropertyName(String propertyName) {
    this.propertyName = propertyName;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Integer getCreatorId() {
    return creatorId;
  }

  public void setCreatorId(Integer creatorId) {
    Preconditions.checkNotNull(creatorId, "creatorId is null");
    Preconditions.checkArgument(creatorId > 0, "creatorId must be greater than 0");
    this.creatorId = creatorId;
  }

  public String getMemo() {
    return memo;
  }

  public void setMemo(String memo) {
    this.memo = memo;
  }

  public Integer getFormatTags() {
    return formatTags;
  }

  public void setFormatTags(Integer formatTags) {
    this.formatTags = formatTags;
  }

}
