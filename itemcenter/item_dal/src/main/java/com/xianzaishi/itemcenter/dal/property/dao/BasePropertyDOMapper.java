package com.xianzaishi.itemcenter.dal.property.dao;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.dal.property.dataobject.BasePropertyDO;

/**
 * 基本属性接口
 * @author dongpo
 *
 */
public interface BasePropertyDOMapper {

  /**
   * 按照基本类目属性id查询
   * 
   * @param propertyId
   * @return
   */
  BasePropertyDO selectBasePropertyById(@Param("propertyId") Integer propertyId);

  /**
   * 按照基本类目名查询
   * 
   * @param propertyName
   * @return
   */
  BasePropertyDO selectBasePropertyByName(@Param("propertyName") String propertyName);

  /**
   * 插入基本属性对象
   * 
   * @param propertyDO
   * @return
   */
  int insertBaseProperty(@Param("propertyDO") BasePropertyDO propertyDO);

  /**
   * 更新基本属性类表
   * 
   * @param propertyDO
   * @return
   */
  int updateBaseProperty(@Param("propertyDO") BasePropertyDO propertyDO);
}
