package com.xianzaishi.itemcenter.dal.common.dao;

import org.apache.ibatis.annotations.Param;

/**
 * sequence生成器
 * 
 * @author zhancang
 */
public interface SequenceDOMapper {
  
  /**
   * 对应商品货号类型
   */
  public static final Short SEQUENCE_TYPE_PROCUCT = 1;
  
  /**
   * 对应采购商品类型
   */
  public static final Short SEQUENCE_TYPE_ITEM_PROCUCT = 2;

  /**
   * 对应销售商品类型
   */
  public static final Short SEQUENCE_TYPE_ITEM_COMMODITY = 4;
  
  /**
   * 对应销售sku的PLU码类型
   */
  public static final Short SEQUENCE_TYPE_PLU_SKU = 6;
  
  /**
   * 对应采购商品起始id
   */
  public static final Long SEQUENCE_START_ITEM_PROCUCT = 10201601L;
  
  /**
   * 对应销售商品起始id
   */
  public static final Long SEQUENCE_START_ITEM_COMMODITY = 10201601L;
  
  /**
   * 查询sequence中业务id对应的当前一个起始id。<br/>
   * 插入id如果不存在，会自动创建该业务id对应的主键初始记录
   * 
   * @param sequenceDO
   * @return
   */
  public Long getNext(@Param("startId") Long startId,@Param("type") Short type);

}
