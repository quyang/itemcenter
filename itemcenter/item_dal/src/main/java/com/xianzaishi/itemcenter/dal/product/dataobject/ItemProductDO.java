package com.xianzaishi.itemcenter.dal.product.dataobject;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.client.item.dto.ItemProductDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;

/**
 * 后台表对应do
 * 
 * @author zhancang
 *
 */
public class ItemProductDO extends ItemProductDTO {

  public static final long serialVersionUID = 1L;

  public static final String SPLIT_CHARATER = ";";
  
  //TODO join 互相翻译
  private String sku;
  
  //TODO join
  private String pic;

  //TODO join
  private String checkinfo;

  public String getSku() {
    return sku;
  }

  public void setSku(String sku) {
    this.sku = sku;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic;
  }

  public String getCheckinfo() {
    return checkinfo;
  }

  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo;
  }
  
  /**
   * beancopy时使用
   */
  public void setItemProductSkuList(List<ItemProductSkuDTO> itemProductSkuList) {
    Preconditions.checkNotNull(itemProductSkuList, "itemProductSkuList == null");
    Preconditions.checkArgument(itemProductSkuList.size() > 0, "itemProductSkuList.size() <= 0");
    String tmpStr = "";
    for(ItemProductSkuDTO tmp:itemProductSkuList){
      tmpStr = tmpStr + tmp.getSkuId() + ";";
    }
    sku = tmpStr;
  }
  
  public List<Long> querySkuIdList(){
    if(StringUtils.isEmpty(this.sku)){
      return Collections.emptyList();
    }
    String picArray[] = sku.split(SPLIT_CHARATER);
    List<Long> result = Lists.newArrayList();
    for(String id:picArray){
      result.add(Long.valueOf(id));
    }
    return result;
  }
}
