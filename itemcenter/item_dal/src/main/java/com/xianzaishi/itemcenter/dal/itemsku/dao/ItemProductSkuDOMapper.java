package com.xianzaishi.itemcenter.dal.itemsku.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.dal.itemsku.dataobject.ItemProductSkuDO;

public interface ItemProductSkuDOMapper {

  /**
   * 插入一个后台sku对象
   */
  int insert(ItemProductSkuDO itemProductSku);

  /**
   * 更新一个后台sku信息
   * 
   * @param record
   * @return
   */
  int update(ItemProductSkuDO itemProductSku);

  /**
   * 查询sku对象列表
   * 
   * @param query
   * @return
   */
  List<ItemProductSkuDO> query(@Param("query") SkuQuery query);
  
  /**
   * 查询sku对象个数
   * @param query
   * @return
   */
  int queryCount (@Param("query") SkuQuery query);

}
