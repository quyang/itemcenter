package com.xianzaishi.itemcenter.dal.property.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.dal.property.dataobject.PropertyDO;

/**
 * 属性接口
 * @author dongpo
 *
 */
public interface PropertyDOMapper {

  /**
   * 按照类目id查询类目属性
   * 
   * @param catId
   * @return
   */
  List<PropertyDO> selectPropertyByCatId(@Param("catId")Integer catId);

  /**
   * 按照类目id和属性id查询类目属性
   * 
   * @param catId
   * @param propertyId
   * @return
   */
  PropertyDO selectPropertyByPropertyId(@Param("catId")Integer catId, @Param("propertyId")Integer propertyId);

  /**
   * 按照类目id和属性名称查询类目属性
   * 
   * @param catId
   * @param name
   * @return
   */
  PropertyDO selectPropertyByPropertyName(@Param("catId")Integer catId, @Param("name")String name);
  
  /**
   * 按照类目id和tag属性特征来查询类目属性
   * 
   * @param catId
   * @param tags
   * @return
   */
  List<PropertyDO> selectPropertyByTags(@Param("catId")Integer catId, @Param("tags")Integer tags);

  /**
   * 插入类目属性
   * 
   * @param propertyDO
   * @return
   */
  int insertProperty(PropertyDO propertyDO);

  /**
   * 更新类目属性
   * 
   * @param propertyDO
   * @return
   */
  int updateProperty(PropertyDO propertyDO);

}
