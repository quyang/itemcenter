package com.xianzaishi.itemcenter.dal.item.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.client.item.query.ItemListQuery;
import com.xianzaishi.itemcenter.dal.item.dataobject.ItemBaseDO;
import com.xianzaishi.itemcenter.dal.item.dataobject.ItemDO;

/**
 * 商品查询接口
 * 
 * @author zhancang
 *
 */
public interface ItemDOMapper {

  /**
   * 插入对象
   * 
   * @param itemDO
   * @return
   */
  int insertItem(ItemDO itemDO);

  /**
   * 更新对象
   * 
   * @param itemDO
   * @return
   */
  int updateItem(ItemDO itemDO);

  /**
   * 查询全量数据
   * 
   * @param itemId
   * @return
   */
  ItemDO queryItem(@Param("itemId") Long itemId);

  /**
   * 查询全量数据
   * 
   * @param itemId
   * @return
   */
  ItemBaseDO queryBaseItem(@Param("itemId") Long itemId);

  /**
   * 按照条件查询数据列表
   * 
   * @param query
   * @return
   */
  List<ItemBaseDO> queryItemList(@Param("query") ItemListQuery query);
  
  /**
   * 按照条件查询数据列表，结果按照cm_cat2的顺序输出
   * 
   * @param query
   * @return
   */
  List<ItemBaseDO> queryItemListInOrder(@Param("query") ItemListQuery query, @Param("inOrderIds") String inOrderIds);

  /**
   * 按照条件统计数据
   * 
   * @param query
   * @return
   */
  Long count(@Param("query")ItemListQuery query);

}
