package com.xianzaishi.itemcenter.dal.itemsku.dataobject;

import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;

public class ItemCommoditySkuDO extends ItemCommoditySkuDTO {

  private static final long serialVersionUID = 1L;

  /**
   * 69码
   */
  private Long sku69code;
  
  //TODO join add checkInfo
  private String checkinfo;

  public String getCheckinfo() {
    return checkinfo;
  }

  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo;
  }
  
  public void setSku69code(Long sku69code) {
    if(null != sku69code && sku69code > 0){
      this.sku69code = sku69code;
    }
  }

  public Long getSku69code() {
    return sku69code;
  }
}
