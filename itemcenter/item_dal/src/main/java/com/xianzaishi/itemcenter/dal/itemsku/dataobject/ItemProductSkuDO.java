package com.xianzaishi.itemcenter.dal.itemsku.dataobject;

import com.xianzaishi.itemcenter.client.itemsku.dto.ItemProductSkuDTO;

public class ItemProductSkuDO extends ItemProductSkuDTO {

  private static final long serialVersionUID = 1L;

  private String checkinfo;

  public String getCheckinfo() {
    return checkinfo;
  }

  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo;
  }
}
